"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.PoromechanicsTwoWay import PoromechanicsTwoWay
from lib.Poromechanics import Monolithic
import pylab as plt

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
# parameters['reorder_dofs_serial'] = False  # Required to improve euclid performance... makes monolithic fail
# parameters["form_compiler"]["quadrature_degree"] = 6

plt.rc('font', family='serif')
plt.rcParams.update(
    {'font.size': 18, 'lines.linewidth': 4})

solid_degree = 1
method = "iterative"


def solve_two_way(Nelements):
    side_length = 1e-2
    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
        Nelements, side_length)

    neumann_solid_markers = [TOP, RIGHT]
    neumann_fluid_markers = [LEFT]

    ds = Measure('ds', domain=mesh, subdomain_data=markers,
                 metadata={'optimize': True})
    dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
    dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

    parameters = {"mu_f": 0.035,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "phi0": 0.1,
                  "mu_s": 4066,
                  "lmbda": 711,
                  "ks": 1e3,
                  "kf": 1e-7,
                  "dt": 0.1,
                  "t0": 0.0,
                  "tf": 0.5,
                  "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                  "uref": 1.0,  # 3e-5,
                  "vref": 1.0,  # 6e-3,
                  "pref": 1.0,  # 4e1,
                  "fe_degree_solid": solid_degree,
                  "fe_degree_fluid_velocity": 2,
                  "fe_degree_pressure": 1,
                  "tolerance residual": 1e-8,
                  "tolerance increment": 1e-100,
                  "maxiter": 100,
                  "anderson_order": 0,
                  "anderson_delay": 0,
                  "export_solutions": False,
                  #"output_name": "monolithic",
                  "output_name": "swelling",
                  "solver_type": "fixed-stress-fp-s",
                  "betas": -0.5,
                  "betaf": 0.0,
                  "betap": 1.,
                  "mixing_type": "None",
                  "verbose": False,
                  "keep_solutions": False,
                  "iterations output": False,
                  "iterations solver output": False,
                  "method": method}

    poromechanics = PoromechanicsTwoWay(parameters, mesh, dsNs, dsNf)

    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    bcs_solid = [DirichletBC(poromechanics.solid.V.sub(0), Constant(0), markers, LEFT),
                 DirichletBC(poromechanics.solid.V.sub(1), Constant(0), markers, BOTTOM)]

    bcs_fluid = [DirichletBC(poromechanics.fluid_pressure.V.sub(0), Constant((0, 0)), markers, TOP),
                 DirichletBC(poromechanics.fluid_pressure.V.sub(0), Constant((0, 0)), markers, BOTTOM)]

    bcs_pressure = [DirichletBC(poromechanics.fluid_pressure.V.sub(1), Constant(0), markers, LEFT),
                    DirichletBC(poromechanics.fluid_pressure.V.sub(1),
                                Constant(0), markers, TOP),
                    DirichletBC(poromechanics.fluid_pressure.V.sub(1), Constant(0), markers, RIGHT)]

    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)

    # Create load terms
    f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))

    def f_sur_fluid(t):  # Hard coded boundary
        return Constant(-1e3 * 0.1 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    # Solve
    from time import perf_counter
    t = perf_counter()
    poromechanics.setup()
    t_setup = perf_counter() - t
    t = perf_counter()
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    t_solve = perf_counter() - t
    # avg_iter = poromechanics.avg_iter
    print("Solved problem with {} dofs".format(poromechanics.V_full.dim()))

    return t_setup, t_solve


def solve_monolithic(Nelements):
    # Create geometry and set Neumann boundaries
    side_length = 1e-2
    mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
        Nelements, side_length)

    neumann_solid_markers = [TOP, RIGHT]
    neumann_fluid_markers = [LEFT]

    ds = Measure('ds', domain=mesh, subdomain_data=markers,
                 metadata={'optimize': True})
    dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
    dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

    # Then create solver class
    # C = 8
    # x, y = SpatialCoordinate(mesh)
    # phi0 = Constant(0.1) + Constant(0.5) * \
    # (sin(Constant(C * pi) * x / side_length))**2
    parameters = {"mu_f": 0.035,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "phi0": 0.1,
                  "mu_s": 4066,
                  "lmbda": 711,
                  "ks": 1e3,
                  "kf": 1e-7,
                  "dt": 0.1,
                  "t0": 0.0,
                  "tf": 0.5,
                  "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                  "uref": 1.0,  # 3e-5,
                  "vref": 1.0,  # 6e-3,
                  "pref": 1.0,  # 4e1,
                  "fe_degree_solid": solid_degree,
                  "fe_degree_fluid_velocity": 2,
                  "fe_degree_pressure": 1,
                  "tolerance residual": 1e-8,
                  "tolerance increment": 1e-100,
                  "maxiter": 100,
                  "anderson_order": 0,
                  "anderson_delay": 0,
                  "export_solutions": False,
                  #"output_name": "monolithic",
                  "output_name": "swelling",
                  "solver_type": "monolithic",  
                  "betas": -0.5,
                  "betaf": 0.0,
                  "betap": 1.,
                  "mixing_type": "None",
                  "verbose": False,
                  "keep_solutions": False,
                  "iterations output": False,
                  "method": method}

    # Monolithic solver and splits based on a monolithic structure

    poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

    bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                    DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
                        2), Constant(0), markers, TOP),
                    DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]

    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)

    # Create load terms
    f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))

    def f_sur_fluid(t):  # Hard coded boundary
        return Constant(-1e3 * 0.1 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    # Solve
    from time import perf_counter
    t = perf_counter()
    poromechanics.setup()
    t_setup = perf_counter() - t
    t = perf_counter()
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    t_solve = perf_counter() - t
    print("Solved problem with {} dofs".format(poromechanics.solid_fluid_pressure.V.dim()))
    # avg_iter = poromechanics.avg_iter
    return t_setup, t_solve


Ns = [] 
twoway = []
monol  = []
prop_setup = []
prop_solve = []
prop_total = []

fil = open("output/wall-time-monolithic-vs-2way.csv", "w+")
print("Number of elements; Two way setup; Two way solution; Two way total; Monolithic setup; Monolithic solution; Monolithic total", file=fil)

for Nels in range(50, 301, 50):
    print("Solving for {} elements per side".format(Nels))

    Ns.append(Nels)
    twoway.append(solve_two_way(Nels))
    monol.append(solve_monolithic(Nels))

    twoway_setup, twoway_solution = twoway[-1]
    twoway_total = sum(twoway[-1])
    mono_setup, mono_solution = monol[-1]
    mono_total = sum(monol[-1])

    prop_setup.append(twoway_setup / mono_setup)
    prop_solve.append(twoway_solution / mono_solution)
    prop_total.append(twoway_total / mono_total)
    print("{};{};{};{};{};{};{}".format(Nels, twoway_setup, twoway_solution, twoway_total, mono_setup, mono_solution, mono_total), file=fil)
    print("\n")

fil.close()
import pylab as plt
# plt.plot(Ns, prop_setup, label="setup")
# plt.plot(Ns, prop_solve, label="solve")
plt.plot(Ns, prop_total, label="total")
plt.plot(Ns, len(Ns) * [1], c='red', linestyle='-')
plt.xlabel('Elements per side')
plt.ylabel('Wall time [s]')
# plt.legend()
plt.savefig("output/wall-time-comparison", bbox_inches="tight")

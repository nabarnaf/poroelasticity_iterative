"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic
import numpy as np

# Create geometry and set Neumann boundaries
Nelements = 10
length = 64
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, length)

# Refine on top


def refine_mesh(mesh):
  cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
  cell_markers.set_all(False)
  for c in cells(mesh):
    verts = np.reshape(c.get_vertex_coordinates(), (3, 2))
    verts_x = verts[:, 0]
    verts_y = verts[:, 1]
    newval = verts_y.min() > 2 * length / 3 and verts_x.min() > length / \
        8 and verts_x.max() < 7 / 8 * length
    cell_markers[c] = newval

  # Redefine markers on new mesh
  return refine(mesh, cell_markers)


mesh = refine_mesh(refine_mesh(mesh))


class Left(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[0], 0.0) and on_boundary


class Right(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[0], length) and on_boundary


class Top(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[1], length) and on_boundary


class Bottom(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[1], 0.0) and on_boundary


left, right, top, bottom = Left(), Right(), Top(), Bottom()
LEFT, RIGHT, TOP, BOTTOM = 1, 2, 3, 4  # Set numbering
NONE = 99  # Marker for empty boundary

markers = MeshFunction("size_t", mesh, 1)
markers.set_all(0)

boundaries = (left, right, top, bottom)
def_names = (LEFT, RIGHT, TOP, BOTTOM)
for side, num in zip(boundaries, def_names):
  side.mark(markers, num)

neumann_solid_markers = [TOP]  # All others get weakly 0'd.
neumann_fluid_markers = []

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

# Then create solver class

# Convert E, nu to Lame parameters
E = 3e4
nu = 0.2
mu_s = E / (2 * (1 + nu))
lmbda = E * nu / ((1 + nu) * (1 - 2 * nu))

parameters = {"mu_f": 1e-3,
              "rhof": 1e3,
              "rhos": 500,
              "phi0": 1e-3,
              "mu_s": mu_s,
              "lmbda": lmbda,
              "ks": 1e6,
              "kf": 1e-7,
              "dt": 1e-1,
              "t0": 0.0,
              "tf": 1.5,
              "Kdr": 2 * mu_s + lmbda,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual": 1e-8,
              "tolerance increment": 1e-100,
              "maxiter": 200,
              "anderson_order": 5,
              "anderson_delay": 0,
              "export_solutions": True,
              #"output_name": "monolithic",
              "output_name": "footing",
              "solver_type": "undrained",  # monolithic, undrained, fixed-stress-fp-u
              # "solver_type": "undrained", #consistent with previous results?
              # "solver_type": "fixed-stress-fp-s", # works well!
              # "solver_type": "diagonal-stab", # works well!
              #"solver_type": "diagonal-stab-3-way", # works well!
              # "solver_type": "fixed-stress-p-fs", # Quite large stabilization resulting in slow convergence
              #"solver_type": "fixed-stress-p-f-s", # There is no reason why suddenyl shoudl work nicely!
              # "solver_type": "fixed-stress-sp-f", # Quite large stabilization resulting in slow convergence
              # "solver_type": "cahouet-chabard-p-fs", # without AA, no improvement visible compared to fs-p-fs; works sufficiently with AA and fixed convex mixing; Only right BC (complementary to both solid and fluid seems to work best; but only left or combinations are not significantly worse.
              #"solver_type": "cahouet-chabard-p-fs-to-3-way", # seems as accurate as cahouet-chabard-p-fs but decoupling of f and s for free.
              # "solver_type": "cahouet-chabard-fp-s-to-3-way", # Seems to work if both sovlers are weighted correctly - mixing needed; BCs should be applied complementary to fluid BCs, i.e., left and right
              "betas": -1,
              "betaf": 0.,
              "betap": 1.,
              # "mixing_type": "None",
              # "mixing_type": "Aitken",
              "mixing_type": "None",
              "mixing_weight_fs": 1.0,
              "mixing_weight_diff": 0.1,
              # "mixing_type": "Aitken-constrained",
              "verbose": True,
              "keep_solutions": False,
              "iterations output": True,
              "method": "iterative"}

# Monolithic solver and splits based on a monolithic structure

poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

# After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
    0), Constant((0, 0)), markers, BOTTOM)]


def boundary_foot(x, on_boundary):
  return on_boundary and near(x[1], length) and abs(x[0] - length / 2) < length / 4


def boundary_foot_not(x, on_boundary):
  return on_boundary and not(near(x[1], length) and abs(x[0] - length / 2) < length / 4)


bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
    1), Constant((0, 0)), boundary_foot)]
# At complement of intersection of solid and fluid
bcs_pressure = [DirichletBC(
    poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), boundary_foot_not)]


poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
poromechanics.setup()

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_fluid = lambda t: Constant((0., 0.))


def f_sur_solid(t):
  # return Expression(("0", "abs(x[0]-L)<L/2?-t*1e5:0"), t=min(0.5, t), L=length / 2, degree=4)
  return Expression(("0", "abs(x[0]-L)<L/2?-t*1e4:0"), t=3, L=length / 2, degree=6)


# Solve
poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
avg_iter = poromechanics.avg_iter
print("Avg iter {}\n".format(avg_iter))

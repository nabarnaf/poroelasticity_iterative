#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 15:56:21 2019

@author: barnafi
"""

#from memory_profiler import profile
from dolfin import *
# Used when linear solver = iterative
PETScOptions.set("ksp_type", "gmres")
PETScOptions.set("ksp_atol", 1e-10)
PETScOptions.set("ksp_rtol", 1e-8)
PETScOptions.set("ksp_maxiter", 1000)
PETScOptions.set("ksp_pc_side", "right")
PETScOptions.set("ksp_monitor")
PETScOptions.set("pc_type", "bjacobi")
PETScOptions.set("sub_ksp_type", "preonly")
PETScOptions.set("sub_pc_type", "ilu")
PETScOptions.set("sub_pc_factor_levels", 3)


def zero_func(t):
    return Constant((0, 0, 0))


# @profile
def execute():
    from lib.MeshCreation import prolateGeometry, generateBoundaryMeasure
    from lib.PoromechanicsNonlinear import MonolithicNonlinear

    parameters["form_compiler"]["representation"] = "uflacs"
    parameters["form_compiler"]["cpp_optimize"] = True
    parameters["form_compiler"]["optimize"] = True
    parameters["form_compiler"]["quadrature_degree"] = 4

    filename = "prolate_h4_v2_ASCII"

    mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(filename)

    solid_neumann_markers = [NONE]  # Used as Robin
    solid_robin_markers = [ENDOCARD, EPICARD, BASE]  # Used as Robin
    fluid_neumann_markers = [BASE]
    fluid_noslip_markers = [ENDOCARD, EPICARD]

    dsSN = generateBoundaryMeasure(mesh, markers, solid_neumann_markers)
    dsSR = generateBoundaryMeasure(mesh, markers, solid_robin_markers)
    dsFN = generateBoundaryMeasure(mesh, markers, fluid_neumann_markers)
    dsFns = generateBoundaryMeasure(mesh, markers, fluid_noslip_markers)

    # Then create solver class
    params = {"mu_f": 0.03,
              "rhof": 1e3,
              "rhos": 1e3,
              "phi0": 0.05,
              "ks": 1e8,  # 1e8
              "kf": 1e-7,  # 1e-7
              "Cg": .88e3,  # [Pa]
              "bf": 8,  # [-]
              "bs": 6,  # [-]
              "bn": 3,  # [-]
              "bfs": 12,  # [-]
              "bfn": 3,  # [-]
              "bsn": 3,  # [-]
              "fibers": "fibers/{}.h5".format(filename),
              "AS": 3e4,  # 3e4
              "k": 5e4,
              "dt": 1e-3,
              "t0": 0.0,
              "tf": 0.4,
              "mu_s": 4066,
              "lmbda": 711,
              "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "gamma": 20,
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual absolute": 1e-8,
              "tolerance residual relative": 1e-6,
              "linear mechanics": False,
              "maxiter": 100,
              "linear solver": "iterative",
              "anderson_order": 0,
              "anderson_delay": 0,
              "export_solutions": False,
              "output_name": "nonlinear-ventricle-fs",
              "solver_type": "monolithic",
              # "solver_type": "undrained",
              # "solver_type": "fixed-stress-fp-s",
              # "solver_type": "cahouet-chabard-fp-s-to-3-way",
              "stab fs": 0.022,  # P1 0.014, P2 0.022
              # "solver_type": "CC-fs-p",
              "reassemble Jacobian": True,
              "reassemble Jacobian every nth iteration": 10,  # only triggered when "reassemble Jacobian" = False
              # "stab diff": 1.0,
              # "mixing_type": "Aitken",
              "mixing_type": "None",
              # "mixing_type": "fixed-weights",
              "mixing_weight_fs": 1.0,  # Used alone for non CC cases
              "mixing_weight_diff": 0.1,
              # "mixing_type": "Aitken-constrained",
              "keep_solutions": False,
              "iterations output": True}

    # Monolithic solver and splits based on a monolithic structure

    poromechanics = MonolithicNonlinear(
        params, mesh, dsSN, dsSR, dsFN, dsFns)

    bcs_solid = []

    # bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0, 0)), markers, ENDOCARD),
    #              DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0, 0)), markers, EPICARD)]
    bcs_fluid = []

    Vp = poromechanics.solid_fluid_pressure.Vp
    bcs_pressure = [DirichletBC(Vp, Constant(0), markers, ENDOCARD),
                    DirichletBC(Vp, Constant(0), markers, EPICARD),
                    DirichletBC(Vp, Constant(0), markers, BASE)]

    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
    poromechanics.setup()

    # Create load terms

    f_vol_solid = f_vol_fluid = f_sur_solid = f_sur_fluid = zero_func
    def p_source(t): return Constant(0.0)

    # Solve
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    avg_iter = poromechanics.avg_iter
    print("Avg iter {}\n".format(avg_iter))


execute()

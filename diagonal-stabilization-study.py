"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic

do_bulk = False
do_densities = False
do_kdr = False
do_permeability = True

# Discretization
fe_degree_solid = 1
fe_degree_fluid = 2
fe_degree_pressure = 1
anderson_depth = 0
file_prefix = "iteration-counts/compact-analysis-diagonal-stab-p{}p{}p{}-aa{}".format(
    fe_degree_solid, fe_degree_fluid, fe_degree_pressure, anderson_depth)
# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)

neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

betas = [0., 0., -0.5, -1.]
betaf = [0., 0., 0.0, 0.]
betap = [0., 1., 1.0, 1.]
# betas = [0]
# betaf = [0]
# betap = [1]
Nbeta = len(betas)


params_base = {"mu_f": 0.035,
               "rhof": 1e3,
               "rhos": 1e3,
               "phi0": 0.1,
               "mu_s": 4066,
               "lmbda": 711,
               "ks": 1e3,
               "kf": 1e-7,
               "dt": 0.1,
               "t0": 0.0,
               "tf": 1,
               "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
               "uref": 1.,
               "vref": 1.,
               "pref": 1.,
               "fe_degree_solid": fe_degree_solid,
               "fe_degree_fluid_velocity": fe_degree_fluid,
               "fe_degree_pressure": fe_degree_pressure,
               "tolerance residual": 1e-8,
               "tolerance increment": 1e-100,
               "maxiter": 500,
               "anderson_order": anderson_depth,
               "anderson_delay": 0,
               "export_solutions": False,
               "output_name": "diagonal-stab",
               "solver_type": "diagonal-stab",
               "betas": 0,
               "betaf": 0,
               "betap": 0,
               "mixing_type": "None",
               "verbose": True,
               "keep_solutions": False,
               "iterations output": False,
               "method": "direct"}

# for ks in [1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e12]:
if do_bulk:
    fil = open("{}-ks.csv".format(file_prefix), "w+")
    print("Ks; (0,0,0); (0,0,1); (-0.5, 0,1); (-1,0,1)", file=fil)
    for ks in [1e3, 1e6, 1e9, 1e12]:
        avg_iters = []
        avg_iters.append(ks)

        for i in range(Nbeta):
            # Then create solver class
            parameters = params_base.copy()
            parameters["ks"] = ks
            parameters["betas"] = betas[i]
            parameters["betaf"] = betaf[i]
            parameters["betap"] = betap[i]

            # Monolithic solver and splits based on a monolithic structure

            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
            bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

            bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

            bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                            DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Create load terms
            f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
                (0., 0.))

            def f_sur_fluid(t):
                return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

            # Solve
            poromechanics.solve(f_vol_solid, f_sur_solid,
                                f_vol_fluid, f_sur_fluid)
            avg_iter = poromechanics.avg_iter
            avg_iters.append(";")
            avg_iters.append(avg_iter)

            print("ks {}; betas {}; betaf {}; betap {}; avg iter {}\n".format(
                ks, betas[i], betaf[i], betap[i], avg_iter))
        avg_iters.append("\\")
        print(*avg_iters, file=fil)

    fil.close()

if do_permeability:
    fil = open("{}-perm.csv".format(file_prefix), "w+")
    print("perm; (0,0,0); (0,0,1); (-0.5, 0,1); (-1,0,1)", file=fil)

    # for kf in [1e-2, 1e-4, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10, 1e-11, 1e-12]:
    for kf in [1e-7, 1e-8, 1e-9, 1e-10, 1e-11, 1e-12]:
        avg_iters = []
        avg_iters.append(kf)

        for i in range(Nbeta):
            # Then create solver class
            parameters = params_base.copy()
            parameters["kf"] = kf
            parameters["betas"] = betas[i]
            parameters["betaf"] = betaf[i]
            parameters["betap"] = betap[i]
            parameters["maxiter"] = 500

            # Monolithic solver and splits based on a monolithic structure

            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
            bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

            bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

            bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                            DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Create load terms
            f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
                (0., 0.))

            def f_sur_fluid(t):
                return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

            # Solve
            poromechanics.solve(f_vol_solid, f_sur_solid,
                                f_vol_fluid, f_sur_fluid)
            avg_iter = poromechanics.avg_iter
            avg_iters.append(";")
            avg_iters.append(avg_iter)

            print("perm {}; betas {}; betaf {}; betap {}; avg iter {}\n".format(
                kf, betas[i], betaf[i], betap[i], avg_iter))
        avg_iters.append("\\")
        print(*avg_iters, file=fil)

    fil.close()

if do_densities:
    fil = open("{}-rho.csv".format(file_prefix), "w+")
    print("rho; (0,0,0); (0,0,1); (-0.5, 0,1); (-1,0,1)", file=fil)

    # for rho in [1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7]:
    for rho in [1e2, 1e4, 1e6, 1e8]:
        avg_iters = []
        avg_iters.append(rho)

        for i in range(Nbeta):
            # Then create solver class
            parameters = params_base.copy()
            parameters["rhof"] = rho
            parameters["rhos"] = rho
            parameters["betas"] = betas[i]
            parameters["betaf"] = betaf[i]
            parameters["betasp"] = betap[i]

            # Monolithic solver and splits based on a monolithic structure

            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
            bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

            bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

            bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                            DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Create load terms
            f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
                (0., 0.))

            def f_sur_fluid(t):
                return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

            # Solve
            poromechanics.solve(f_vol_solid, f_sur_solid,
                                f_vol_fluid, f_sur_fluid)
            avg_iter = poromechanics.avg_iter
            avg_iters.append(";")
            avg_iters.append(avg_iter)

            print("rho {}; betas {}; betaf {}; betap {}; avg iter {}\n".format(
                rho, betas[i], betaf[i], betap[i], avg_iter))
        avg_iters.append("\\")
        print(*avg_iters, file=fil)

    fil.close()


if do_kdr:
    fil = open("{}-kdr.csv".format(file_prefix), "w+")
    print("kdr; (0,0,0); (0,0,1); (-0.5, 0,1); (-1,0,1)", file=fil)

    MUs = [4066e-2, 4066e-1, 4066e0, 4066e1, 4066e2]
    LAM = [711e-2, 711e-1, 711e0, 711e1, 711e2]
    NMu = len(MUs)

    for j in range(NMu):

        avg_iters = []
        Kdr = MUs[j] + LAM[j]
        avg_iters.append(Kdr)

        for i in range(Nbeta):
            # Then create solver class
            parameters = params_base.copy()
            parameters["mu_s"] = MUs[j]
            parameters["lmbda"] = LAM[j]
            parameters["Kdr"] = MUs[j] + LAM[j]
            parameters["betas"] = betas[i]
            parameters["betaf"] = betaf[i]
            parameters["betasp"] = betap[i]

            # Monolithic solver and splits based on a monolithic structure
            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
            bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

            bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                         DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

            bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                            DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Create load terms
            f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
                (0., 0.))

            def f_sur_fluid(t):
                return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

            # Solve
            poromechanics.solve(f_vol_solid, f_sur_solid,
                                f_vol_fluid, f_sur_fluid)
            avg_iter = poromechanics.avg_iter
            avg_iters.append(";")
            avg_iters.append(avg_iter)

            print("kdr {}; betas {}; betaf {}; betap {}; avg iter {}\n".format(
                Kdr, betas[i], betaf[i], betap[i], avg_iter))
        avg_iters.append("\\")
        print(*avg_iters, file=fil)

    fil.close()

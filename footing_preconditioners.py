from dolfin import *
import numpy as np
from mpi4py import MPI
from petsc4py import PETSc
from lib.MeshCreation import generate_square
from lib.AndersonAcceleration import AndersonAcceleration
from lib.AAR import AAR
from time import time
initial_time = time()
comm = PETSc.Comm(MPI.COMM_WORLD)
my_rank = comm.Get_rank()

# prec_type = "diagonal-stab"
prec_type = "undrained"
# prec_type = "diagonal-stab-3way"
inner_prec_type = 'lu' # Any petsc preconditioner
w1, w2 = 0.9, 0.1
# solver_type = "cg"
# solver_type = "gmres"
solver_type = "aar"

# Solver parameters
atol, rtol, maxiter = 1e-10, 1e-8, 500
monitor_convergence = True
#AAR specific
p_aar, order = 5, 10
order_preconditioner = 0  # Doesnt work with AAR
omega = beta = 1

degree_s, degree_f, degree_p = 1,2,1

# Problem parameters
N = 10
dim = 2
length = 64
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    N, length)

# Refine on top
def refine_mesh(mesh):
  cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
  cell_markers.set_all(False)
  for c in cells(mesh):
    verts = np.reshape(c.get_vertex_coordinates(), (3, 2))
    verts_x = verts[:, 0]
    verts_y = verts[:, 1]
    newval = verts_y.min() > 2 * length / 3 and verts_x.min() > length / \
        8 and verts_x.max() < 7 / 8 * length
    cell_markers[c] = newval

  # Redefine markers on new mesh
  return refine(mesh, cell_markers)
mesh = refine_mesh(refine_mesh(mesh))

# Everything by hand due to markers...
class Left(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[0], 0.0) and on_boundary
class Right(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[0], length) and on_boundary
class Top(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[1], length) and on_boundary
class Bottom(SubDomain):
  def inside(self, x, on_boundary):
    return near(x[1], 0.0) and on_boundary
left, right, top, bottom = Left(), Right(), Top(), Bottom()
LEFT, RIGHT, TOP, BOTTOM = 1, 2, 3, 4  # Set numbering
NONE = 99  # Marker for empty boundary

markers = MeshFunction("size_t", mesh, 1)
markers.set_all(0)

boundaries = (left, right, top, bottom)
def_names = (LEFT, RIGHT, TOP, BOTTOM)
for side, num in zip(boundaries, def_names):
  side.mark(markers, num)

neumann_solid_markers = [TOP]  # All others get weakly 0'd.
neumann_fluid_markers = []

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dx = dx(mesh)
dsNeumannSolid = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNeumannFluid = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

# FE space
V = FunctionSpace(mesh, MixedElement(VectorElement('CG', triangle, degree_s),
                                     VectorElement('CG', triangle, degree_f),
                                     FiniteElement('CG', triangle, degree_p)))
if my_rank == 0: print("Dofs = {}".format(V.dim()), flush=True)
sol = Function(V)
sol_n = Function(V)
sol_nn = Function(V)
us_nm1, uf_nm1, p_nm1 = sol_n.split()
us_nm2, uf_nm2, p_nm2 = sol_nn.split()

# BCs
bcs_solid = [DirichletBC(V.sub(0), Constant((0, 0)), markers, BOTTOM)]

def boundary_foot(x, on_boundary):
  return on_boundary and near(x[1], length) and abs(x[0] - length / 2) < length / 4
def boundary_foot_not(x, on_boundary):
  return on_boundary and not(near(x[1], length) and abs(x[0] - length / 2) < length / 4)

bcs_fluid = [DirichletBC(V.sub(
    1), Constant((0, 0)), boundary_foot)]
# At complement of intersection of solid and fluid
bcs_pressure = [DirichletBC(
    V.sub(2), Constant(0), boundary_foot_not)]

# Create list with presssure bcs according to dofmap ordering
dofs_p = V.sub(2).dofmap().dofs()
bcs_sub_pressure = []
for b in bcs_pressure:
    bc_vals = b.get_boundary_values().keys()
    for i,dof in enumerate(dofs_p):
        if dof in bc_vals:
            bcs_sub_pressure.append(i)


bcs = bcs_solid + bcs_fluid
bcs_diff = bcs + bcs_pressure

p_source = lambda t: Constant(0)
ff_sur = lambda t: Constant((0,0))
def fs_sur(t):
  # return Expression(("0", "abs(x[0]-L)<L/2?-t*1e5:0"), t=min(0.5, t), L=length / 2, degree=4)
  return Expression(("0", "abs(x[0]-L)<L/2?-t*1e4:0"), t=3, L=length / 2, degree=6)

# params
E = 3e4
nu = 0.2
rhof = 1000
rhos = 500
mu_f = 1e-3
phi0 = 1e-3
phis = 1 - phi0
# mu_s = 4000
mu_s = E / (2 * (1 + nu))
# lmbda = 700
lmbda = E * nu / ((1 + nu) * (1 - 2 * nu))
ks = 1e6
kf = 1e-7
ikf = inv(kf)

# Stabilization
betas = -0.5
betaf = 0
betap = 1

# time
t0 = 0
t = t0
tf = 0.1
dt = 1e-1
idt = 1/dt


# Compute A, P and b
us, vf, p = TrialFunctions(V)
v, w, q = TestFunctions(V)


def hooke(ten):
    return 2 * mu_s * ten + lmbda * tr(ten) * Identity(2)


def eps(vec):
    return sym(grad(vec))


# First base matrix
a_s_base = (Constant(rhos / dt**2) * phis * dot(us, v)
            + inner(hooke(eps(us)), eps(v))
            - p * div(phis * v)
            - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

a_f_base = (Constant(rhof / dt) * phi0 * dot(vf, w)
            + 2. * Constant(mu_f) *
            inner(phi0 * eps(vf), eps(w))
            - p * div(phi0 * w)
            + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1 / dt) * us), w)) * dx

a_p_base = (phis**2 / Constant(ks * dt) * p * q
            + div(phi0 * vf) * q
            + div(phis * Constant(1. / dt) * us) * q) * dx

A = PETScMatrix()
assemble(a_s_base + a_f_base + a_p_base, tensor=A)
for b in bcs:
    b.apply(A)

# Then preconditioner matrix
M = PETScMatrix()
M_diff = PETScMatrix()
if prec_type == "undrained":
    N = Constant(ks) / phis**2

    a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
           + inner(hooke(eps(us)), eps(v))
           + N * div(phis * us) * div(phis * v)
           - phi0 ** 2 * dot(inv(kf) * (- Constant(1. / dt) * us), v)) * dx

    a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
           + 2. * Constant(mu_f) * phi0 *
           inner(eps(vf), eps(w))
           - p * div(phi0 * w)
           + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), w)) * dx

    a_p = (phis**2 / Constant(ks * dt) * p * q
           + div(phi0 * vf) * q
           + div(phis * Constant(1. / dt) * us) * q) * dx
    a_p_diff = 0*q*dx
elif prec_type == "diagonal-stab":
    beta_s_hat = betas

    a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
           + inner(hooke(eps(us)), eps(v))
           - p * div(phis * v)
           - phi0**2 * dot(inv(kf) * (vf - Constant((1. + beta_s_hat) / dt) * us), v)) * dx

    beta_f_hat = betaf

    a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
           + 2. * Constant(mu_f) * phi0 *
           inner(eps(vf), eps(w))
           - p * div(phi0 * w)
           + Constant(1. + beta_f_hat) * phi0**2 * dot(inv(kf) * vf, w)) * dx

    beta_p_hat = betap
    beta_p = beta_p_hat * phis**2 / \
        Constant(dt * (2. * mu_s / dim + lmbda))

    a_p = (phis**2 / Constant(ks * dt) * p * q
           + beta_p * p * q
           + div(phi0 * vf) * q) * dx
    a_p_diff = 0*q*dx
elif prec_type == "diagonal-stab-3way":
    beta_s_hat = betas

    a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
           + inner(hooke(eps(us)), eps(v))
           - p * div(phis * v)
           - phi0**2 * dot(inv(kf) * (vf - Constant((1. + beta_s_hat) * 1. / (dt)) * us), v)) * dx

    beta_f_hat = betaf

    a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
           + 2. * Constant(mu_f) * phi0 *
           inner(eps(vf), eps(w))
           - p * div(phi0 * w)
           + Constant(1. + beta_f_hat) * phi0**2 * dot(inv(kf) * (vf), w)) * dx

    beta_p_hat = betap
    beta_p = beta_p_hat * phis**2 / \
        Constant(dt * (2. * mu_s / dim + lmbda))
    beta_CC1 = phi0 / Constant(2. * mu_f / dim)
    #beta_CC2 = phi0**2 *  inv(phi0 * Constant(rhof / dt) + Constant(1. + beta_f_hat) * phi0**2 * inv(kf))
    beta_CC2 = 1e0 * inv(Constant(rhof / dt) / phi0 + inv(kf))

    a_p = (phis**2 / Constant(ks * dt) * p * q
           + (beta_p + beta_CC1) * p * q) * dx

    a_p_diff = (phis**2 / Constant(ks * dt) * p * q
                + beta_p * p * q
                + dot(beta_CC2 * grad(p), grad(q))) * dx
assemble(a_s + a_f + a_p, tensor = M)
assemble(a_s + a_f + a_p_diff, tensor = M_diff)
for b in bcs:
    b.apply(M)

for b in bcs_diff:
    b.apply(M_diff)

# Compute RHS in a given time step
def get_rhs(t):
    # Compute solid residual
    rhs_s_n = dot(fs_sur(t), v) * dsNeumannSolid
    lhs_s_n = dot(Constant(rhos * idt**2) * phis * (-2. * us_nm1 + us_nm2), v) * dx - phi0**2 * dot(Constant(ikf) * (- Constant(idt) * (- us_nm1)), w)* dx
    r_s = rhs_s_n - lhs_s_n

    # Compute fluid residual
    rhs_f_n = dot(ff_sur(t), w) * dsNeumannFluid

    lhs_f = dot(Constant(rhof * idt) * phi0 * (- uf_nm1), w) + phi0**2 * dot(Constant(ikf) * (- Constant(idt) * (- us_nm1)), w)
    lhs_f_n = lhs_f * dx

    r_f = rhs_f_n - lhs_f_n

    # Compute pressure residual
    rhs_p_n = Constant(1 / rhof) * p_source(t) * q * dx

    D_sf = div(Constant(idt) * phis * (- us_nm1)) * q
    M_p = phis**2 / Constant(ks * dt) * (- p_nm1) * q
    lhs_p_n = (M_p + D_sf) * dx
    r_p = rhs_p_n - lhs_p_n

    out_vec = PETScVector()
    assemble(rhs_s_n + rhs_f_n + rhs_p_n, tensor=out_vec)
    for b in bcs:
        b.apply(out_vec)
    return out_vec

# Create preconditioner context
class PreconditionerCC(object):

    def __init__(self, M, M_diff, V, flag_3_way=False, w1=1.0, w2=0.1):
        self.M = M
        self.M_diff = M_diff
        self.flag_3_way = flag_3_way
        self.w1 = w1
        self.w2 = w2
        self.ns = V.sub(0).dim()
        self.nf = V.sub(1).dim()
        self.np = V.sub(2).dim()
        self.dofmap_s = V.sub(0).dofmap().dofs()
        self.dofmap_f = V.sub(1).dofmap().dofs()
        self.dofmap_p = V.sub(2).dofmap().dofs()
        self.dofmap_fp = sorted(self.dofmap_f + self.dofmap_p)
        self.anderson = AndersonAcceleration(order_preconditioner)

    def setUp(self, pc):
        # Here we build the PC object that uses the concrete,
        # assembled matrix A.  We will use this to apply the action
        # of A^{-1}
        self.pc_s = PETSc.PC().create()
        self.pc_f = PETSc.PC().create()
        self.pc_p = PETSc.PC().create()
        self.pc_fp = PETSc.PC().create()
        self.pc_p_diff = PETSc.PC().create()

        # Create index sets for each physics
        self.is_s = PETSc.IS().createGeneral(self.dofmap_s)
        self.is_f = PETSc.IS().createGeneral(self.dofmap_f)
        self.is_p = PETSc.IS().createGeneral(self.dofmap_p)
        self.is_fp = PETSc.IS().createGeneral(self.dofmap_fp)

        # Create temp block vectors used in apply()
        self.temp_sx = PETSc.Vec().create()
        self.temp_sy = PETSc.Vec().create()
        self.temp_fx = PETSc.Vec().create()
        self.temp_fy = PETSc.Vec().create()
        self.temp_px = PETSc.Vec().create()
        self.temp_py = PETSc.Vec().create()
        self.temp_fpx = PETSc.Vec().create()
        self.temp_fpy = PETSc.Vec().create()
        self.temp_p_diffx = PETSc.Vec().create()
        self.temp_p_diffy = PETSc.Vec().create()

        # Extract sub-matrices
        self.Ms_s = self.M.createSubMatrix(self.is_s, self.is_s)
        self.Mf_s = self.M.createSubMatrix(self.is_f, self.is_s)
        self.Mf_f = self.M.createSubMatrix(self.is_f, self.is_f)
        self.Mp_s = self.M.createSubMatrix(self.is_p, self.is_s)
        self.Mp_f = self.M.createSubMatrix(self.is_p, self.is_f)
        self.Mp_p = self.M.createSubMatrix(self.is_p, self.is_p)
        self.Mfp_s = self.M.createSubMatrix(self.is_fp, self.is_s)
        self.Mfp_fp = self.M.createSubMatrix(self.is_fp, self.is_fp)
        self.Mp_diff = self.M_diff.createSubMatrix(self.is_p, self.is_p)

        # Configure block preconditioners
        # self.pc_s.setOptionsPrefix("prec_s_")
        # self.pc_f.setOptionsPrefix("prec_f_")
        # self.pc_p.setOptionsPrefix("prec_p_")
        # self.pc_p_diff.setOptionsPrefix("prec_p_diff_")

        self.pc_s.setType(inner_prec_type)
        self.pc_s.setOperators(self.Ms_s)
        self.pc_f.setType(inner_prec_type)
        self.pc_f.setOperators(self.Mf_f)
        self.pc_p.setType(inner_prec_type)
        self.pc_p.setOperators(self.Mp_p)
        self.pc_fp.setType(inner_prec_type)
        self.pc_fp.setOperators(self.Mfp_fp)
        self.pc_p_diff.setType(inner_prec_type)
        self.pc_p_diff.setOperators(self.Mp_diff)

        if inner_prec_type == "lu":
            factor_method = "mumps"
            self.pc_s.setFactorSolverType(factor_method)
            self.pc_f.setFactorSolverType(factor_method)
            self.pc_p.setFactorSolverType(factor_method)
            self.pc_fp.setFactorSolverType(factor_method)
            self.pc_p_diff.setFactorSolverType(factor_method)

    def apply(self, pc, x, y):
        # Result is y = A^{-1}x

        x.getSubVector(self.is_s, self.temp_sx)
        y.getSubVector(self.is_s, self.temp_sy)
        self.pc_s.apply(self.temp_sx, self.temp_sy)
        # TODO: use mmult to avoid creating temp vectors for off-diagonal contributions
        if self.flag_3_way:
            x.getSubVector(self.is_f, self.temp_fx)
            y.getSubVector(self.is_f, self.temp_fy)
            x.getSubVector(self.is_p, self.temp_px)
            y.getSubVector(self.is_p, self.temp_py)
            x.getSubVector(self.is_p, self.temp_p_diffx)
            y.getSubVector(self.is_p, self.temp_p_diffy)
            self.pc_f.apply(self.temp_fx - self.Mf_s * self.temp_sy, self.temp_fy)
            self.pc_p.apply(self.temp_px - self.Mp_s * self.temp_sy - self.Mp_f * self.temp_fy, self.temp_py)

            # Same rhs as for pc_p
            self.temp_p_diffx = self.temp_px - self.Mp_s * self.temp_sy - self.Mp_f * self.temp_fy
            # Apply bc to pressure rhs first
            n_bcs = len(bcs_sub_pressure)
            self.temp_p_diffx.setValues(bcs_sub_pressure, np.zeros(n_bcs))
            self.pc_p_diff.apply(self.temp_p_diffx, self.temp_p_diffy)

            self.temp_py.scale(self.w1)
            self.temp_py.axpy(self.w2, self.temp_p_diffy)
            # with open('out.txt', 'a') as f:
                # f.write("{}\n".format(self.temp_p_diffy.norm()))

            x.restoreSubVector(self.is_f, self.temp_fx)
            y.restoreSubVector(self.is_p, self.temp_py)
            x.restoreSubVector(self.is_p, self.temp_px)
            y.restoreSubVector(self.is_f, self.temp_fy)
        else:  # use 2way
            x.getSubVector(self.is_fp, self.temp_fpx)
            y.getSubVector(self.is_fp, self.temp_fpy)

            # compute A_fp_s ys, ys resulting vector from before
            self.pc_fp.apply(self.temp_fpx - self.Mfp_s * self.temp_sy, self.temp_fpy)
            x.restoreSubVector(self.is_fp, self.temp_fpx)
            y.restoreSubVector(self.is_fp, self.temp_fpy)

        x.restoreSubVector(self.is_s, self.temp_sx)
        y.restoreSubVector(self.is_s, self.temp_sy)

        self.anderson.get_next_vector(y)


comm = PETSc.Comm(MPI.COMM_WORLD)
my_rank = comm.Get_rank()

# Preconditioner context
flag_3_way = prec_type == "diagonal-stab-3way"
ctx = PreconditionerCC(M.mat(), M_diff.mat(), V, flag_3_way, w1, w2)
pc = PETSc.PC().create()
pc.setType('python')
pc.setPythonContext(ctx)
pc.setOperators(A.mat())
pc.setUp()

# Create solver
if solver_type in ("cg", "gmres"):
    solver = PETSc.KSP().create(comm=comm)
    solver.setOperators(A.mat())
    solver.setType(solver_type)
    solver.setTolerances(atol, rtol, 1e20, maxiter)
    solver.setPC(pc)
    if solver_type == "gmres": solver.setGMRESRestart(maxiter)
    if monitor_convergence: PETSc.Options().setValue("-ksp_monitor", None)
    solver.setFromOptions()
else: # AAR
    solver = AAR(order, p_aar, omega, beta, A.mat(), pc=pc, atol=atol, rtol=rtol, maxiter=maxiter, monitor_convergence=monitor_convergence)
# Time loop
while t < tf:
    current_time = time()
    t += dt
    rhs = get_rhs(t)  # Includes bcs
    solver.solve(rhs.vec(), sol.vector().vec())
    its = solver.getIterationNumber()
    if(my_rank == 0): print("-- Solved time t={:.4f} in {} iterations".format(t, its), flush=True)
    sol_nn.assign(sol_n)
    sol_n.assign(sol)

if(my_rank == 0): print("Execution time = {}".format(time() - initial_time))

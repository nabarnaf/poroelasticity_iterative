"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from time import time
from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import TwoWay

# parameters['reorder_dofs_serial'] = False
PETScOptions.set("ksp_atol", 1e-10)
PETScOptions.set("ksp_rtol", 1e-8)
PETScOptions.set("ksp_maxiter", 1000)
PETScOptions.set("ksp_type", "gmres")
# PETScOptions.set("ksp_monitor_true_residual")
PETScOptions.set("pc_type", "ilu")
PETScOptions.set("pc_factor_levels", 3)
# PETScOptions.set("pc_type", "gamg")
# PETScOptions.set("mg_coarse_pc_type", "lu")
# PETScOptions.set("pc_gamg_agg_nsmooths", 5)
# PETScOptions.set("pc_mg_cycles", "v")
# PETScOptions.set("help")
# PETScOptions.set("pc_type", "hypre")
# PETScOptions.set("pc_hypre_type", "boomeramg")
# PETScOptions.set("pc_hypre_boomeramg_relax_type_all", "SOR/Jacobi")
# PETScOptions.set("pc_hypre_boomeramg_relax_type_all", "Chebyshev")
# PETScOptions.set("pc_hypre_boomeramg_strong_threshold", "0.1")  # Default 0.25

# Create geometry and set Neumann boundaries
Nelements = 20
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)


neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

exponent = 1
MU = 4066 * 10**(exponent)
LAMBDA = 711 * 10**(exponent)
parameters = {"mu_f": 0.035,
              "rhof": 1e3,
              "rhos": 1e3,
              "phi0": 0.1,
              "mu_s": MU,
              "lmbda": LAMBDA,
              "ks": 1e6,
              "kf": 1e-7,
              "dt": 0.1,
              "t0": 0.0,
              "tf": 0.1,
              "Kdr": MU + LAMBDA,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual absolute": 1e-10,
              "tolerance residual relative": 1e-6,
              "maxiter": 1,
              "anderson_order": 10,
              "anderson_delay": 0,
              "export_solutions": False,
              # "output_name": "monolithic",
              "output_name": "swelling",
              # monolithic, undrained, diagonal-stab, fixed-stress-fp-u
              # "solver_type": "monolithic",
              # "solver_type": "undrained",
              "solver_type": "diagonal-stab",  # consistent with previous results?
              # "solver_type": "fixed-stress-fp-s", # works well!
              # "solver_type": "diagonal-stab-3-way",  # works well!
              "betas": -0.5,
              "betaf": 0.,
              "betap": 1.,
              # "solver_type": "fixed-stress-p-fs", # Quite large stabilization resulting in slow convergence
              # "solver_type": "fixed-stress-p-f-s", # There is no reason why suddenyl shoudl work nicely!
              # "solver_type": "fixed-stress-sp-f", # Quite large stabilization resulting in slow convergence
              # "solver_type": "cahouet-chabard-p-fs", # without AA, no improvement visible compared to fs-p-fs; works sufficiently with AA and fixed convex mixing; Only right BC (complementary to both solid and fluid seems to work best; but only left or combinations are not significantly worse.
              # "solver_type": "cahouet-chabard-p-fs-to-3-way", # seems as accurate as cahouet-chabard-p-fs but decoupling of f and s for free.
              # "solver_type": "cahouet-chabard-fp-s-to-3-way", # Seems to work if both sovlers are weighted correctly - mixing needed; BCs should be applied complementary to fluid BCs, i.e., left and right
              # "mixing_type": "None",
              "mixing_type": "fixed-weights",
              "mixing_weight_fs": 1,
              "mixing_weight_diff": 0.1,
              # "mixing_type": "Aitken-constrained",
              # "mixing_type": "Aitken",
              "verbose": False,
              "keep_solutions": False,
              "iterations output": True,
              "method": "iterative",  # direct, iterative, AAR
              "splitting as preconditioner": False,
              "iterative maxiter": 1000,
              "AAR order": 10,
              "AAR p": 5,
              "AAR omega": 1,
              "AAR beta": 1}


poromechanics = TwoWay(parameters, mesh, dsNs, dsNf)

# After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
bcs_solid = [DirichletBC(poromechanics.solid.V.sub(0), Constant(0), markers, LEFT),
             DirichletBC(poromechanics.solid.V.sub(1), Constant(0), markers, BOTTOM)]

bcs_fluid = [DirichletBC(poromechanics.fluid_pressure.V.sub(0), Constant((0, 0)), markers, TOP),
             DirichletBC(poromechanics.fluid_pressure.V.sub(0), Constant((0, 0)), markers, BOTTOM)]

bcs_pressure = [DirichletBC(poromechanics.fluid_pressure.V.sub(1), Constant(0), markers, LEFT),
                DirichletBC(poromechanics.fluid_pressure.V.sub(1),
                            Constant(0), markers, TOP),
                DirichletBC(poromechanics.fluid_pressure.V.sub(1), Constant(0), markers, RIGHT)]


poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
poromechanics.setup()

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))


def f_sur_fluid(t):  # Hard coded boundary
    return Constant(-1e3 * 0.1 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)


# Solve
t = time()
poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
avg_iter = poromechanics.avg_iter
print("Avg iter {} in {}s\n".format(avg_iter, time() - t))

from dolfin import *
from petsc4py import PETSc
# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)", degree=4)
g = Expression("sin(5*x[0])", degree=4)
a = inner(grad(u), grad(v))*dx
L = f*v*dx + g*v*ds

A = PETScMatrix()
assemble(a, tensor=A)

b = PETScVector()
assemble(L, tensor=b)

class PreconditionerCC(object):
    def setUp(self, pc):
        A, P = pc.getOperators()
        self.A = A
        self.P = P
        self.pc = PETSc.PC().create()
        self.pc.setType('bjacobi')
        self.pc.setOperators(P)
        self.pc.setFactorLevels(10)
    def apply(self, pc, x, y):
        self.pc.apply(x, y)

ksp = PETSc.KSP().create()
ksp.setOperators(A.mat(), A.mat())
# ksp.setUp()
ksp.setType("cg")
pc = ksp.getPC()
pc.setType('python')
ctx = PreconditionerCC()
pc.setPythonContext(ctx)
# pc.setType('ilu')
# pc.setFactorLevels(10)
# pc.setUp()
ksp.setFromOptions()

bc.apply(A, b)
sol = b.copy()
ksp.solve(b.vec(), sol.vec())

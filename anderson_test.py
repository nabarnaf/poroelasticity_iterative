"""
Bratu test problem to test Anderson acceleration
"""

from dolfin import *
from lib.AndersonAcceleration import AndersonAcceleration
from time import time

# set_log_active(False)

N = 20
mesh = UnitSquareMesh(N, N)

# Parameters
mu = 1e2
maxiter = 100
tol = 1e-10
m = 5
delay = 0

V = FunctionSpace(mesh, "CG", 1)
bc = DirichletBC(V, Constant(0), "on_boundary")

u = TrialFunction(V)
v = TestFunction(V)
u0 = Function(V)

error, it = 1, 0
solk = Function(V)
sol = Function(V)
times_no = []
while error > tol and it < maxiter:
    a = dot(grad(u), grad(v)) * dx
    L = -Constant(mu) * exp(solk) * v * dx

    current_t = time()
    solve(a == L, sol, bc)

    error = (sol.vector().vec() - solk.vector().vec()).norm()
    it += 1

    solution_time = time() - current_t
    print("No accel: it {}, error={} in {}s".format(it, error, solution_time))
    times_no.append(solution_time)
    solk.assign(sol)
iterations_no = it

error, it = 1, 0
solk.vector().zero()
sol = Function(V)
anderson = AndersonAcceleration(m)

times_accel = []
while error > tol and it < maxiter:
    a = dot(grad(u), grad(v)) * dx
    L = -Constant(mu) * exp(solk) * v * dx

    current_t = time()
    solve(a == L, sol, bc)

    anderson.get_next_vector(sol.vector().vec())
    error = (sol.vector() - solk.vector()).norm("l2")
    assign(solk, sol)
    it += 1
    solution_time = time() - current_t
    print("Accel: iteration {}, error={} in {}s".format(it, error, solution_time))

    times_accel.append(solution_time)

iterations_accel = it

print("Acceleration: \t\t {} iterations, {:.2e}s avg time".format(
    iterations_accel, sum(times_accel) / iterations_accel))
print("No acceleration: \t\t {} iterations, {:.2e}s avg time".format(
    iterations_no, sum(times_no) / iterations_no))

# import pylab as plt
# plt.subplot(1,2,1)
# plot(sol)
# plt.subplot(1,2,2)
# plot(sol_accel)
# plt.show()

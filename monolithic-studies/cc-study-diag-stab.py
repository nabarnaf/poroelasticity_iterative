"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.PoromechanicsMonolithic import Monolithic

# Discretization
fe_degree_solid = 1
fe_degree_fluid = 2
fe_degree_pressure = 1

# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)

neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

betas = [0., 0., 0.0, -0.5, -0.5, -1., -1.0, 0,  0.0]
betaf = [0., 0., 0.0,  0.0,  0.0,  0.,  0.0, 1, -0.5]
betap = [0., 1., 0.5,  1.0,  0.5,  1.,  0.5, 1,  1.0]
Nbeta = len(betas)

fil = open("analysis-cc-p1p2p1-perm.csv", "w+")
print("perm, none, aa1, aa3, aa5, aitken", file=fil)

for kf in [1e-2, 1e-4, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10, 1e-11, 1e-12]:
    avg_iters = []
    avg_iters.append(kf)

    for aa in [0, 1, 3, 5]:
        # Then create solver class
        parameters = {"mu_f": 0.035,
                      "rhof": 1e3,
                      "rhos": 1e3,
                      "phi0": 0.1,
                      "mu_s": 4066,
                      "lmbda": 711,
                      "ks": 1e8,
                      "kf": kf,
                      "dt": 0.1,
                      "t0": 0.0,
                      "tf": 0.9999999999999,
                      "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                      "uref": 1.,
                      "vref": 1.,
                      "pref": 1.,
                      "fe_degree_solid": fe_degree_solid,
                      "fe_degree_fluid_velocity": fe_degree_fluid,
                      "fe_degree_pressure": fe_degree_pressure,
                      "tolerance": 1e-100, #1e-6
                      "absolute_residual_tolerance": 1e-8,
                      "maxiter": 500,
                      "anderson_order": aa,
                      "anderson_delay": 0,
                      "export_solutions": False,
                      "output_name": "diagonal-stab",
                      "solver_type": "cahouet-chabard-p-fs",
                      "mixing_type": "fixed-weights",
                      "mixing_weight_fs": 1.,
                      "mixing_weight_diff": 1.,
                      "verbose": True,
                      "keep_solutions": False}
        
        # Monolithic solver and splits based on a monolithic structure
        poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)
        
        # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
        bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                     DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]
        
        bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                     DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]
        
        bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]
        
        poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
        poromechanics.setup()
        
        # Create load terms
        f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))
        
        def f_sur_fluid(t):
            return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)
        
        # Solve
        poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
        avg_iter = poromechanics.avg_iter
        avg_iters.append("&")
        avg_iters.append(avg_iter)

        print("perm {}; aa {} ; avg iter {}\n".format(kf, aa, avg_iter))

    # Then create solver class
    parameters = {"mu_f": 0.035,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "phi0": 0.1,
                  "mu_s": 4066,
                  "lmbda": 711,
                  "ks": 1e8,
                  "kf": kf,
                  "dt": 0.1,
                  "t0": 0.0,
                  "tf": 0.9999999999999,
                  "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                  "uref": 1.,
                  "vref": 1.,
                  "pref": 1.,
                  "fe_degree_solid": fe_degree_solid,
                  "fe_degree_fluid_velocity": fe_degree_fluid,
                  "fe_degree_pressure": fe_degree_pressure,
                  "tolerance": 1e-100, #1e-6
                  "absolute_residual_tolerance": 1e-8,
                  "maxiter": 500,
                  "anderson_order": 0,
                  "anderson_delay": 0,
                  "export_solutions": False,
                  "output_name": "diagonal-stab",
                  "solver_type": "cahouet-chabard-p-fs",
                  "mixing_type": "Aitken",
                  "verbose": True,
                  "keep_solutions": False}
    
    # Monolithic solver and splits based on a monolithic structure
    
    poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)
    
    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]
    
    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]
    
    bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                    DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]
    
    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
    poromechanics.setup()
    
    # Create load terms
    f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))
    
    def f_sur_fluid(t):
        return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)
    
    # Solve
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    avg_iter = poromechanics.avg_iter
    avg_iters.append("&")
    avg_iters.append(avg_iter)

    print("perm {}; aitken; avg iter {}\n".format(kf, avg_iter))

    avg_iters.append("\\")
    print(avg_iters, file=fil)

fil.close()


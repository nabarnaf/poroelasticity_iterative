"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.PoromechanicsMonolithic import Monolithic

# Discretization
fe_degree_solid = 1
fe_degree_fluid = 2
fe_degree_pressure = 1

# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)

neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

betas = [0., 0., 0.0, -0.5, -0.5, -1., -1.0, 0,  0.0]
betaf = [0., 0., 0.0,  0.0,  0.0,  0.,  0.0, 1, -0.5]
betap = [0., 1., 0.5,  1.0,  0.5,  1.,  0.5, 1,  1.0]
Nbeta = len(betas)


fil = open("analysis-p1p2p1-kdr.csv", "w+")
print("kdr, (0,0,0), (0,0,1), (0,0,0.5), (-0.5, 0,1), (-0.5,0,0.5), (-1,0,1), (-1,0,0.5), (0,1,1), (0,-0.5,1)", file=fil)

MUs = [4066e-2, 4066e-1, 4066e0, 4066e1, 4066e2, 4066e3]
LAM = [711e-2, 711e-1, 711e0, 711e1, 711e2, 711e3]
NMu = len(MUs)

for j in range(NMu):

    avg_iters = []
    Kdr = MUs[j] + LAM[j]
    avg_iters.append(Kdr)

    for i in range(Nbeta):
        # Then create solver class
        parameters = {"mu_f": 0.035,
                      "rhof": 1e3,
                      "rhos": 1e3,
                      "phi0": 0.1,
                      "mu_s": MUs[j],
                      "lmbda": LAM[j],
                      "ks": 1e8,
                      "kf": 1e-9,
                      "dt": 0.1,
                      "t0": 0.0,
                      "tf": 0.9999999999999,
                      "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                      "uref": 1.,
                      "vref": 1.,
                      "pref": 1.,
                      "fe_degree_solid": fe_degree_solid,
                      "fe_degree_fluid_velocity": fe_degree_fluid,
                      "fe_degree_pressure": fe_degree_pressure,
                      "tolerance": 1e-100,
                      "absolute_residual_tolerance": 1e-8,
                      "maxiter": 200,
                      "anderson_order": 0,
                      "anderson_delay": 0,
                      "export_solutions": False,
                      "output_name": "diagonal-stab",
                      "solver_type": "diagonal-stab",
                      "betas" : betas[i],
                      "betaf" : betaf[i],
                      "betap" : betap[i],
                      "mixing_type": "None",
                      "verbose": True,
                      "keep_solutions": False}
        
        # Monolithic solver and splits based on a monolithic structure
        
        poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)
        
        # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
        bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                     DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]
        
        bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                     DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]
        
        bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, LEFT),
                        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, RIGHT)]
        
        poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
        poromechanics.setup()
        
        # Create load terms
        f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))
        
        def f_sur_fluid(t):
            return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)
        
        # Solve
        poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
        avg_iter = poromechanics.avg_iter
        avg_iters.append("&")
        avg_iters.append(avg_iter)

        print("kdr {}; betas {}; betaf {}; betap {}; avg iter {}\n".format(Kdr, betas[i], betaf[i], betap[i], avg_iter))
    avg_iters.append("\\")
    print(avg_iters, file=fil)

fil.close()

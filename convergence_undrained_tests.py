"""
Script in charge of performing convergence tests on the selling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right

Outputs:
- Convergence increasing N
- Convergence decreasing Km
- Repeat previous for non constant porosity (nearly solid, nearly fluid and highly oscilatory)
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic


# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)


neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))


# Parameters for convergence
# Create array of bulk modulus(es?)  Correct plural...?
ks_vals = [10**n for n in range(3, 7)]  # Up to 10**8
km_vals = [10**n for n in range(-14, -7)]   # Up to 10**7
phi_vals = [Expression(" sin(C * x[0] / L) * sin(C * x[1] / L)/2",
                       degree=6, C=C, L=side_length, domain=mesh) for C in range(1, 11)]


# Then create solver class
# Then create solver class
parameters = {"mu_f": 0.035,
              "rhof": 2e3,
              "rhos": 2e3,
              "phi0": 0.1,
              "mu_s": 4066,
              "lmbda": 711,
              "ks": 1e4,
              "kf": 1e-7,
              "dt": 0.1,
              "t0": 0.0,
              "tf": 1.0,
              "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 1,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual": 1e-8,
              "tolerance increment": 1e-100,
              "maxiter": 100,
              "anderson_order": 5,
              "anderson_delay": 0,
              "export_solutions": False,
              #"output_name": "monolithic",
              "output_name": "cc-fp-s-3-aitken",
              #"solver_type": "monolithic", #monolithic, undrained, fixed-stress-fp-u
              "solver_type": "undrained",  # consistent with previous results?
              #"solver_type": "fixed-stress-fp-s", # works well!
              # "solver_type": "diagonal-stab", # works well!
              #"solver_type": "diagonal-stab-3-way", # works well!
              # "betas": -0.5,
              # "betaf": 0.0,
              # "betap": 1.,
              #"solver_type": "fixed-stress-p-fs", # Quite large stabilization resulting in slow convergence
              #"solver_type": "fixed-stress-p-f-s", # There is no reason why suddenyl shoudl work nicely!
              #"solver_type": "fixed-stress-sp-f", # Quite large stabilization resulting in slow convergence
              #"solver_type": "cahouet-chabard-p-fs", # without AA, no improvement visible compared to fs-p-fs; works sufficiently with AA and fixed convex mixing; Only right BC (complementary to both solid and fluid seems to work best; but only left or combinations are not significantly worse.
              #"solver_type": "cahouet-chabard-p-fs-to-3-way", # seems as accurate as cahouet-chabard-p-fs but decoupling of f and s for free.
              #"solver_type": "cahouet-chabard-fp-s-to-3-way", # Seems to work if both sovlers are weighted correctly - mixing needed; BCs should be applied complementary to fluid BCs, i.e., left and right
              "mixing_type": "None",
              # "mixing_type": "Aitken",
              #"mixing_type": "fixed-weights",
              #"mixing_weight_fs": 1.,
              #"mixing_weight_diff": 0.1,
              #"mixing_type": "Aitken-constrained",
              "verbose": False,
              "keep_solutions": True,
              "iterations output": False,
              "method": "iterative"}


def solve_swelling(**kwargs):
    """
    Solve Swelling case, possibly receives a dictionary with values to be overriden from the original parameters.
    """

    # Copy parameters, give priority to given parameters
    params_copy = {}
    for k in parameters:
        if k in kwargs.keys():
            params_copy[k] = kwargs[k]
        else:
            params_copy[k] = parameters[k]

    poromechanics = Monolithic(params_copy, mesh, dsNs, dsNf)

    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(0), Constant(0), markers, LEFT),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(0).sub(1), Constant(0), markers, BOTTOM)]

    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, TOP),
                 DirichletBC(poromechanics.solid_fluid_pressure.V.sub(1), Constant((0, 0)), markers, BOTTOM)]

    poromechanics.set_bcs(bcs_solid, bcs_fluid)
    poromechanics.setup()
    # Create load terms
    f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant((0., 0.))

    def f_sur_fluid(t):
        return Constant(-1e5 *(1 - exp(-t**2 / 0.25))) * FacetNormal(mesh)

    # Solve
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)

    return poromechanics


def convergence_N():
    """
    Test convergence for an increasing ks
    """

    print("\n\nTest convergence for decreasing N (by increasing ks)")

    iterations = []
    rates = []
    for ks in ks_vals:
        poromechs = solve_swelling(ks=ks)
        avg_iterations = poromechs.compute_average_iterations()
        avg_rate = poromechs.compute_average_convergence()
        iterations.append(avg_iterations)
        rates.append(avg_rate)

    return iterations, rates

    # Output to pyplot, TODO


def convergence_km():
    """
    Test convergence for vanishing permeability
    """

    print("\n\nTest convergence for increasing km")

    iterations = []
    rates = []
    for km in km_vals:
        poromechs = solve_swelling(kf=km)
        avg_iterations = poromechs.compute_average_iterations()
        avg_rate = poromechs.compute_average_convergence()
        iterations.append(avg_iterations)
        rates.append(avg_rate)

    return iterations, rates

    # Output to pyplot, TODO


def convergence_spatial_phi():
    """
    Test convergence on different spatially dependent porosities
    """

    print("\n\nTest convergence for oscillatory porosities")

    iterations = []
    rates = []
    for phi0 in phi_vals:
        poromechs = solve_swelling(phi0=phi0)
        avg_iterations = poromechs.compute_average_iterations()
        avg_rate = poromechs.compute_average_convergence()
        iterations.append(avg_iterations)
        rates.append(avg_rate)

    return iterations, rates

    # Output to pyplot, TODO


# Perform tests
N_iterations, N_rates = convergence_N()
km_iterations, km_rates = convergence_km()
phi_iterations, phi_rates = convergence_spatial_phi()

# Generate output
print("     Bulk modulus list: ", ks_vals)
print("     Avg iterations: ", N_iterations)
print("     Avg rate", N_rates)

print("\n     Permeabilities list: ", km_vals)
print("     Avg iterations: ", km_iterations)
print("     Avg rate", km_rates)

print("\n     Number of porosities: ", len(phi_vals))
print("     Avg iterations: ", phi_iterations)
print("     Avg rate", phi_rates)

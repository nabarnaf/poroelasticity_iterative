"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
It then compares many schemes based on a monolithic formulation to then save iteration counts
"""
from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic
from copy import deepcopy

# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)

neumann_solid_markers = [TOP, RIGHT]
neumann_fluid_markers = [LEFT]

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

# Create base parameters
parameters_base = {"mu_f": 0.035,
                   "rhof": 2e3,
                   "rhos": 2e3,
                   "phi0": 0.1,
                   "mu_s": 4066,
                   "lmbda": 711,
                   "ks": 1e4,
                   "kf": 1e-7,
                   "dt": 0.1,
                   "t0": 0.0,
                   "uref": 1, 
                   "vref": 1,
                   "pref": 1,
                   "tf": 0.9999999999999,
                   "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                   "fe_degree_solid": 2,
                   "fe_degree_fluid_velocity": 2,
                   "fe_degree_pressure": 1,
                   "tolerance residual": 1e-8,
                   "tolerance increment": 1e-6, 
                   "maxiter": 200,
                   "anderson_order": 0,
                   "anderson_delay": 0,
                   "export_solutions": False,
                   "output_name": "",
                   "solver_type": "monolithic",
                   "mixing_type": "None",
                   "mixing_weight_fs": 1.0,
                   "iterations output": False,
                   "keep_solutions": False,
                   "method": "iterative"}

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
    (0., 0.))


def f_sur_fluid(t):
    return Constant(-1e3 * parameters_base["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)


ks_list = [1e4, 1e8]
aa_list = [0, 1, 5]
solid_degrees = [1, 2]
solvers_single_increment = ["undrained", "fixed-stress-fp-s",
                            "fixed-stress-p-fs"]#, "fixed-stress-sp-f", "fixed-stress-p-f-s"]
solvers_double_increment = ["cahouet-chabard-p-fs",
                            "cahouet-chabard-fp-s-to-3-way", "cahouet-chabard-p-fs-to-3-way"]


def getParams():
    return deepcopy(parameters_base)


def getBCs(poromechanics):
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        0).sub(0), Constant(0), markers, LEFT),
        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            0).sub(1), Constant(0), markers, BOTTOM)]

    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        1), Constant((0, 0)), markers, TOP),
        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            1), Constant((0, 0)), markers, BOTTOM)]

    bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        2), Constant(0), markers, LEFT),
        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            2), Constant(0), markers, TOP),
        DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            2), Constant(0), markers, RIGHT)]
    return bcs_solid, bcs_fluid, bcs_pressure


fil = open("output/analysis-single-increment.csv", "w+")
print("Ks, FE_solid, mixing, AA, solver, avg iter", file=fil)

for ks in ks_list:
    for solid_degree in solid_degrees:
        for mixing_type in ["None"]: #, "Aitken"]:
            for aa in aa_list:
                for solver_type in solvers_single_increment:

                    # Then create solver class
                    parameters = getParams()
                    parameters["anderson_order"] = aa
                    parameters["solver_type"] = solver_type
                    parameters["ks"] = ks
                    parameters["mixing_type"] = mixing_type
                    parameters["fe_degree_solid"] = solid_degree

                    # Monolithic solver and splits based on a monolithic structure
                    poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

                    bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

                    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
                    poromechanics.setup()

                    # Solve
                    poromechanics.solve(f_vol_solid, f_sur_solid,
                                        f_vol_fluid, f_sur_fluid)
                    avg_iter = poromechanics.avg_iter

                    print("Ks {}; FE_solid {}; solver {}; mixing {}; AA {}; avg iter {}\n".format(
                        ks, solid_degree, solver_type, mixing_type, aa, avg_iter))
                    print("{}, {}, {}, {}, {}, {}".format(
                        ks, solid_degree, mixing_type, aa, solver_type, avg_iter), file=fil)

fil.close()

fil = open("output/analysis-double-increment-fixed-weight.csv", "w+")
print("Ks, FE_solid, mixing, mixing_weight_diff, AA, solver, avg iter", file=fil)
for ks in ks_list:
    for solid_degree in solid_degrees:    
        for mixing_type in ["fixed-weights"]:
            for mixing_weight_diff in [0.5, 0.1]:
                for aa in aa_list:
                    for solver_type in solvers_double_increment:

                        # Then create solver class
                        parameters = getParams()
                        parameters["ks"] = ks
                        parameters["mixing_type"] = "fixed-weights"
                        parameters["mixing_weight_diff"] = mixing_weight_diff
                        parameters["solver_type"] = solver_type
                        parameters["fe_degree_solid"] = solid_degree
                        parameters["anderson_order"] = aa

                        # Monolithic solver and splits based on a monolithic structure
                        poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

                        bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

                        poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
                        poromechanics.setup()

                        # Solve
                        poromechanics.solve(f_vol_solid, f_sur_solid,
                                            f_vol_fluid, f_sur_fluid)
                        avg_iter = poromechanics.avg_iter
                        print("Ks {}; FE_solid {}; solver {}; mixing {}; mixing weight {}; AA {}; avg iter {}\n".format(
                            ks, solid_degree, solver_type, mixing_type, mixing_weight_diff, aa, avg_iter))
                        print("{}, {}, {}, {}, {}, {}, {}".format(ks, solid_degree, mixing_type,
                                                                  mixing_weight_diff, aa, solver_type, avg_iter), file=fil)

fil.close()

fil = open("output/analysis-double-increment-aitken.csv", "w+")
print("Ks, FE_solid, mixing, AA, solver, avg iter", file=fil)
for ks in ks_list:
    for solid_degree in solid_degrees:
        for mixing_type in ["Aitken", "Aitken-constrained"]:
            for aa in aa_list:
                for solver_type in solvers_double_increment:


                    # Then create solver class
                    parameters = getParams()
                    parameters["ks"] = ks
                    parameters["mixing_type"] = mixing_type
                    parameters["solver_type"] = solver_type
                    parameters["fe_degree_solid"] = solid_degree
                    parameters["anderson_order"] = aa

                    # Monolithic solver and splits based on a monolithic structure
                    poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

                    bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

                    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
                    poromechanics.setup()

                    # Solve
                    try:
                        poromechanics.solve(f_vol_solid, f_sur_solid,
                                        f_vol_fluid, f_sur_fluid)
                        avg_iter = poromechanics.avg_iter
                        print("Ks {}; FE_solid {}; solver {}; mixing {}; AA {}; avg iter {}\n".format(
                            ks, solid_degree, solver_type, mixing_type, aa, avg_iter))
                        print("{}, {}, {}, {}, {}, {}".format(
                            ks, solid_degree, mixing_type, aa, solver_type, avg_iter), file=fil)
                    except:
                        continue

fil.close()

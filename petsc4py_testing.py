from petsc4py import PETSc
import dolfin as df

# Vector operations

n = 10  # Size of vector.

x = PETSc.Vec().createSeq(n)  # Faster way to create a sequential vector.

x.setValues(range(n), range(n))  # x = [0 1 ... 9]
x.shift(1)  # x = x + 1 (add 1 to all elements in x)

print('Performing various vector operations on x =', x.getArray())

print('Sum of elements of x =', x.sum())
print('Dot product with itself =', x.dot(x))
print('1-norm =', x.norm(PETSc.NormType.NORM_1))
print('2-norm =', x.norm())
print('Infinity-norm =', x.norm(PETSc.NormType.NORM_INFINITY))


# Matrix operations

# Create vector to multiply matrices by.
n, m = 5, 10
e = PETSc.Vec().createSeq(m)
e.set(1)
y = PETSc.Vec().createSeq(n)  # Put answer here.

A = PETSc.Mat().createDense([n, m])
A.setUp()

v = PETSc.Vec().createSeq(n)
v.setValues(range(n), range(n))

A.setValues(range(n), [m / 2], v)
A.assemble()

print('Values of dense matrix with new column:')
# print(A.getValues(range(n), range(m)))


# Interface with dolfin, handle subvectors

mesh = df.UnitSquareMesh(6, 6)
el = df.VectorElement("CG", df.interval, 1)
el2 = df.FiniteElement("DG", df.interval, 0)
V = df.FunctionSpace(mesh, df.MixedElement(el, el2))

dofmap = V.dofmap()
dofmap0 = V.sub(0).dofmap()

x = PETSc.Vec().createSeq(V.dim())

x.shift(-1)

print("Sub 0 dofmap", dofmap0.dofs())

x.setValues(dofmap0.dofs(), dofmap0.dofs())

print("Print x:", x.getArray())
print("Values at dofmap0", x.getValues(dofmap0.dofs()))
print("Values at dofmap1", x.getValues(V.sub(1).dofmap().dofs()))


u = df.Function(V)
u0, u1 = u.split(True)

print("\n\n-----Test concatenation in PETSc")
u0vec = u0.vector().vec()
u0vec.setValues(range(u0vec.size), range(u0vec.size))
u1vec = u1.vector().vec()
u1vec.setValues(range(u1vec.size), range(u1vec.size))
u1vec.shift(100)

print("\nInitial vectors", u0.vector().get_local(), u1.vector().get_local())

bigvec = PETSc.Vec().createSeq(u0vec.size + u1vec.size)

bigvec.setValues(range(u0vec.size), u0vec)
bigvec.setValues(range(u0vec.size, u0vec.size + u1vec.size), u1vec)

print("\nConcatenated vector", bigvec.getArray())

bigvec.shift(-100)
bigvec.getValues(range(u0vec.size), values=u0vec)
bigvec.getValues(range(u0vec.size, u0vec.size + u1vec.size), values=u1vec)


print("\nInitial vectors after shift", u0.vector().get_local(), u1.vector().get_local())

"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
It then compares many schemes based on a monolithic formulation to then save iteration counts
"""
from dolfin import *
from lib.MeshCreation import prolateGeometry, generateBoundaryMeasure
from lib.PoromechanicsNonlinear import MonolithicNonlinear
from copy import deepcopy
import time
import statistics

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6

filename = "prolate_h4_v2_ASCII"
# filename = "prolate_4mm"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(filename)

solid_robin_markers = [ENDOCARD, EPICARD, BASE]
solid_neumann_markers = [NONE]
fluid_neumann_markers = [BASE]
fluid_noslip_markers = [ENDOCARD, EPICARD]
# fluid_noslip_markers = [NONE]

dsSN = generateBoundaryMeasure(mesh, markers, solid_neumann_markers)
dsSR = generateBoundaryMeasure(mesh, markers, solid_robin_markers)
dsFN = generateBoundaryMeasure(mesh, markers, fluid_neumann_markers)
dsFns = generateBoundaryMeasure(mesh, markers, fluid_noslip_markers)

# Create base parameters
parameters_base = {"mu_f": 0.03,
                   "rhof": 1e3,
                   "rhos": 1e3,
                   "phi0": 0.05,
                   "ks": 5e4,  # 1e8
                   "kf": 1e-7,  # 1e-7
                   "Cg": .88e3,  # [Pa]
                   "bf": 8,  # [-]
                   "bs": 6,  # [-]
                   "bn": 3,  # [-]
                   "bfs": 12,  # [-]
                   "bfn": 3,  # [-]
                   "bsn": 3,  # [-]
                   "fibers": "fibers/{}.h5".format(filename),
                   "AS": 3e4,  # 3e4
                   "k": 5e4,
                   "dt": 1e-3,
                   "t0": 0.0,
                   "tf": 0.3,
                   "mu_s": 4066,
                   "lmbda": 711,
                   "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                   "gamma": 20,
                   "uref": 1.0,  # 3e-5,
                   "vref": 1.0,  # 6e-3,
                   "pref": 1.0,  # 4e1,
                   "linear mechanics": False,
                   "fe_degree_solid": 2,
                   "fe_degree_fluid_velocity": 2,
                   "fe_degree_pressure": 1,
                   "tolerance residual": 1e-6,
                   "tolerance increment": 1e-20,  # Can't converge on increments
                   "maxiter": 100,
                   "anderson_order": 0,
                   "anderson_delay": 0,
                   "export_solutions": False,
                   "output_name": "nonlinear",
                   # "solver_type": "monolithic",
                   # "solver_type": "undrained",
                   # "solver_type": "fixed-stress-fp-s",
                   # "solver_type": "cahouet-chabard-fp-s-to-3-way",
                   "stab fs": 1.0,
                   "stab diff": 1.0,
                   # "mixing_type": "Aitken",
                   # "mixing_type": "None",
                   "mixing_type": "fixed-weights",
                   "mixing_weight_fs": 1.0,  # Used for non CC cases
                   "mixing_weight_diff": 0.1,
                   # "mixing_type": "Aitken-constrained",
                   "keep_solutions": False,
                   "iterations output": True}

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = f_sur_fluid = lambda t: Constant(
    (0., 0., 0.))


ks_list = [1e8]
aa_list = [0, 1, 5]
solid_degrees = [1, 2]
solvers_list = ["monolithic", "undrained",
                "fixed-stress-fp-s", "cahouet-chabard-fp-s-to-3-way"]
# solvers_list = ["fixed-stress-fp-s", "cahouet-chabard-fp-s-to-3-way"]
# solvers_list = ["fixed-stress-fp-s"] # "", "cahouet-chabard-fp-s-to-3-way"]


def getParams():
    return deepcopy(parameters_base)


def getBCs(poromechanics):
    bcs_solid = []
    bcs_fluid = []

    sfp = poromechanics.solid_fluid_pressure
    if sfp.solver_type == "monolithic":
        Vs = sfp.V.sub(0)
        Vf = sfp.V.sub(1)
    elif sfp.solver_type in ("fixed-stress-fp-s", "undrained"):
        Vs = sfp.Vs
        Vf = sfp.Vfp.sub(0)
    elif sfp.solver_type == "cahouet-chabard-fp-s-to-3-way":
        Vs = sfp.Vs
        Vf = sfp.Vf

    # bcs_solid = [DirichletBC(Vs, Constant((0, 0, 0)), markers, BASE)]
    # bcs_fluid = [DirichletBC(Vf, Constant((0, 0, 0)), markers, ENDOCARD),
        # DirichletBC(Vf, Constant((0, 0, 0)), markers, EPICARD)]

    if poromechanics.solver_type == "cahouet-chabard-fp-s-to-3-way":
        Vp = poromechanics.solid_fluid_pressure.Vp
        bcs_pressure = [DirichletBC(Vp, Constant(0), markers, ENDOCARD),
                        DirichletBC(Vp, Constant(0), markers, EPICARD),
                        DirichletBC(Vp, Constant(0), markers, BASE)]
    else:
        bcs_pressure = []
    return bcs_solid, bcs_fluid, bcs_pressure


fil = open("output/analysis-nonlinear-test-opt.csv", "w+")
print("Ks, FE_solid, AA, solver, avg iter", file=fil)

iterations = []

MAX_ITER_CAP = 10

for ks in ks_list:
    for aa in aa_list:
        for deg in solid_degrees:
            for solver_type in solvers_list:

                print("Solving Ks {}; FE degree {}; solver {}; AA {}\n".format(
                    ks, deg, solver_type, aa))

                # Then create solver class
                parameters = getParams()
                parameters["anderson_order"] = aa
                parameters["solver_type"] = solver_type
                parameters["ks"] = ks
                parameters["fe_degree_solid"] = deg
                if deg == 1:
                    if solver_type == "fixed-stress-fp-s":
                        parameters["stab fs"] = 0.014
                    elif solver_type == "cahouet-chabard-fp-s-to-3-way":
                        parameters["stab fs"] = 0.08
                elif deg == 2 and solver_type == "fixed-stress-fp-s":
                    if solver_type == "fixed-stress-fp-s":
                        parameters["stab fs"] = 0.022
                    elif solver_type == "cahouet-chabard-fp-s-to-3-way":
                        parameters["stab fs"] = 0.1


                # Monolithic solver and splits based on a monolithic structure
                poromechanics = MonolithicNonlinear(
                    parameters, mesh, dsSN, dsSR, dsFN, dsFns)

                bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

                poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
                poromechanics.setup()

                poromechanics.markers = markers
                poromechanics.ENDOCARD = ENDOCARD
                poromechanics.EPICARD = EPICARD

                # Solve
                # try:
                t_start = time.perf_counter()

                #### Copy solve method from poromechanics to create stopping after 10 its with MAXITER.
                t = poromechanics.t0
                its = []

                maxiter_counter = 0
                while t < poromechanics.tf:

                    t += poromechanics.dt
                    poromechanics.t = t
                    iter_count = poromechanics.solve_time_step(f_vol_solid(t), f_sur_solid(t), f_vol_fluid(t), f_sur_fluid(t))
                    its.append(iter_count)
                    if iter_count == parameters["maxiter"]:
                        maxiter_counter += 1
                    else:
                        maxiter_counter = 0
                    if maxiter_counter == MAX_ITER_CAP:
                        print("Maximum number of capped iterations reached, breaking current setting.")
                        break
                    print("-- Solved time t={:.4f} in {} iterations".format(t, iter_count))

                avg_iter = statistics.mean(its)
                t_work = time.perf_counter() - t_start

                iterations.append((solver_type, ks, deg, aa, its))

                print("Ks {}; FE degree {}; solver {}; AA {}; avg iter {}, time {}\n".format(
                    ks, deg, solver_type, aa, avg_iter, t_work))
                print("{}, {}, {}, {}, {}, {}".format(
                    ks, deg, aa, solver_type, avg_iter, t_work), file=fil)

fil.close()


# Save iterations in json file
with open("ventricle_study_iterates.json", "w") as f:
    import json
    r = json.dumps(iterations)
    f.write(r)

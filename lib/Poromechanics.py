from dolfin import *
from numpy import dot
from petsc4py import PETSc
from lib.Solid import Solid
from lib.Fluid import Fluid
from lib.Pressure import Pressure
from lib.FluidPressure import FluidPressure
from lib.SolidFluidPressure import SolidFluidPressure
from lib.AndersonAcceleration import AndersonAcceleration
from lib.Mixing import MixingAitken, MixingFixed


class PoromechanicsAbstract:
    """
    Abstract class in charge of solving a poromechanics problem.
    """

    def __init__(self, parameters, mesh, dsNeumann_solid, dsNeumann_fluid):
        """
        Abstract constructor
        """

        self.parameters = parameters
        self.mesh = mesh
        self.dsNeumann_solid = dsNeumann_solid
        self.dsNeumann_fluid = dsNeumann_fluid
        self.dim = mesh.topology().dim()

        # First assert that all required variables are in the parameters dictionary
        required_fields = ["t0", "tf", "dt", "output_name", "uref", "vref", "pref",
                           "tolerance residual absolute", "tolerance residual relative",
                           "maxiter", "keep_solutions", "iterations output", "solver_type",
                           "mixing_type"]
        assert all([x in parameters.keys() for x in required_fields]
                   ), "Missing arguments in parameters: {}".format(required_fields)

        self.uref = parameters["uref"]
        self.vref = parameters["uref"]
        self.pref = parameters["uref"]
        self.reference = (self.uref, self.vref, self.pref)

        # Solver parameters
        self.solver_type = parameters["solver_type"]
        self.mixing_type = parameters["mixing_type"]
        self.mixing_weight = 1.
        self.mixing_weight_diff = 1.
        if self.mixing_type in ["fixed-weights"]:
            self.mixing_weight = parameters["mixing_weight_fs"]
            self.mixing_weight_diff = parameters["mixing_weight_diff"]
        self.CC_solver_list = ["cahouet-chabard-fp-s-to-3-way", "diagonal-stab-3-way",
                               "cahouet-chabard-p-fs", "cahouet-chabard-p-fs-to-3-way"]
        allowed_mixings = ["fixed-weights",
                           "Aitken", "Aitken-constrained", "None"]
        if not(self.mixing_type in allowed_mixings):
            print("Mixing does not exist")
            exit()
        if self.solver_type in self.CC_solver_list and not(self.mixing_type in allowed_mixings):
            print("Choose approrpiate mixing.")
            exit()

        self.CC_flag = True if self.solver_type in self.CC_solver_list else False

        # Iteration parameters
        self.tol_residual_abs = parameters["tolerance residual absolute"]
        self.tol_residual_rel = parameters["tolerance residual relative"]
        self.maxiter = parameters["maxiter"]

        # Time parameters
        self.t0 = parameters["t0"]
        self.t = self.t0
        self.tf = parameters["tf"]
        self.dt = parameters["dt"]

        # Scalings
        self.uref = parameters["uref"]
        self.vref = parameters["vref"]
        self.pref = parameters["pref"]

        # Export
        self.xdmf = XDMFFile(
            "output/{}.xdmf".format(parameters["output_name"]))
        self.xdmf.parameters["functions_share_mesh"] = True
        self.xdmf.parameters["flush_output"] = True
        self.xdmf.parameters["rewrite_function_mesh"] = False

        self.export_solutions = parameters["export_solutions"]
        self.keep_solutions = parameters["keep_solutions"]
        if parameters["keep_solutions"]:
            self.solutions = []

        # Log
        self.iterations_output = parameters["iterations output"]
        self.iterations = []

    def setup_anderson_and_mixing(self):

        # Initialize handler for the mixing of the increments
        N_increments = 2 if self.CC_flag else 1
        if self.mixing_type == "Aitken":
            mixing = MixingAitken(N_increments, variant="unconstrained")

        elif self.mixing_type in "fixed-weights":
            mixing = MixingFixed(
                self.parameters["mixing_weight_fs"], self.parameters["mixing_weight_diff"], N_increments)

        elif self.mixing_type == "Aitken-constrained":
            mixing = MixingAitken(N_increments, variant="constrained")

        elif self.mixing_type == "None":
            mixing = MixingFixed(1, 0, N_increments)

        # Anderson acceleration, order<=0 does no acceleration
        anderson = AndersonAcceleration(self.parameters["anderson_order"])

        return mixing, anderson

    def export(self, time):
        """
        Export solutions, assumed to be independent (coming from collapsed spaces).
        """

        self.us_nm1.rename("displacement", "displacement")
        self.uf_nm1.rename("fluid velocity", "fluid velocity")
        self.p_nm1.rename("pressure", "pressure")
        self.xdmf.write(self.us_nm1, time)
        self.xdmf.write(self.uf_nm1, time)
        self.xdmf.write(self.p_nm1, time)

    def solve(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source=None):
        """
        Solve poromechanics problem. Problem loads are assumed to give a function when evaluated at a specific time. For example:
        f_vol = lambda t: Constant((1,1))
        """

        # All functions start as 0 (for now), so no modifications are required.

        from time import time
        current_time = time()
        print("Begining simulation")
        iterations = []

        if self.export_solutions:
            self.export(0)

        if p_source:
            pass
        else:
            def p_source(t): return Constant(0)

        while self.t < self.tf:

            self.t += self.dt
            iter_count = self.solve_time_step(f_vol_solid(
                self.t), f_sur_solid(self.t), f_vol_fluid(self.t), f_sur_fluid(self.t), p_source(self.t))
            iterations.append(iter_count)
            print("-- Solved time t={:.4f} in {:.3f}s and {} iterations".format(self.t,
                                                                                time() - current_time, iter_count))
            if self.export_solutions:
                self.export(self.t)
            current_time = time()

        self.iterations = iterations
        self.avg_iter = self.compute_average_iterations()

    def compute_solid_error(self, us_ref, us):
        return errornorm(us_ref, us, norm_type="H1", degree_rise=0)

    def compute_fluid_error(self, uf_ref, uf):
        return errornorm(uf_ref, uf, norm_type="H1", degree_rise=0)

    def compute_pressure_error(self, p_ref, p):
        return errornorm(p_ref, p, degree_rise=0)

    def compute_average_iterations(self, use_all=True):
        """
        Returns average of iterations. By default use the last half.
        """
        if use_all:
            return sum(self.iterations) / len(self.iterations)
        else:
            # Get last half
            its = self.iterations[int(len(self.iterations) / 2):-1]
            return sum(its) / len(its)

    def compute_average_convergence(self, return_rates=False):
        """
        Compute the average convergence rate of each time step
        """

        print("Computing experimental rates")
        assert self.keep_solutions, "Solutions must be stored to compute average convergence of minimization scheme"

        def compute_error_k(sol_k, sol_n):
            """
            Note: Convergence is measured w.r.t previous timestep, i.e the converging sequence is |sol_k - sol_n|, sol_k -> sol_np1
            """
            us_n, uf_n = sol_n["us"], sol_n["uf"]
            us_k, uf_k = sol_k["us"], sol_k["uf"]

            return sqrt(self.compute_solid_error(us_n, us_k)**2
                        + self.compute_fluid_error(uf_n, uf_k)**2)

        # Iterate over all timesteps
        rates = []
        for i_t, solutions in enumerate(self.solutions):

            rates_t = []
            # Need at least four: a_km1, a_k, a_kp1 and last (solution)
            if len(solutions) < 4:
                continue
            for i in range(len(solutions)):
                # Create space for all quantities
                if i == 0 or i >= len(solutions) - 2:
                    continue
                a_kp1 = compute_error_k(solutions[i + 1], solutions[-1])
                a_k = compute_error_k(solutions[i], solutions[-1])
                a_km1 = compute_error_k(solutions[i - 1], solutions[-1])

                rates_t.append(ln(a_kp1 / a_k) / ln(a_k / a_km1))

            # Append average at this timestep
            if len(rates_t) > 0:
                rates.append(sum(rates_t) / len(rates_t))

        if return_rates:
            return sum(rates) / len(rates), rates
        else:
            return sum(rates) / len(rates)

    def solution2petsc(self, p, uf, us):
        """
        Convert independent Functions into one big PETSc vector for Anderson acceleration. As always, assumed order is
        pressure, fluid, solid.
        """

        # Extract PETSc vectors
        p_vec = p.vector().vec()
        uf_vec = uf.vector().vec()
        us_vec = us.vector().vec()

        # Extract vector dimensions
        np = p_vec.size
        nf = uf_vec.size
        ns = us_vec.size

        # Allocate vector
        out_vec = PETSc.Vec().createSeq(np + nf + ns)

        # Set corresponding components
        out_vec.setValues(range(np), p_vec)
        out_vec.setValues(range(np, np + nf), uf_vec)
        out_vec.setValues(range(np + nf, np + nf + ns), us_vec)

        return out_vec

    def petsc2solution(self, petsc_vec, p, uf, us):
        """
        Assigns petsc_vec as in the solution2petsc method into the corresponding solutions
        """

        # Extract PETSc vectors
        p_vec = p.vector().vec()
        uf_vec = uf.vector().vec()
        us_vec = us.vector().vec()

        # Extract vector dimensions
        np = p_vec.size
        nf = uf_vec.size
        ns = us_vec.size

        petsc_vec.getValues(range(np), values=p_vec)
        petsc_vec.getValues(range(np, np + nf), values=uf_vec)
        petsc_vec.getValues(range(np + nf, np + nf + ns), values=us_vec)

    def inner_product(self, petsc_vec0, petsc_vec1, residual_p, residual_f, residual_s, np, nf, ns):

        p0 = petsc_vec0.getValues(range(np))
        uf0 = petsc_vec0.getValues(range(np, np + nf))
        us0 = petsc_vec0.getValues(range(np + nf, np + nf + ns))

        p1 = petsc_vec1.getValues(range(np))
        uf1 = petsc_vec1.getValues(range(np, np + nf))
        us1 = petsc_vec1.getValues(range(np + nf, np + nf + ns))

        return p0.dot(p1) / residual_p + uf0.dot(uf1) / residual_f + us0.dot(us1) / residual_s

    def hooke(self, ten):
        return 2 * Constant(self.parameters["mu_s"]) * ten + Constant(self.parameters["lmbda"]) * tr(ten) * Identity(self.dim)

    def eps(self, vec):
        return sym(grad(vec))


class Monolithic(PoromechanicsAbstract):

    def __init__(self, parameters, mesh, dsNeumann_solid, dsNeumann_fluid):
        """
        Class in charge of coupling solid and fluid through an iterative splitting scheme.
        """
        super().__init__(parameters, mesh, dsNeumann_solid, dsNeumann_fluid)

        self.solid_fluid_pressure = SolidFluidPressure(
            parameters, mesh, self.dsNeumann_solid, self.dsNeumann_fluid, self.reference)
        print("Solving poromechanics problem with {} dofs".format(self.solid_fluid_pressure.V.dim()))

        # Create placeholders for current solutions
        f_nm1 = Function(self.solid_fluid_pressure.V, name="u - v - p")
        self.us_nm1, self.uf_nm1, self.p_nm1 = f_nm1.split(True)
        self.us_nm2 = self.us_nm1.copy(True)

    def set_bcs(self, bcs_solid, bcs_fluid, bcs_pressure=[]):
        """
        Set boundary conditions to both physics. Assumed to be constant.
        """
        self.solid_fluid_pressure.set_solid_bcs(bcs_solid)
        self.solid_fluid_pressure.set_fluid_bcs(bcs_fluid)
        self.solid_fluid_pressure.set_pressure_bcs(bcs_pressure)

    def setup(self):
        """
        Assemble system matrices for both physics.
        Must be performed after setting boundary conditions, if present.
        """
        self.solid_fluid_pressure.setup()

    def solve_time_step(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source):
        """
        Solve a time step by means of the iterative scheme.
         It requires volumetric and surface loads as input,
        it handles variable update internally
        """

        sol_k = Function(self.solid_fluid_pressure.V)
        us_k, uf_k, p_k = sol_k.split()

        sol_new = Function(self.solid_fluid_pressure.V)
        increment_new1 = Function(self.solid_fluid_pressure.V)
        increment_new2 = Function(self.solid_fluid_pressure.V)

        # Initialize variables are last computed time step
        assign(us_k, self.us_nm1)
        assign(uf_k, self.uf_nm1)
        assign(p_k, self.p_nm1)
        assign(sol_new, sol_k)

        # Norms to normalize errors
        norm_us = norm(self.us_nm1)
        norm_uf = norm(self.uf_nm1)
        norm_p = norm(self.p_nm1)
        if norm_us < 3e-16:
            norm_us = 1
        if norm_uf < 3e-16:
            norm_uf = 1
        if norm_p < 3e-16:
            norm_p = 1
        norm_total = norm_us + norm_uf + norm_p  # Just use l1 vector norm

        # Get reference value
        from numpy.linalg import norm as npnorm

        def solid_norm(vec):
            dofs = self.solid_fluid_pressure.V.sub(0).dofmap().dofs()
            return npnorm(vec.getValues(dofs))

        def fluid_norm(vec):
            dofs = self.solid_fluid_pressure.V.sub(1).dofmap().dofs()
            return npnorm(vec.getValues(dofs))

        def pressure_norm(vec):
            dofs = self.solid_fluid_pressure.V.sub(2).dofmap().dofs()
            return npnorm(vec.getValues(dofs))
        # res0 = self.compute_residual_error(
            # f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k)
        # res0_s, res0_f, res0_p = solid_norm(
            # res0), fluid_norm(res0), pressure_norm(res0)

        res0_s, res0_f, res0_p = self.compute_residual_norm(
            f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k, comp="split")

        if res0_s < 3e-14:
            res0_s = 1
        if res0_f < 3e-14:
            res0_f = 1
        if res0_p < 3e-14:
            res0_p = 1

        mixing, anderson = self.setup_anderson_and_mixing()

        # It is a linear problem with exact Jacobian;
        # nevertheless use an Newton type approach on purpose.
        error_residual_abs, error_residual_rel, it = 1, 1, 0
        if self.keep_solutions:
            # Add a list in last position
            self.solutions.append([])

        self.ref_nrm = [1., 1., 1.]

        self.increment_converged = True

        while error_residual_rel > self.tol_residual_rel \
                and error_residual_abs > self.tol_residual_abs \
                and it < self.maxiter:

            # Step 1: Solve incremental problem
            R = self.compute_residual(
                self.p_nm1, self.uf_nm1,
                self.us_nm1, self.us_nm2,
                p_k, uf_k, us_k,
                f_vol_solid, f_sur_solid,
                f_vol_fluid, f_sur_fluid,
                p_source, "full")

            increment_new1.assign(self.solid_fluid_pressure.solve(R))

            # Step 1b: Apply diffusion stabilization
            if self.CC_flag:
                R_diff = self.compute_residual(
                    self.p_nm1, self.uf_nm1,
                    self.us_nm1, self.us_nm2,
                    p_k, uf_k, us_k,
                    f_vol_solid, f_sur_solid,
                    f_vol_fluid, f_sur_fluid,
                    p_source, "full")

                increment_new2.assign(
                    self.solid_fluid_pressure.solve_diff(R_diff))

            # Step 2: Obtain increment through mixing
            mixing.update_increment(sol_k.vector().vec(
            ), increment_new1.vector().vec(), increment_new2.vector().vec())

            # Update solution
            sol_new.vector().vec().axpy(1.0, mixing.increment)

            # Step 3, give increments to Anderson handler and obtain the current solution
            anderson.get_next_vector(sol_new.vector().vec())

            # Compute errors
            # residual = self.compute_residual_error(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source,
            # sol_new, norm_us=norm_us, norm_uf=norm_uf, norm_p=norm_p)
            res_s, res_f, res_p = self.compute_residual_norm(f_vol_solid, f_sur_solid,
                                                             f_vol_fluid, f_sur_fluid,
                                                             p_source, sol_new, comp="split",
                                                             norm_us=norm_us, norm_uf=norm_uf, norm_p=norm_p)
            residual_norm = sqrt(res_s**2 + res_f**2 + res_p**2)
            # res_s, res_f, res_p = solid_norm(residual), fluid_norm(
            # residual), pressure_norm(residual)

            # Use same tolerance for absolute and relative errors
            error_residual_abs = residual_norm
            error_residual_rel = res_s / res0_s + res_f / res0_f + res_p / res0_p

            assign(sol_k, sol_new)

            it += 1
            if self.iterations_output:
                print("------- Iteration {}, res abs={:.3E}, res rel={:.3E}".format(
                    it, error_residual_abs, error_residual_rel))

            if self.keep_solutions:
                # Add to current list the last solution as a dictionary
                self.solutions[-1].append({"us": us_k.copy(True),
                                           "uf": uf_k.copy(True), "p": p_k.copy(True)})

            if error_residual_abs > 1e30:
                it = self.maxiter
                print("DIVERGED")
                break

        if it == self.maxiter:
            print("WARNING: Maximum iterations attained")

        # Finally update internal functions
        assign(self.us_nm2, self.us_nm1)
        assign(self.us_nm1, us_k)
        assign(self.uf_nm1, uf_k)
        assign(self.p_nm1, p_k)

        return it

    def compute_solid_error(self, us_ref, us):
        return errornorm(us_ref, us, norm_type="L2", degree_rise=0)

    def compute_fluid_error(self, uf_ref, uf):
        return errornorm(uf_ref, uf, norm_type="L2", degree_rise=0)

    def compute_pressure_error(self, p_ref, p):
        return errornorm(p_ref, p, degree_rise=0)

    def compute_residual(self, p_nm1_, uf_nm1_, us_nm1_, us_nm2_, p_km1_, uf_km1_, us_km1_, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="None"):

        if comp == None:
            print("No components have been chosen in the computation of the residual.")
            sys.exit()

        # Scale variables
        us_nm1 = Constant(self.uref) * us_nm1_
        us_nm2 = Constant(self.uref) * us_nm2_
        uf_nm1 = Constant(self.vref) * uf_nm1_
        p_nm1 = Constant(self.pref) * p_nm1_
        us_km1 = Constant(self.uref) * us_km1_
        uf_km1 = Constant(self.vref) * uf_km1_
        p_km1 = Constant(self.pref) * p_km1_

        """
        Compute fluid residual as in step 0
        """
        v, w, q = TestFunctions(self.solid_fluid_pressure.V)

        dt = Constant(self.dt)
        idt = 1. / dt
        phi0 = self.solid_fluid_pressure.phi0
        phi0s = 1 - phi0
        ikf = inv(self.solid_fluid_pressure.kf)
        mu_f = self.solid_fluid_pressure.mu_f
        rhof = self.solid_fluid_pressure.rhof
        rhos = self.solid_fluid_pressure.rhos
        ks = self.solid_fluid_pressure.ks

        # Compute solid residual
        rhs_s_n = dot(fs_vol, v) * dx + dot(fs_sur, v) * \
            self.solid_fluid_pressure.dsNeumannSolid

        M_s = dot(Constant(rhos * idt**2) * phi0s *
                  (us_km1 - 2. * us_nm1 + us_nm2), v)

        A_s = inner(self.solid_fluid_pressure.hooke(
            self.solid_fluid_pressure.eps(us_km1)), self.solid_fluid_pressure.eps(v))
        DT_s = - div(phi0s * v) * p_km1
        K_s = - phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), v)
        lhs_s_n = (M_s + K_s + A_s + DT_s) * dx

        r_s = rhs_s_n - lhs_s_n

        # Compute fluid residual
        rhs_f_n = dot(ff_vol, w) * dx + dot(ff_sur, w) * \
            self.solid_fluid_pressure.dsNeumannFluid

        M_f = dot(Constant(rhof * idt) * phi0 * (uf_km1 - uf_nm1), w)
        A_f = 2 * mu_f * \
            inner(phi0 * self.solid_fluid_pressure.eps(uf_km1),
                  self.solid_fluid_pressure.eps(w))
        DT_f = - div(phi0 * w) * p_km1
        K_f = phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), w)
        lhs_f_n = (M_f + A_f + DT_f + K_f) * dx

        r_f = rhs_f_n - lhs_f_n

        # Compute pressure residual
        rhs_p_n = Constant(1 / rhof) * p_source * q * dx

        D_sf = div(idt * phi0s * (us_km1 - us_nm1) + phi0 * uf_km1) * q
        M_p = phi0s**2 / Constant(ks * dt) * (p_km1 - p_nm1) * q
        lhs_p_n = (M_p + D_sf) * dx

        r_p = rhs_p_n - lhs_p_n

        self.rhs = assemble(rhs_s_n + rhs_f_n + rhs_p_n)

        if comp == "full":
            R = r_s + r_f + r_p
        elif comp == "solid":
            R = r_s
        elif comp == "fluid":
            R = r_f
        elif comp == "pressure":
            R = r_p

        return R

    def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp="full", norm_us=1, norm_uf=1, norm_p=1):
        # def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp = "full", norm_us=1, norm_uf=1, norm_p=1):

        # # Scale variables
        # us_nm1 = Constant(self.uref) * us_nm1_
        # us_nm2 = Constant(self.uref) * us_nm2_
        # uf_nm1 = Constant(self.vref) * uf_nm1_
        # p_nm1 = Constant(self.pref) * p_nm1_
        # us_km1 = Constant(self.uref) * us_km1_
        # uf_km1 = Constant(self.vref) * uf_km1_
        # p_km1 = Constant(self.pref) * p_km1_
        """
        Compute fluid residual as in step 0
        """

        us_k, uf_k, p_k = solution_current.split()
        if comp == "full":
            R = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                      us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="full")
            R = assemble(R)
            bc_s = [DirichletBC(b)
                    for b in self.solid_fluid_pressure.solid_bcs]
            bc_f = [DirichletBC(b)
                    for b in self.solid_fluid_pressure.fluid_bcs]
            for b in bc_s + bc_f:
                b.apply(R)
            return norm(R)
        elif comp == "split":
            Rs = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="solid")
            Rs = assemble(Rs)
            Rf = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="fluid")
            Rf = assemble(Rf)
            Rp = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="pressure")
            Rp = assemble(Rp)

            bc_s = [DirichletBC(b)
                    for b in self.solid_fluid_pressure.solid_bcs]
            bc_f = [DirichletBC(b)
                    for b in self.solid_fluid_pressure.fluid_bcs]
            for b in bc_s + bc_f:
                b.apply(Rs)
                b.apply(Rf)
                b.apply(Rp)
            return norm(Rs), norm(Rf), norm(Rp)


class TwoWay(PoromechanicsAbstract):

    def __init__(self, parameters, mesh, dsNeumann_solid, dsNeumann_fluid):
        """
        Class in charge of coupling solid and fluid through an iterative splitting scheme.
        """

        super().__init__(parameters, mesh, dsNeumann_solid, dsNeumann_fluid)

        # Initialize all physics
        self.solid = Solid(parameters, mesh, dsNeumann_solid, self.reference)
        self.fluid_pressure = FluidPressure(
            parameters, mesh, dsNeumann_fluid, self.reference)

        self.V_full = FunctionSpace(mesh,
                                    MixedElement(VectorElement('CG', mesh.ufl_cell(), parameters["fe_degree_solid"]),
                                                 VectorElement(
                                        'CG', mesh.ufl_cell(), parameters["fe_degree_fluid_velocity"]),
                                        FiniteElement('CG', mesh.ufl_cell(), parameters["fe_degree_pressure"])))
        print("Solving poromechanics problem with {} dofs".format(self.V_full.dim()))

        # Create placeholders for current solutions
        self.vf_p = Function(self.fluid_pressure.V)
        self.us = Function(self.solid.V)

        self.uf_nm1, self.p_nm1 = self.vf_p.split(True)
        self.us_nm1 = self.us.copy(True)
        self.us_nm2 = self.us_nm1.copy(True)

    def set_bcs(self, bcs_solid, bcs_fluid, bcs_pressure):
        """
        Set boundary conditions to both physics. Assumed to be constant.
        """
        self.solid.set_bcs(bcs_solid)
        self.fluid_pressure.set_fluid_bcs(bcs_fluid)
        self.fluid_pressure.set_pressure_bcs(bcs_pressure)

    def setup(self):
        """
        Assemble system matrices for both physics. Must be performed after setting boundary conditions, if present.
        """
        self.solid.setup()
        self.fluid_pressure.setup()

        # Allocate vectors
        self.sol_k = Function(self.V_full)
        us_k, uf_k, p_k = self.sol_k.split()

        self.sol_new = Function(self.V_full)
        self.increment = Function(self.V_full)
        self.increment_new1 = Function(self.V_full)
        self.increment_new2 = Function(self.V_full)
        self.Rs = PETScVector()
        self.Rf = PETScVector()
        self.Rp = PETScVector()
        self.Rfp = PETScVector()

    def solve_time_step(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source):
        """
        Solve a time step by means of the iterative scheme. It requires volumetric and surface loads as input,
        it handles variable update internally
        """

        sol_k = self.sol_k
        us_k, uf_k, p_k = sol_k.split()

        sol_new = self.sol_new

        # increment = self.increment
        increment_new1 = self.increment_new1
        increment_new2 = self.increment_new2

        Rs = self.Rs
        Rfp = self.Rfp

        # Initialize variables are last computed time step
        assign(us_k, self.us_nm1)
        assign(uf_k, self.uf_nm1)
        assign(p_k, self.p_nm1)
        assign(sol_new, sol_k)

        # Norms to normalize errors
        norm_us = norm(self.us_nm1)
        norm_uf = norm(self.uf_nm1)
        norm_p = norm(self.p_nm1)
        if norm_us < 3e-16:
            norm_us = 1
        if norm_uf < 3e-16:
            norm_uf = 1
        if norm_p < 3e-16:
            norm_p = 1
        norm_total = norm_us + norm_uf + norm_p  # Just use l1 vector norm

        res0_s, res0_f, res0_p = self.compute_residual_norm(
            f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k, comp="split")

        if res0_s < 3e-14:
            res0_s = 1
        if res0_f < 3e-14:
            res0_f = 1
        if res0_p < 3e-14:
            res0_p = 1

        mixing, anderson = self.setup_anderson_and_mixing()

        # It is a linear problem with exact Jacobian;
        # nevertheless use an Newton type approach on purpose.
        error_residual_abs, error_residual_rel, it = 1, 1, 0
        if self.keep_solutions:
            # Add a list in last position
            self.solutions.append([])

        self.ref_nrm = [1., 1., 1.]

        self.increment_converged = True
        self.residual_converged = True

        while error_residual_rel > self.tol_residual_rel \
                and error_residual_abs > self.tol_residual_abs \
                and it < self.maxiter:

            # Step 1: Solve incremental problem
            self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "fluid-pressure")

            df, dp = self.fluid_pressure.solve_fixed_stress_stab(Rfp)

            self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "solid", df_k=df, dp_k=dp)
            ds = self.solid.solve(Rs)

            assign(increment_new1.sub(0), ds)
            assign(increment_new1.sub(1), df)
            assign(increment_new1.sub(2), dp)

            # Step 1b: Apply diffusion stabilization
            if self.CC_flag:
                self.compute_residual(
                    self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "fluid-pressure")

                df, dp = self.fluid_pressure.solve_diffusion_stab(Rfp)

                self.compute_residual(
                    self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "solid", df_k=df, dp_k=dp)
                ds = self.solid.solve(Rs)
                assign(increment_new2.sub(0), ds)
                assign(increment_new2.sub(1), df)
                assign(increment_new2.sub(2), dp)

            # Step 2: Obtain increment through mixing
            mixing.update_increment(sol_k.vector().vec(
            ), increment_new1.vector().vec(), increment_new2.vector().vec())

            # Update solution
            sol_new.vector().vec().axpy(1.0, mixing.increment)

            # Step 3, give increments to Anderson handler and obtain the current solution
            anderson.get_next_vector(sol_new.vector().vec())

            # Compute errors
            res_s, res_f, res_p = self.compute_residual_norm(
                f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k, comp="split")
            residual_norm = sqrt(res_s**2 + res_f**2 + res_p**2)

            # Use same tolerance for absolute and relative errors
            error_residual_abs = residual_norm
            error_residual_rel = res_s / res0_s + res_f / res0_f + res_p / res0_p

            assign(sol_k, sol_new)

            it += 1
            if self.iterations_output:
                print("------- Iteration {}, res abs={:.3E}, res rel={:.3E}".format(
                    it, error_residual_abs, error_residual_rel))

            if self.keep_solutions:
                # Add to current list the last solution as a dictionary
                self.solutions[-1].append({"us": us_k.copy(True),
                                           "uf": uf_k.copy(True), "p": p_k.copy(True)})

            if error_residual_abs > 1e30:
                it = self.maxiter
                print("DIVERGED")
                break

        # Finally update internal functions
        assign(self.us_nm2, self.us_nm1)
        assign(self.us_nm1, us_k)
        assign(self.uf_nm1, uf_k)
        assign(self.p_nm1, p_k)

        return it

    def compute_residual(self, p_nm1_, uf_nm1_, us_nm1_, us_nm2_, p_km1_, uf_km1_, us_km1_, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="None", df_k=None, dp_k=None):

        if comp == None:
            print("No components have been chosen in the computation of the residual.")
            sys.exit()

        # Scale variables
        us_nm1 = Constant(self.uref) * us_nm1_
        us_nm2 = Constant(self.uref) * us_nm2_
        uf_nm1 = Constant(self.vref) * uf_nm1_
        p_nm1 = Constant(self.pref) * p_nm1_
        us_km1 = Constant(self.uref) * us_km1_
        uf_km1 = Constant(self.vref) * uf_km1_
        p_km1 = Constant(self.pref) * p_km1_

        """
        Compute fluid residual as in step 0
        """
        v = TestFunction(self.solid.V)
        w, q = TestFunctions(self.fluid_pressure.V)

        dt = Constant(self.dt)
        idt = 1. / dt
        phi0 = self.fluid_pressure.phi0
        phi0s = 1 - phi0
        ikf = inv(self.solid.kf)
        mu_f = self.fluid_pressure.mu_f
        rhof = self.fluid_pressure.rhof
        rhos = self.solid.rhos
        ks = self.fluid_pressure.ks

        # Compute solid residual
        rhs_s_n = dot(fs_vol, v) * dx + dot(fs_sur, v) * \
            self.solid.dsNeumann

        M_s = dot(Constant(rhos * idt**2) * phi0s *
                  (us_km1 - 2. * us_nm1 + us_nm2), v)
        A_s = inner(self.solid.hooke(
            self.solid.eps(us_km1)), self.solid.eps(v))
        DT_s = - div(phi0s * v) * p_km1
        K_s = - phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), v)
        lhs_s_n = (M_s + K_s + A_s + DT_s) * dx

        r_s = rhs_s_n - lhs_s_n

        if df_k:
            if dp_k:
                r_s -= (-div(phi0s * v) * dp_k - phi0 **
                        2 * dot(ikf * df_k, v)) * dx

        # Compute fluid residual
        rhs_f_n = dot(ff_vol, w) * dx + dot(ff_sur, w) * \
            self.fluid_pressure.dsNeumann

        M_f = dot(Constant(rhof * idt) * phi0 * (uf_km1 - uf_nm1), w)
        A_f = 2 * mu_f * phi0 * \
            inner(self.solid.eps(uf_km1),
                  self.solid.eps(w))
        DT_f = - div(phi0 * w) * p_km1
        K_f = phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), w)
        lhs_f_n = (M_f + A_f + DT_f + K_f) * dx

        r_f = rhs_f_n - lhs_f_n

        # Compute pressure residual
        rhs_p_n = Constant(1 / rhof) * p_source * q * dx

        D_sf = div(idt * phi0s * (us_km1 - us_nm1) + phi0 * uf_km1) * q
        M_p = phi0s**2 / Constant(ks * dt) * (p_km1 - p_nm1) * q
        lhs_p_n = (M_p + D_sf) * dx

        r_p = rhs_p_n - lhs_p_n

        if comp == "full":
            R = r_s + r_f + r_p
            assemble(R, tensor=self.R)
        elif comp == "solid":
            R = r_s
            assemble(R, tensor=self.Rs)
        elif comp == "fluid":
            R = r_f
            assemble(R, tensor=self.Rf)
        elif comp == "pressure":
            R = r_p
            assemble(R, tensor=self.Rp)
        elif comp == "fluid-pressure":
            R = r_f + r_p
            assemble(R, tensor=self.Rfp)
        else:
            raise Exception(
                "Wrong component from PoromechanicsTwoWay.compute_residual")
        return R

    def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp="full", norm_us=1, norm_uf=1, norm_p=1):
        # def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp = "full", norm_us=1, norm_uf=1, norm_p=1):

        # # Scale variables
        # us_nm1 = Constant(self.uref) * us_nm1_
        # us_nm2 = Constant(self.uref) * us_nm2_
        # uf_nm1 = Constant(self.vref) * uf_nm1_
        # p_nm1 = Constant(self.pref) * p_nm1_
        # us_km1 = Constant(self.uref) * us_km1_
        # uf_km1 = Constant(self.vref) * uf_km1_
        # p_km1 = Constant(self.pref) * p_km1_
        """
        Compute fluid residual as in step 0
        """

        us_k, uf_k, p_k = solution_current.split()
        if comp == "full":
            R = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                      us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="full")
            return norm(R)
        elif comp == "split":
            Rs = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="solid")
            Rf = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="fluid")
            Rp = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                       us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="pressure")

            Rs = assemble(Rs)
            Rf = assemble(Rf)
            Rp = assemble(Rp)
            bc_s = [DirichletBC(b) for b in self.solid.bcs]
            bc_f = [DirichletBC(b) for b in self.fluid_pressure.fluid_bcs]
            for b in bc_s:
                b.apply(Rs)
            for b in bc_f:
                b.apply(Rf)
            return norm(Rs), norm(Rf), norm(Rp)

        # dt = Constant(self.dt)
        # idt = 1. / dt
        # phi0 = self.fluid_pressure.phi0
        # phi0s = 1 - phi0
        # ikf = inv(self.fluid_pressure.kf)
        # mu_f = self.fluid_pressure.mu_f
        # rhof = self.fluid_pressure.rhof
        # rhos = self.solid.rhos
        # ks = self.fluid_pressure.ks

        # V = self.V_full
        # us, uf, p = TrialFunctions(V)
        # vs, vf, q = TestFunctions(V)

        # if not self.A_monolithic_assembled:

        #     a_s = (Constant(rhos * idt**2) * phi0s * dot(us, vs)
        #            + inner(self.solid.hooke(self.solid.eps(us)),
        #                    self.solid.eps(vs))
        #            - p * div((1 - phi0) * vs)
        #            - phi0 ** 2 * dot(ikf * (uf - idt * us), vs)) * dx

        #     a_f = (Constant(rhof * idt) * phi0 * dot(uf, vf)
        #            + 2. * Constant(mu_f) * phi0 *
        #            inner(self.solid.eps(uf), self.solid.eps(vf))
        #            - p * div(phi0 * vf)
        #            + phi0 ** 2 * dot(ikf * (uf - idt * us), vf)) * dx

        #     a_p = (phi0s**2 / Constant(ks * dt) * p * q
        #            + div(phi0 * uf) * q
        #            + div(phi0s * idt * us) * q) * dx

        #     assemble(a_s + a_f + a_p, tensor=self.A_monolithic)
        #     self.A_monolithic_assembled = True
        #     for b in self.bcs_monolithic:
        #         b.apply(self.A_monolithic)

        # # Compute solid residual
        # rhs_s_n = dot(fs_vol, vs) * dx + dot(fs_sur, vs) * \
        #     self.solid.dsNeumann

        # M_s = dot(Constant(rhos * idt**2) * phi0s *
        #           (- 2. * self.us_nm1 + self.us_nm2), vs)
        # K_s = - phi0**2 * dot(ikf * idt * self.us_nm1, vs)
        # lhs_s_n = (M_s + K_s) * dx

        # r_s = rhs_s_n - lhs_s_n

        # # Compute fluid residual
        # rhs_f_n = dot(ff_vol, vf) * dx + dot(ff_sur, vf) * \
        #     self.fluid_pressure.dsNeumann

        # M_f = dot(Constant(rhof * idt) * phi0 * (-self.uf_nm1), vf)
        # K_f = phi0**2 * dot(ikf * idt * self.us_nm1, vf)
        # lhs_f_n = (M_f + K_f) * dx

        # r_f = rhs_f_n - lhs_f_n

        # # Compute pressure residual
        # rhs_p_n = Constant(1 / rhof) * p_source * q * dx

        # D_sf = div(idt * phi0s * (-self.us_nm1)) * q
        # M_p = phi0s**2 / Constant(ks * dt) * (-self.p_nm1) * q
        # lhs_p_n = (M_p + D_sf) * dx

        # r_p = rhs_p_n - lhs_p_n

        # assemble(r_s + r_f + r_p, tensor=self.b_monolithic)

        # for b in self.bcs_monolithic:
        #     b.apply(self.b_monolithic)

        # # Compute weighted norm
        # res = self.A_monolithic * solution_current.vector() - self.b_monolithic

        # if comp == "full":
        #     return norm(res)
        # elif comp == "split":
        #     from numpy.linalg import norm as npnorm
        #     res = res.vec()
        #     res_s = res.getValues(self.V_full.sub(0).dofmap().dofs())
        #     res_f = res.getValues(self.V_full.sub(1).dofmap().dofs())
        #     res_p = res.getValues(self.V_full.sub(2).dofmap().dofs())
        #     return npnorm(res_s), npnorm(res_f), npnorm(res_p)

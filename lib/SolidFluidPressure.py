from dolfin import *
from lib.AAR import AAR
from petsc4py import PETSc


class SolidFluidPressure:

    def __init__(self, parameters, mesh, dsNeumannSolid, dsNeumannFluid, reference=[1, 1, 1]):
        """
        Create instance of fluid model, which handles the fluid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """

        self.parameters = parameters
        # First assert that all required variables are in the parameters dictionary
        required_fields = [
            "fe_degree_solid",
            "fe_degree_fluid_velocity",
            "fe_degree_pressure",
            "mu_s",
            "lmbda",
            "mu_f",
            "rhos",
            "rhof",
            "phi0",
            "ks",
            "kf",
            "dt",
            "solver_type"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters"

        # Solver
        self.solver_type = parameters["solver_type"]

        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree_u = parameters["fe_degree_solid"]
        self.fe_degree_v = parameters["fe_degree_fluid_velocity"]
        self.fe_degree_p = parameters["fe_degree_pressure"]
        self.V = FunctionSpace(mesh,
                               MixedElement(VectorElement('CG', mesh.ufl_cell(), self.fe_degree_u),
                                            VectorElement(
                                                'CG', mesh.ufl_cell(), self.fe_degree_v),
                                            FiniteElement('CG', mesh.ufl_cell(), self.fe_degree_p)))

        self.dsNeumannSolid = dsNeumannSolid
        self.dsNeumannFluid = dsNeumannFluid

        # Model parameters
        self.mu_s = parameters["mu_s"]
        self.lmbda = parameters["lmbda"]
        self.mu_f = parameters["mu_f"]
        self.rhos = parameters["rhos"]
        self.rhof = parameters["rhof"]
        self.phi0 = parameters["phi0"]
        self.ks = parameters["ks"]
        self.kf = parameters["kf"]
        self.Kdr = 2. * self.mu_s / self.dim + self.lmbda

        # Time parameters
        self.dt = parameters["dt"]

        # Scalings
        self.uref = reference[0]
        self.vref = reference[1]
        self.pref = reference[2]

        # Stabilization factors required for diagonal stabilization
        self.betas = 1.
        self.betaf = 1.
        self.betap = 1.
        if "betas" in parameters:
            self.betas = parameters["betas"]
        if "betaf" in parameters:
            self.betaf = parameters["betaf"]
        if "betap" in parameters:
            self.betap = parameters["betap"]
        # print("Chosen stabilization: {} {} {}",
        #       self.betas, self.betaf, self.betap)

    def set_solid_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant.
        """
        self.solid_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def set_fluid_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.fluid_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def set_pressure_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.pressure_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def get_bcs(self, diff=False):

        bcs = []
        if self.solid_bcs:
            bcs = bcs + self.solid_bcs
        if self.fluid_bcs:
            bcs = bcs + self.fluid_bcs
        if diff and self.pressure_bcs:
            bcs = bcs + self.pressure_bcs
        return bcs

    def setup(self):
        """
        Assemble problem matrix, incorporate essential boundary conditions as well.
        """
        us, vf, p = TrialFunctions(self.V)
        v, w, q = TestFunctions(self.V)

        # Scale trial functions
        us = Constant(self.uref) * us
        vf = Constant(self.vref) * vf
        p = Constant(self.pref) * p

        phi0 = self.phi0
        phis = 1 - phi0  # Solid fraction
        dt = self.dt
        rhos = self.rhos
        rhof = self.rhof
        ks = self.ks
        kf = self.kf
        mu_f = self.mu_f
        mu_s = self.mu_s
        lmbda = self.lmbda
        dim = self.dim

        a_p_diff = 0

        # Base forms are later used to assemble monolithic matrix, used for preconditioner testing.
        a_s_base = (Constant(rhos / dt**2) * phis * dot(us, v)
                    + inner(self.hooke(self.eps(us)), self.eps(v))
                    - p * div(phis * v)
                    - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

        a_f_base = (Constant(rhof / dt) * phi0 * dot(vf, w)
                    + 2. * Constant(mu_f) *
                    inner(phi0 * self.eps(vf), self.eps(w))
                    - p * div(phi0 * w)
                    + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1 / dt) * us), w)) * dx

        a_p_base = (phis**2 / Constant(ks * dt) * p * q
                    + div(phi0 * vf) * q
                    + div(phis * Constant(1. / dt) * us) * q) * dx

        if (self.solver_type == "monolithic"):

            a_s = a_s_base

            a_f = a_f_base

            a_p = a_p_base

        elif (self.solver_type == "undrained"):

            # print("Warning: For large ks this results in a Jacobian for the solid system equivalent with the Jacobian for nearly incompressible materials. Chosen discretization is not locking-free!")

            # Stabilization
            N = Constant(ks) / phis**2

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   + N * div(phis * us) * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (- Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), w)) * dx

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + div(phi0 * vf) * q
                   + div(phis * Constant(1. / dt) * us) * q) * dx

        elif (self.solver_type == "diagonal-stab"):

            #print("Note: This seems to work for realistic data!")

            beta_s_hat = self.betas

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0**2 * dot(inv(kf) * (vf - Constant((1. + beta_s_hat) * 1. / dt) * us), v)) * dx

            beta_f_hat = self.betaf

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + Constant(1. + beta_f_hat) * phi0**2 * dot(inv(kf) * vf, w)) * dx

            beta_p_hat = self.betap
            beta_p = beta_p_hat * phis**2 / \
                Constant(dt * (2. * mu_s / dim + lmbda))

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + beta_p * p * q
                   + div(phi0 * vf) * q) * dx

        elif (self.solver_type == "diagonal-stab-3-way"):

            #print("Note: This seems to work for realistic data!")

            beta_s_hat = self.betas

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0**2 * dot(inv(kf) * (vf - Constant((1. + beta_s_hat) * 1. / (dt)) * us), v)) * dx

            beta_f_hat = self.betaf

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + Constant(1. + beta_f_hat) * phi0**2 * dot(inv(kf) * (vf), w)) * dx

            beta_p_hat = self.betap
            beta_p = beta_p_hat * phis**2 / \
                Constant(dt * (2. * mu_s / dim + lmbda))
            beta_CC1 = phi0 / Constant(2. * mu_f / dim)
            #beta_CC2 = phi0**2 *  inv(phi0 * Constant(rhof / dt) + Constant(1. + beta_f_hat) * phi0**2 * inv(kf))
            beta_CC2 = 1e0 * inv(Constant(rhof / dt) / phi0 + inv(kf))

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_p + beta_CC1) * p * q) * dx

            a_p_diff = (phis**2 / Constant(ks * dt) * p * q
                        + beta_p * p * q
                        + dot(beta_CC2 * grad(p), grad(q))) * dx

        elif (self.solver_type == "fixed-stress-fp-s"):

            #print("Note: This seems to work for realistic data!")

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0**2 * dot(inv(kf) * (vf - Constant(1. / (2. * dt)) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0**2 * dot(inv(kf) * (vf), w)) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + beta_A * p * q
                   + div(phi0 * vf) * q) * dx

        elif (self.solver_type == "cahouet-chabard-fp-s-to-3-way"):

            # print("Works but requires suitable mixing for improved performance compared to the fs-p-f-s split. Pressure BCs are required on the complement of the flux Dirichlet BCs.")

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf), w)) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)
            beta_k = 1e0 * inv(Constant(rhof / dt) / phi0 + inv(kf))

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_A + beta_M) * p * q) * dx

            a_p_diff = (phis**2 / Constant(ks * dt) * p * q
                        + beta_A * p * q
                        + dot(beta_k * grad(p), grad(q))) * dx

        elif (self.solver_type == "fixed-stress-sp-f"):

            print("Warning: For small mu_f, the stabilization added due to beta_M is resulting in slow convergence.")

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (- Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1 / dt) * us), w)) * dx

            beta_M = phi0 / Constant(2. * mu_f / dim)

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + beta_M * p * q
                   + div(phis * Constant(1. / dt) * us) * q) * dx

        elif (self.solver_type == "fixed-stress-p-fs"):

            print("Warning: For small mu_f, the stabilization added due to beta_M is resulting in slow convergence.")

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1 / dt) * us), w)) * dx

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + div(phi0 * vf) * q
                   + div(phis * Constant(1. / dt) * us) * q) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_A + beta_M) * p * q) * dx

        elif (self.solver_type == "fixed-stress-p-f-s"):

            print("Warning: For small mu_f, the stabilization added due to beta_M is resulting in slow convergence.")

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf), w)) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_A + beta_M) * p * q) * dx

        elif (self.solver_type == "cahouet-chabard-p-fs"):

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf - Constant(1 / dt) * us), w)) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            det = (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) \
                * (Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) \
                - (phi0**2 * Constant(inv(kf)))**2
            beta_k = Constant(dt) / det * \
                ((Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) * (phis / Constant(dt))**2
                 + 2. * phi0**2 * Constant(inv(kf)) *
                 phi0 * (phis / Constant(dt))
                 + (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) * phi0**2)

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_A + beta_M) * p * q) * dx

            a_p_diff = (phis**2 / Constant(ks * dt) * p * q
                        + dot(beta_k * grad(p), grad(q))) * dx

        elif (self.solver_type == "cahouet-chabard-p-fs-to-3-way"):

            a_s = (Constant(rhos / dt**2) * phis * dot(us, v)
                   + inner(self.hooke(self.eps(us)), self.eps(v))
                   - p * div(phis * v)
                   - phi0 ** 2 * dot(inv(kf) * (vf - Constant(1. / dt) * us), v)) * dx

            a_f = (Constant(rhof / dt) * phi0 * dot(vf, w)
                   + 2. * Constant(mu_f) * phi0 *
                   inner(self.eps(vf), self.eps(w))
                   - p * div(phi0 * w)
                   + phi0 ** 2 * dot(inv(kf) * (vf), w)) * dx

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            det = (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) \
                * (Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) \
                - (phi0**2 * Constant(inv(kf)))**2
            beta_k = Constant(dt) / det * \
                ((Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) * (phis / Constant(dt))**2
                 + 2. * phi0**2 * Constant(inv(kf)) *
                 phi0 * (phis / Constant(dt))
                 + (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) * phi0**2)

            a_p = (phis**2 / Constant(ks * dt) * p * q
                   + (beta_A + beta_M) * p * q) * dx

            a_p_diff = (phis**2 / Constant(ks * dt) * p * q
                        + dot(beta_k * grad(p), grad(q))) * dx

        # Set matrix and solver. No time dependent terms, so it can be assembled only once.
        A = PETScMatrix()
        assemble(a_s + a_f + a_p, tensor=A)
        self.apply_solid_bc(A)
        self.apply_fluid_bc(A)
        A_base = PETScMatrix()
        assemble(a_s_base + a_f_base + a_p_base, tensor=A_base)
        self.apply_solid_bc(A_base)
        self.apply_fluid_bc(A_base)
        self.A = A
        self.A_base = A_base

        if self.parameters["method"] == "iterative":
            self.solver = PETScKrylovSolver()
            self.solver.set_from_options()
            if self.parameters["splitting as preconditioner"]:
                # TODO: Inefficient, this is already computed for residuals.
                self.solver.set_operators(
                    self.A_base, self.A)  # Set operators (A, P)
            else:
                self.solver.set_operator(self.A)
        elif self.parameters["method"] == "AAR":
            self.solver = AAR(order=self.parameters["AAR order"], p=self.parameters["AAR p"],
                              omega=self.parameters["AAR omega"], beta=self.parameters["AAR beta"],
                              x0=None, matM=self.A.mat(), matA=self.A_base.mat(),
                              atol=self.parameters["tolerance residual absolute"],
                              rtol=self.parameters["tolerance residual relative"],
                              maxiter=self.parameters["iterative maxiter"])
        else:  # Direct by default
            self.LU = LUSolver(self.A, 'mumps')

        # Same for the second solver based on diffusion stabilization - only required for particular solvers
        if self.solver_type in ["cahouet-chabard-fp-s-to-3-way", "cahouet-chabard-p-fs", "cahouet-chabard-p-fs-to-3-way", "diagonal-stab-3-way"]:
            A_diff = assemble(a_s + a_f + a_p_diff)
            A_diff.mat().setBlockSize(7)
            self.apply_solid_bc(A_diff)
            self.apply_fluid_bc(A_diff)
            self.apply_pressure_bc(A_diff)

            self.A_diff = A_diff
            if self.parameters["method"] == "iterative":
                self.solver_diff = PETScKrylovSolver()
                self.solver.set_from_options()
                # self.solver_diff.set_operator(self.A_diff)
                if self.parameters["splitting as preconditioner"]:
                    self.solver.set_operators(
                        self.A_base, self.A_diff)  # Set operators (A, P)
                else:
                    self.solver.set_operator(self.A_diff)
            elif self.parameters["method"] == "AAR":
                self.solver_diff = AAR(order=self.parameters["AAR order"], p=self.parameters["AAR p"],
                                       omega=self.parameters["AAR omega"], beta=self.parameters["AAR beta"],
                                       x0=None, matM=self.A_diff.mat(), matA=self.A_base.mat(),
                                       atol=self.parameters["tolerance residual absolute"],
                                       rtol=self.parameters["tolerance residual relative"],
                                       maxiter=self.parameters["iterative maxiter"])
            else:  # Direct by default
                self.LU_diff = LUSolver(self.A_diff, 'mumps')

    def solve(self, f):
        """
        Solve current iteration given external loads and previous iterations
        """

        F = PETScVector()
        assemble(f, tensor=F)
        self.apply_solid_bc(F)
        self.apply_fluid_bc(F)

        sol = Function(self.V)

        if self.parameters["method"] in ("iterative", "AAR"):
            its = self.solver.solve(sol.vector(), F)
            if its and self.parameters["iterations solver output"]:
                print("\tSolved in {} iterations".format(its))
        else:
            self.LU.solve(sol.vector(), F)
        return sol

    def solve_diff(self, f):
        """
        Solve current iteration given external loads and previous iterations
        """

        F = assemble(f)
        self.apply_solid_bc(F)
        self.apply_fluid_bc(F)
        self.apply_pressure_bc(F)

        sol = Function(self.V)

        if self.parameters["method"] in ("iterative", "AAR"):
            its = self.solver.solve(sol.vector(), F)
            print("\tSolved in {} iterations".format(its))
        else:
            self.LU_diff.solve(sol.vector(), F)
        return sol

    def hooke(self, ten):
        return 2 * self.mu_s * ten + self.lmbda * tr(ten) * Identity(self.dim)

    def eps(self, vec):
        return sym(grad(vec))

    def apply_solid_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.solid_bcs:
            for b in self.solid_bcs:
                b.apply(obj)

    def apply_fluid_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.fluid_bcs:
            for b in self.fluid_bcs:
                b.apply(obj)

    def apply_pressure_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.pressure_bcs:
            for b in self.pressure_bcs:
                b.apply(obj)

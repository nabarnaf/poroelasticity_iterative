from dolfin import *


class Pressure:

    def __init__(self, parameters, mesh, reference=[1., 1., 1.]):
        """
        Create instance of fluid model, which handles the fluid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """

        # First assert that all required variables are in the parameters dictionary
        required_fields = ["fe_degree_pressure",
                           "phi0",
                           "mu_f",
                           "kf",
                           "ks",
                           "rhos",
                           "rhof",
                           "dt"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters"

        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree_p = parameters["fe_degree_pressure"]
        self.V = self.V_diff = self.V_fs = FunctionSpace(
            mesh, "CG", self.fe_degree_p)

        # Model parameters
        self.phi0 = parameters["phi0"]
        self.mu_f = parameters["mu_f"]
        self.kf = parameters["kf"]
        self.ks = parameters["ks"]
        self.rhos = parameters["rhos"]
        self.rhof = parameters["rhof"]
        self.Kdr = parameters["Kdr"]

        # Time parameters
        self.dt = parameters["dt"]

        # Construct stabilization terms
        lmbda = 0.5
        self.beta = self.dim * self.phi0 / Constant(2 * lmbda * 2 * self.mu_f / self.dim) + \
            (1 - self.phi0)**2 / Constant(2 * lmbda * self.dt * self.Kdr)

        # Added for testing only, previously computed value is theoretically correct.
        if "beta" in parameters.keys():
            self.beta = Constant(parameters["beta"])

        if "B" in parameters.keys():
            self.B = parameters["B"]
        else:
            self.B = None

        # Construct blocks for M block matrix and its inverse (constructed as a generalized Cramer rule)
        Id = Identity(self.dim)
        self.m00 = Constant(self.rhos / self.dt ** 2) * (1 - self.phi0) * \
            Id + Constant(1 / self.dt) * self.phi0**2 * inv(self.kf) * Id
        self.m01 = -self.phi0**2 * inv(self.kf) * Id
        self.m10 = -self.phi0**2 * inv(self.kf) * Id
        self.m11 = self.rhof * self.phi0 * Id + \
            Constant(self.dt) * self.phi0**2 * inv(self.kf) * Id

        # Inverse right matrix given by inv(AD - BC) for a block matrix [A B; C D]
        M_factor = inv(self.m00 * self.m11 - self.m01 * self.m10)
        self.invm00 = self.m11 * M_factor
        self.invm01 = -self.m01 * M_factor
        self.invm10 = -self.m10 * M_factor
        self.invm11 = self.m00 * M_factor

        self.pref = reference[2]

    def bilinearBlockform(self, tuple_lhs, tuple_rhs):
        """
        Perform the product of the stabilization block matrix B (see article) for the form
        (1/dt[phi0s.I, dt.phi.I].T inv(M)[phi0s.I, dt.phi0.I][u; v], [u; v])
        for M of size 2d x 2d and u,v of size d. It doesn't require the block components as they are precomputed in the constructor as mij and invmij.
        """

        # First modify arguments with vectors [phi0s, dt.phi0]
        tuple_lhs = ((1 - self.phi0) *
                     tuple_lhs[0], Constant(self.dt) * self.phi0 * tuple_lhs[1])
        tuple_rhs = ((1 - self.phi0) / Constant(self.dt) *
                     tuple_rhs[0], self.phi0 * tuple_rhs[1])  # Divided by dt
        m00 = self.invm00
        m01 = self.invm01
        m10 = self.invm10
        m11 = self.invm11

        # Apply inverse to tuple_lhs
        lhs0 = m00 * tuple_lhs[0] + m01 * tuple_lhs[1]
        lhs1 = m10 * tuple_lhs[0] + m11 * tuple_lhs[1]

        # If test flag, then just return grad grad weighted by B.
        if self.B:
            return Constant(self.B) * dot(tuple_lhs[0], tuple_rhs[0])
        return dot(lhs0, tuple_rhs[0]) + dot(lhs1, tuple_rhs[1])

    def set_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def setup(self):
        """
        Assemble problem matrix, incorporate essential boundary conditions as well.
        """
        p_fs = Constant(self.pref) * TrialFunction(self.V_fs)
        q_fs = TestFunction(self.V_fs)

        p_diff = Constant(self.pref) * TrialFunction(self.V_diff)
        q_diff = TestFunction(self.V_diff)

        phis = 1 - self.phi0  # Solid fraction

        # Fixed-stress stabilization
        ap_fs = (phis**2 / Constant(self.ks * self.dt) * p_fs * q_fs
                 + self.beta * p_fs * q_fs) * dx
        # Diffusion stabilization
        ap_diff = (phis**2 / Constant(self.ks * self.dt) * p_diff * q_diff
                   + self.bilinearBlockform((grad(p_diff), grad(p_diff)), (grad(q_diff), grad(q_diff)))) * dx

        A_fs = PETScMatrix()
        assemble(ap_fs, tensor = A_fs)
        A_diff = PETScMatrix()
        assemble(ap_diff, tensor = A_diff)
        self.apply_bc(A_diff)

        # Set matrix and solver. No time dependent terms, so it can be assembled only once.
        self.A_fs = A_fs
        self.LU_fs = LUSolver(self.A_fs)
        self.A_diff = A_diff
        self.LU_diff = LUSolver(self.A_diff)

    def resetup(self, pref):
        self.pref = pref
        self.setup()

    def solve_fixed_stress_stab(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """

        sol = Function(self.V_fs)
        self.LU_fs.solve(sol.vector(), F)
        return sol

    def solve_diffusion_stab(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """

        self.apply_bc(F)

        sol = Function(self.V_diff)
        self.LU_diff.solve(sol.vector(), F)
        return sol

    def __eps(self, vec):
        return sym(grad(vec))

    def apply_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.bcs:
            for b in self.bcs:
                b.apply(obj)

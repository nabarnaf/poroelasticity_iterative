#
# This class is DEPRECATED. Use Mixing instead, this is kept for compatibility with old tests.
#
from dolfin import *
from copy import deepcopy
import numpy as np

# TODO remove result and update directly solution and increments

def triple2scipy(vectors):
    """
    Takes a triple of vectors and returns a long column vector.
    Note that parallel gives incorrect results, as local vectors would give different results on each process. As this part is not
    particularly intensirve, vectors could be gathered on one processor first and then used to compute the results.
    """
    vp = vectors[0].vector().get_local()
    vf = vectors[1].vector().get_local()
    vs = vectors[2].vector().get_local()

    from numpy import hstack
    return hstack([vp, vf, vs])

def axpy_one_vec(result, alpha0, x0):

    # TODO replace this with petsc versions
    for j, comp in enumerate(result):
        comp.vector()[:] = alpha0 * x0[j].vector()

def axpy_two_vecs(result, alpha0, x0, alpha1, incr1):

    # TODO replace this with petsc versions
    for j, comp in enumerate(result):
        comp.vector()[:] = alpha0 * x0[j].vector() + alpha1 * incr1[j].vector()

class MixingMonolithicFixed:

    def __init__(self, weight1, weight2):
        self.weight1 = weight1
        self.weight2 = weight2

    def solve_iteration(self, it, solution_new, increment_new, solution_old, increment1, increment2=None):
        """
        Mixing using fixed weights - convex combinations have to be defined by the user if wished
        """

        if increment2==None:
            axpy_one_vec(increment_new, self.weight1, increment1)

        else:
            axpy_two_vecs(increment_new, self.weight1, increment1, self.weight2, increment2)

        axpy_two_vecs(solution_new, 1., solution_old, 1., increment_new)
        
class MixingMonolithicAitken:

    def __init__(self, variant = "unconstrained"):
        self.variant = variant

        self.solutions = []

        self.alpha  = 1.
        self.alpha1 = 0.5
        self.alpha2 = 0.5

    def append_solution(self, sol):
        new_sol = [f.copy(True) for f in sol]
        self.solutions.append(new_sol)

    def solve_iteration(self, it, solution_new, increment_new, solution_old, increment1, increment2=None):
        """
        Compute one iteration of Aitken on the increments
        """

        if (increment2 == None):
            """
            Aitken for a single increment
            """

            if (it > 0):

                sol = triple2scipy(solution_old) - triple2scipy(self.prev_solution_old)
                inc = triple2scipy(increment1)   - triple2scipy(self.prev_increment1)

                nrm  = np.dot(inc, inc)
                proj = np.dot(sol, inc)

                # If the problem seems badly scaled keep weights from previous iteration
                if nrm > 3e-16:
                    self.alpha = -proj / nrm

            self.prev_solution_old = [f.copy(True) for f in solution_old]
            self.prev_increment1 = [f.copy(True) for f in increment1]

            axpy_one_vec(increment_new, self.alpha, increment1)
            axpy_two_vecs(solution_new, 1., solution_old, 1., increment_new)

        else:

            """
            Aitken for two increments as performed by Simone Deparis et al.
            """
            if (it > 0):

                sol  = triple2scipy(solution_old) - triple2scipy(self.prev_solution_old)
                inc1 = triple2scipy(increment1)   - triple2scipy(self.prev_increment1)
                inc2 = triple2scipy(increment2)   - triple2scipy(self.prev_increment2)

                if self.variant == "constrained":
                    """
                    Solve for weights that sum up to 1 resulting in the smallest possible increment.
                    """
                    temp = inc1 - inc2
                    temp2 = sol + inc2

                    nrm  = np.dot(temp,temp)
                    proj = np.dot(temp2,temp)

                    # If the problem seems badly scaled keep weights from previous iteration
                    if nrm > 3e-16:
                        self.alpha1 = -proj / nrm
                        self.alpha2 = 1. - self.alpha1

                elif self.variant == "unconstrained":
                    """
                    No additional constraint for weights which includes a line search type relaxation aiming for the smallest possible increment.
                    """

                    Inc = np.transpose(np.array([inc1, inc2]))
                    Q, R = np.linalg.qr(Inc)
                    alphas = np.linalg.solve(R, -Q.T @ sol)
                    self.alpha1 = alphas[0]
                    self.alpha2 = alphas[1]

                else:

                    print("Chosen variant for Aitken does not exist.")

            self.prev_increment2 = [f.copy(True) for f in increment2]
            self.prev_solution_old = [f.copy(True) for f in solution_old]
            self.prev_increment1 = [f.copy(True) for f in increment1]

            axpy_two_vecs(increment_new, self.alpha1, increment1, self.alpha2, increment2)

            axpy_two_vecs(solution_new, 1., solution_old, 1., increment_new)

class MixingMonolithicLineSearch:
    #"""
    #Optimal choice of weights based on the residuals
    #"""

    def __init__(self, variant = "unconstrained"):
        self.variant = variant

        self.alpha1 = 0.5
        self.alpha2 = 0.5

    def solve_iteration(self, it, solution_new, increment_new, solution_old, increment1, increment2, residual_old, incremental_residual1, incremental_residual2, ref_nrm = [1., 1., 1.]):
        """
        Setup least squares problem and solve by QR factorization
        """
        old_res  = np.hstack([f for f in residual_old])
        inc_res1 = np.hstack([f for f in incremental_residual1])
        inc_res2 = np.hstack([f for f in incremental_residual2])

        for j in range(3):
            old_res[j] /= ref_nrm[j]
            inc_res1[j] /= ref_nrm[j]
            inc_res2[j] /= ref_nrm[j]

        if self.variant == "constrained-sum":
            """
            Solve for weights that sum up to 1 resulting in the smallest possible residual.
            """
            temp = inc_res1 - inc_res2
            temp2 = old_res + inc_res2

            nrm  = np.dot(temp,temp)
            proj = np.dot(temp2,temp)

            # If the problem seems badly scaled keep weights from previous iteration
            if nrm > 3e-16:
                self.alpha1 = -proj / nrm
                self.alpha2 = 1. - self.alpha1

        elif self.variant == "constrained-1":
            """
            Solve for second weight (while fixing the first to 1) resulting in the smallest possible residual.
            """
            temp = old_res + inc_res1

            nrm  = np.dot(inc_res2, inc_res2)
            proj = np.dot(inc_res2, temp)

            # If the problem seems badly scaled keep weights from previous iteration
            self.alpha1 = 1.
            if nrm > 3e-16:
                self.alpha2 = -proj / nrm
            else:
                self.alpha2 = 0.

        elif self.variant == "successive":
            """
            Solve for first and second weight successively (start with first) resulting in the smallest possible residual.
            """
            # Start with the first direction
            nrm  = np.dot(inc_res1, inc_res1)
            proj = np.dot(inc_res1, old_res)

            if nrm > 3e-16:
                self.alpha1 = -proj / nrm
            else:
                self.alpha1 = 1.

            # Now the second direction
            temp = old_res + self.alpha1 * inc_res1
            nrm  = np.dot(inc_res2, inc_res2)
            proj = np.dot(inc_res2, temp)

            if nrm > 3e-16:
                self.alpha2 = -proj / nrm
            else:
                self.alpha2 = 0.

        elif self.variant == "unconstrained":
            """
            No additional constraint for weights which includes a line search type relaxation aiming for the smallest possible residual.
            """
            IncRes = np.transpose(np.array([inc_res1, inc_res2]))
            Q, R = np.linalg.qr(IncRes)
            alphas = np.linalg.solve(R, -Q.T @ old_res)
            self.alpha1 = alphas[0]
            self.alpha2 = alphas[1]

            print(R)

        else:

            print("Chosen variant for Line search does not exist.")

        print([self.alpha1, self.alpha2])
        axpy_two_vecs(increment_new, self.alpha1, increment1, self.alpha2, increment2)
        axpy_two_vecs(solution_new, 1., solution_old, 1., increment_new)

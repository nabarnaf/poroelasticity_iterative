#
# This class is DEPRECATED. Use Poromechanics instead, this is kept for compatibility with old tests.
#
from dolfin import *
from numpy import dot
from petsc4py import PETSc
from lib.Solid import Solid
from lib.Fluid import Fluid
from lib.Pressure import Pressure
from lib.FluidPressure import FluidPressure
from lib.SolidFluidPressure import SolidFluidPressure
from lib.AndersonAcceleration import AndersonAcceleration
from lib.MixingMonolithic import MixingMonolithicFixed, MixingMonolithicAitken, MixingMonolithicLineSearch

# Petsc vector adapters directly copied from PoromechanicsAbstract


def solution2petsc(p, uf, us):
    """
    Convert independent Functions into one big PETSc vector for Anderson acceleration. As always, assumed order is
    pressure, fluid, solid.
    """

    # Extract PETSc vectors
    p_vec = p.vector().vec()
    uf_vec = uf.vector().vec()
    us_vec = us.vector().vec()

    # Extract vector dimensions
    np = p_vec.size
    nf = uf_vec.size
    ns = us_vec.size

    # Allocate vector
    out_vec = PETSc.Vec().createSeq(np + nf + ns)

    # Set corresponding components
    out_vec.setValues(range(np), p_vec)
    out_vec.setValues(range(np, np + nf), uf_vec)
    out_vec.setValues(range(np + nf, np + nf + ns), us_vec)

    return out_vec


def petsc2solution(petsc_vec, p, uf, us):
    """
    Assigns petsc_vec as in the solution2petsc method into the corresponding solutions
    """

    # Extract PETSc vectors
    p_vec = p.vector().vec()
    uf_vec = uf.vector().vec()
    us_vec = us.vector().vec()

    # Extract vector dimensions
    np = p_vec.size
    nf = uf_vec.size
    ns = us_vec.size

    petsc_vec.getValues(range(np), values=p_vec)
    petsc_vec.getValues(range(np, np + nf), values=uf_vec)
    petsc_vec.getValues(range(np + nf, np + nf + ns), values=us_vec)

class Monolithic:

    def __init__(self, parameters, mesh, dsNeumann_solid, dsNeumann_fluid):
        """
        Class in charge of coupling solid and fluid through an iterative splitting scheme.
        """

        self.parameters = parameters

        # First assert that all required variables are in the parameters dictionary
        required_fields = ["t0", "tf", "dt", "output_name", "tolerance increment",
                           "maxiter", "keep_solutions"]
        assert all([x in parameters.keys() for x in required_fields]
                   ), "Missing arguments in parameters: {}".format(required_fields)

        # Iteration parameters
        self.tol = parameters["tolerance increment"]
        self.restol = parameters["tolerance residual"]
        self.maxiter = parameters["maxiter"]
        self.anderson_order = parameters["anderson_order"]
        self.anderson_delay = parameters["anderson_delay"]

        # Time parameters
        self.t0 = parameters["t0"]
        self.tf = parameters["tf"]
        self.dt = parameters["dt"]

        # Scalings
        self.uref = parameters["uref"]
        self.vref = parameters["vref"]
        self.pref = parameters["pref"]
        reference = (self.uref, self.vref, self.pref)

        # Solver parameters
        self.verbose = parameters["verbose"]
        self.solver_type = parameters["solver_type"]
        self.mixing_type = parameters["mixing_type"]
        self.mixing_weight = 1.
        self.mixing_weight_diff = 1.
        if self.mixing_type in ["fixed-weights"]:
            self.mixing_weight = parameters["mixing_weight_fs"]
            self.mixing_weight_diff = parameters["mixing_weight_diff"]
        self.CC_solver_list = ["cc-fp-s", "cahouet-chabard-fp-s-to-3-way",
                               "cahouet-chabard-p-fs", "cahouet-chabard-p-fs-to-3-way", "diagonal-stab-3-way"]
        allowed_mixings = ["fixed-weights",
                           "Aitken", "Aitken-constrained", "None"]
        CC_allowed_mixings = ["fixed-weights", "Aitken", "Aitken-constrained"]
        if not(self.mixing_type in allowed_mixings):
            print("Mixing does not exist")
            exit()
        if self.solver_type in self.CC_solver_list and not(self.mixing_type in allowed_mixings):
            print("Choose approrpiate mixing.")
            exit()

        # Initialize all physics
        self.solid_fluid_pressure = SolidFluidPressure(
            parameters, mesh, dsNeumann_solid, dsNeumann_fluid, reference)

        # Create placeholders for current solutions
        f_nm1 = Function(self.solid_fluid_pressure.V, name="u - v - p")
        self.us_nm1, self.uf_nm1, self.p_nm1 = f_nm1.split()
        self.us_nm2 = self.us_nm1.copy(True)

        # Export
        self.xdmf = XDMFFile(
            "output/{}.xdmf".format(parameters["output_name"]))
        self.xdmf.parameters["functions_share_mesh"] = True
        self.xdmf.parameters["flush_output"] = True

        self.export_solutions = parameters["export_solutions"]
        self.keep_solutions = parameters["keep_solutions"]
        if parameters["keep_solutions"]:
            self.solutions = []

        self.iterations = []  # Placeholder for number of iterations in each timestep

    def set_bcs(self, bcs_solid, bcs_fluid, bcs_pressure=[]):
        """
        Set boundary conditions to both physics. Assumed to be constant.
        """
        self.solid_fluid_pressure.set_solid_bcs(bcs_solid)
        self.solid_fluid_pressure.set_fluid_bcs(bcs_fluid)
        self.solid_fluid_pressure.set_pressure_bcs(bcs_pressure)

    def setup(self):
        """
        Assemble system matrices for both physics. Must be performed after setting boundary conditions, if present.
        """
        self.solid_fluid_pressure.setup()

    def solve_time_step(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid):
        """
        Solve a time step by means of the iterative scheme. It requires volumetric and surface loads as input,
        it handles variable update internally
        """

        f = Function(self.solid_fluid_pressure.V)
        us_k, uf_k, p_k = f.split()

        # Debug!!
        us_new = us_k.copy(True)
        uf_new = uf_k.copy(True)
        p_new = p_k.copy(True)

        # Initialize variables are last computed time step
        assign(us_k, self.us_nm1)
        assign(uf_k, self.uf_nm1)
        assign(p_k, self.p_nm1)

        # Norms to normalize errors
        norm_us = norm(self.us_nm1, norm_type="L2")
        norm_uf = norm(self.uf_nm1, norm_type="L2")
        norm_p = norm(self.p_nm1)
        if norm_us < 3e-16:
            norm_us = 1
        if norm_uf < 3e-16:
            norm_uf = 1
        if norm_p < 3e-16:
            norm_p = 1

        # Initialize handler for the mixing of the increments
        if self.mixing_type in ["Aitken"]:
            mixing = MixingMonolithicAitken(variant="unconstrained")

        elif self.mixing_type in ["fixed-weights"]:
            mixing = MixingMonolithicFixed(
                self.mixing_weight, self.mixing_weight_diff)

        elif self.mixing_type in ["Aitken-constrained"]:
            mixing = MixingMonolithicAitken(variant="constrained")

        elif self.mixing_type in ["None"]:
            mixing = MixingMonolithicFixed(1., 0.)

        # Line search based / Residual minimizing relaxation techniques have proven to not work sufficiently well
        #mixing = MixingMonolithicLineSearch(variant = "unconstrained")
        #mixing = MixingMonolithicLineSearch(variant = "constrained-sum")
        #mixing = MixingMonolithicLineSearch(variant = "constrained-1")
        #mixing = MixingMonolithicLineSearch(variant = "successive")

        # Anderson acceleration
        anderson = AndersonAcceleration(self.parameters)
        xk_list = [solution2petsc(p_k, uf_k, us_k)]
        fk_list = []

        # It is a linear problem with exact Jacobian; nevertheless use an Newton type approach on purpose.
        error, residual, it = 1, 1, 0
        if self.keep_solutions:
            # Add a list in last position
            self.solutions.append([])

        self.ref_nrm = [1., 1., 1.]

        self.increment_converged = True
        self.residual_converged = True

        while error > self.tol and residual > self.restol and it < self.maxiter:

            # Step 1: Solve incremental problem
            R = self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "full")

            ds, df, dp = self.solid_fluid_pressure.solve(R)

            # Step 1b: Apply diffusion stabilization
            if self.solver_type in self.CC_solver_list:

                R_diff = self.compute_residual(
                    self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "full")

                ds_diff, df_diff, dp_diff = self.solid_fluid_pressure.solve_diff(
                    R_diff)
                increment_diff = (ds_diff, df_diff, dp_diff)

            # Step 2: Update solution
            solution_k = (us_k, uf_k, p_k)
            increment_k = (ds, df, dp)
            us_new = us_k.copy(True)
            uf_new = uf_k.copy(True)
            p_new = p_k.copy(True)
            solution_new = (us_new, uf_new, p_new)
            ds_new = ds.copy(True)
            df_new = df.copy(True)
            dp_new = dp.copy(True)
            increment_new = (ds_new, df_new, dp_new)

            # For solvers involving an alternative stabilization include mixing of the two increments;
            # Otherwise, just update in straight forward fashion
            if self.solver_type in self.CC_solver_list:

                # Increment-based mixing
                mixing.solve_iteration(
                    it, solution_new, increment_new, solution_k, increment_k, increment_diff)
            else:

                # Increment based relaxation
                if not(self.mixing_type == "None"):
                    mixing.solve_iteration(
                        it, solution_new, increment_new, solution_k, increment_k)

                for j, comp in enumerate(solution_new):
                    comp.vector()[:] = solution_k[j].vector() + \
                        increment_new[j].vector()

            # Step 3, give increments to Anderson handler and obtain the current solution
            # NOTE: Mixing has been performed without petsc vectors and is included here directly compared to the origin of this code.

            # Give the mixed increment
            fk = solution2petsc(dp_new, df_new, ds_new)
            fk_list.append(fk)
            gk = solution2petsc(p_new, uf_new, us_new)
            petsc_new = anderson.solve_iteration(it, xk_list, fk_list, gk=gk)
            xk_list.append(petsc_new)
            petsc2solution(petsc_new, p_new, uf_new, us_new)

            # Step 4: Compute separate residuals - only required when using residuals for termination of the method
            R_new = self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_new, uf_new, us_new, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "full")
            self.solid_fluid_pressure.apply_solid_bc(R_new)
            self.solid_fluid_pressure.apply_fluid_bc(R_new)

            R_s_new = self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_new, uf_new, us_new, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "solid")
            self.solid_fluid_pressure.apply_solid_bc(R_s_new)

            R_f_new = self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_new, uf_new, us_new, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "fluid")
            self.solid_fluid_pressure.apply_fluid_bc(R_f_new)

            R_p_new = self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_new, uf_new, us_new, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, "pressure")

            res_s = norm(R_s_new)
            res_f = norm(R_f_new)
            res_p = norm(R_p_new)

            # if it < 2:
            #    self.ref_nrm = [res_s, res_f, res_p]

            res_total1 = norm(R_new)
            res_total2 = (res_s**2 + res_f**2 + res_p**2)**0.5
            residual = res_total1

            error_solid = self.compute_solid_error(us_new, us_k) / norm_us
            error_fluid = self.compute_fluid_error(uf_new, uf_k) / norm_uf
            error_press = self.compute_pressure_error(p_new, p_k) / norm_p

            error = error_solid + error_fluid + error_press

            assign(us_k, us_new)
            assign(uf_k, uf_new)
            assign(p_k, p_new)

            it += 1
            print("------- Anderson iteration {}, error={}".format(it, error))
            if self.verbose:
                print("------- solid error={} {} {}".format(error_solid, norm_us, res_s))
                print("------- fluid error={} {} {}".format(error_fluid, norm_uf, res_f))
                print(
                    "------- pressure error={} {} {}".format(error_press, norm_p, res_p))
                print("------- total residual={} {}".format(res_total1, res_total2))
                print("\n")

            if self.keep_solutions:
                # Add to current list the last solution as a dictionary
                self.solutions[-1].append({"us": us_k.copy(True),
                                           "uf": uf_k.copy(True), "p": p_k.copy(True)})

        if error > self.tol and residual > self.restol:
            print("WARNING: method not converged")

        if error > self.tol:
            self.increment_converged = False

        if residual > self.restol:
            self.residual_converged = False

        # Finally update internal functions
        assign(self.us_nm2, self.us_nm1)
        assign(self.us_nm1, us_k)
        assign(self.uf_nm1, uf_k)
        assign(self.p_nm1, p_k)

        return it

    def solve(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid):
        """
        Solve poromechanics problem. Problem loads are assumed to give a function when evaluated at a specific time. For example:
        f_vol = lambda t: Constant((1,1))
        """

        # All functions start as 0 (for now), so no modifications are required.

        t = self.t0

        from time import time
        current_time = time()
        print("Beginning simulation")
        iterations = []

        if self.export_solutions:
            self.export(0)

        self.increment_converged = True
        self.residual_converged = True

        while t < self.tf and (self.residual_converged or self.increment_converged):

            t += self.dt
            iter_count = self.solve_time_step(f_vol_solid(
                t), f_sur_solid(t), f_vol_fluid(t), f_sur_fluid(t))
            iterations.append(iter_count)
            print("-- Solved time t={} in {:.3f}s and {} iterations".format(t,
                                                                            time() - current_time, iter_count))

            if self.export_solutions:
                self.export(t)
            current_time = time()

        print("Increments / Residual converged? {} / {}".format(
            self.increment_converged, self.residual_converged))

        self.iterations = iterations
        self.avg_iter = self.compute_average_iterations(True)

    def export(self, time):
        self.xdmf.write(self.us_nm1, time)
        self.xdmf.write(self.uf_nm1, time)
        self.xdmf.write(self.p_nm1, time)

    def compute_solid_error(self, us_ref, us):
        return errornorm(us_ref, us, norm_type="L2", degree_rise=0)

    def compute_fluid_error(self, uf_ref, uf):
        return errornorm(uf_ref, uf, norm_type="L2", degree_rise=0)

    def compute_pressure_error(self, p_ref, p):
        return errornorm(p_ref, p, degree_rise=0)

    def compute_residual(self, p_nm1_, uf_nm1_, us_nm1_, us_nm2_, p_km1_, uf_km1_, us_km1_, fs_vol, fs_sur, ff_vol, ff_sur, comp="None"):

        if comp == None:
            print("No components have been chosen in the computation of the residual.")
            sys.exit()

        # Scale variables
        us_nm1 = Constant(self.uref) * us_nm1_
        us_nm2 = Constant(self.uref) * us_nm2_
        uf_nm1 = Constant(self.vref) * uf_nm1_
        p_nm1 = Constant(self.pref) * p_nm1_
        us_km1 = Constant(self.uref) * us_km1_
        uf_km1 = Constant(self.vref) * uf_km1_
        p_km1 = Constant(self.pref) * p_km1_

        """
        Compute fluid residual as in step 0
        """
        v, w, q = TestFunctions(self.solid_fluid_pressure.V)

        dt = Constant(self.dt)
        idt = 1. / dt
        phi0 = self.solid_fluid_pressure.phi0
        phi0s = 1 - phi0
        ikf = inv(self.solid_fluid_pressure.kf)
        mu_f = self.solid_fluid_pressure.mu_f
        rhof = self.solid_fluid_pressure.rhof
        rhos = self.solid_fluid_pressure.rhos
        ks = self.solid_fluid_pressure.ks

        # Compute solid residual
        rhs_s_n = dot(fs_vol, v) * dx + dot(fs_sur, v) * \
            self.solid_fluid_pressure.dsNeumannSolid

        M_s = dot(Constant(rhos * idt**2) * phi0s *
                  (us_km1 - 2. * us_nm1 + us_nm2), v)
        A_s = inner(self.solid_fluid_pressure.hooke(
            self.solid_fluid_pressure.eps(us_km1)), self.solid_fluid_pressure.eps(v))
        DT_s = - div(phi0s * v) * p_km1
        K_s = - phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), v)
        lhs_s_n = (M_s + K_s + A_s + DT_s) * dx

        r_s = rhs_s_n - lhs_s_n

        # Compute fluid residual
        rhs_f_n = dot(ff_vol, w) * dx + dot(ff_sur, w) * \
            self.solid_fluid_pressure.dsNeumannFluid

        M_f = dot(Constant(rhof * idt) * phi0 * (uf_km1 - uf_nm1), w)
        A_f = 2 * mu_f * phi0 * \
            inner(self.solid_fluid_pressure.eps(uf_km1),
                  self.solid_fluid_pressure.eps(w))
        DT_f = - div(phi0 * w) * p_km1
        K_f = phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), w)
        lhs_f_n = (M_f + A_f + DT_f + K_f) * dx

        r_f = rhs_f_n - lhs_f_n

        # Compute pressure residual
        rhs_p_n = 0

        D_sf = div(idt * phi0s * (us_km1 - us_nm1) + phi0 * uf_km1) * q
        M_p = phi0s**2 / Constant(ks * dt) * (p_km1 - p_nm1) * q
        lhs_p_n = (M_p + D_sf) * dx

        r_p = rhs_p_n - lhs_p_n

        if comp == "full":
            R = assemble(r_s + r_f + r_p)
        elif comp == "solid":
            R = assemble(r_s)
        elif comp == "fluid":
            R = assemble(r_f)
        elif comp == "pressure":
            R = assemble(r_p)

        return R

    def compute_incremental_residual(self, p_km1_, uf_km1_, us_km1_, comp="None"):

        if comp == None:
            print("No components have been chosen in the computation of the residual.")
            sys.exit()

        # Scale variables
        us_km1 = Constant(self.uref) * us_km1_
        uf_km1 = Constant(self.vref) * uf_km1_
        p_km1 = Constant(self.pref) * p_km1_

        """
        Compute fluid residual as in step 0
        """
        v, w, q = TestFunctions(self.solid_fluid_pressure.V)

        dt = Constant(self.dt)
        idt = 1. / dt
        phi0 = self.solid_fluid_pressure.phi0
        phi0s = 1 - phi0
        ikf = inv(self.solid_fluid_pressure.kf)
        mu_f = self.solid_fluid_pressure.mu_f
        rhof = self.solid_fluid_pressure.rhof
        rhos = self.solid_fluid_pressure.rhos
        ks = self.solid_fluid_pressure.ks

        # Compute solid residual
        M_s = dot(Constant(rhos * idt**2) * phi0s * us_km1, v)
        A_s = inner(self.solid_fluid_pressure.hooke(
            self.solid_fluid_pressure.eps(us_km1)), self.solid_fluid_pressure.eps(v))
        DT_s = - div(phi0s * v) * p_km1
        K_s = - phi0**2 * dot(ikf * (uf_km1 - idt * us_km1), v)
        lhs_s_n = (M_s + K_s + A_s + DT_s) * dx

        r_s = - lhs_s_n

        # Compute fluid residual
        M_f = dot(Constant(rhof * idt) * phi0 * uf_km1, w)
        A_f = 2 * mu_f * phi0 * \
            inner(self.solid_fluid_pressure.eps(uf_km1),
                  self.solid_fluid_pressure.eps(w))
        DT_f = - div(phi0 * w) * p_km1
        K_f = phi0**2 * dot(ikf * (uf_km1 - idt * us_km1), w)
        lhs_f_n = (M_f + A_f + DT_f + K_f) * dx

        r_f = - lhs_f_n

        # Compute pressure residual
        D_sf = div(idt * phi0s * us_km1 + phi0 * uf_km1) * q
        M_p = phi0s**2 / Constant(ks * dt) * p_km1 * q
        lhs_p_n = (M_p + D_sf) * dx

        r_p = - lhs_p_n

        if comp == "full":
            R = assemble(r_s + r_f + r_p)
        elif comp == "solid":
            R = assemble(r_s)
        elif comp == "fluid":
            R = assemble(r_f)
        elif comp == "pressure":
            R = assemble(r_p)

        return R

    def compute_average_iterations(self, use_all=False):
        """
        Returns average of iterations. By default use the last half.
        """
        if use_all:
            if self.residual_converged or self.increment_converged:
                return sum(self.iterations) / len(self.iterations)
            else:
                return "-"
        else:
            # Get last half
            its = self.iterations[int(len(self.iterations) / 2):-1]
            return sum(its) / len(its)

    def compute_average_convergence(self, return_rates=False):
        """
        Compute the average convergence rate of each time step
        """

        print("Computing experimental rates")
        assert self.keep_solutions, "Solutions must be stored to compute average convergence of minimization scheme"

        def compute_error_k(sol_k, sol_n):
            """
            Note: Convergence is measured w.r.t previous timestep, i.e the converging sequence is |sol_k - sol_n|, sol_k -> sol_np1
            """
            us_n, uf_n = sol_n["us"], sol_n["uf"]
            us_k, uf_k = sol_k["us"], sol_k["uf"]

            return sqrt(self.compute_solid_error(us_n, us_k)**2
                        + self.compute_fluid_error(uf_n, uf_k)**2)

        # Iterate over all timesteps
        rates = []
        for i_t, solutions in enumerate(self.solutions):

            rates_t = []
            # Need at least four: a_km1, a_k, a_kp1 and last (solution)
            if len(solutions) < 4:
                continue
            for i in range(len(solutions)):
                # Create space for all quantities
                if i == 0 or i >= len(solutions) - 2:
                    continue
                a_kp1 = compute_error_k(solutions[i + 1], solutions[-1])
                a_k = compute_error_k(solutions[i], solutions[-1])
                a_km1 = compute_error_k(solutions[i - 1], solutions[-1])

                rates_t.append(ln(a_kp1 / a_k) / ln(a_k / a_km1))

            # Append average at this timestep
            if len(rates_t) > 0:
                rates.append(sum(rates_t) / len(rates_t))

        if return_rates:
            return sum(rates) / len(rates), rates
        else:
            return sum(rates) / len(rates)

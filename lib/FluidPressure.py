from dolfin import *
from lib.AAR import AAR
from petsc4py import PETSc


class FluidPressure:

    def __init__(self, parameters, mesh, dsNeumann, reference=[1, 1, 1]):
        """
        Create instance of fluid model, which handles the fluid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """

        # First assert that all required variables are in the parameters dictionary
        self.parameters = parameters
        required_fields = ["fe_degree_fluid_velocity",
                           "fe_degree_pressure", "mu_f", "rhof", "phi0", "ks", "kf", "dt"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters"

        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree_u = parameters["fe_degree_fluid_velocity"]
        self.fe_degree_p = parameters["fe_degree_pressure"]

        self.V = FunctionSpace(mesh,
                               MixedElement(VectorElement('CG', mesh.ufl_cell(), self.fe_degree_u),
                                            FiniteElement('CG', mesh.ufl_cell(), self.fe_degree_p)))
        self.dsNeumann = dsNeumann

        # Model parameters
        self.mu_f = parameters["mu_f"]
        self.rhof = parameters["rhof"]
        self.phi0 = parameters["phi0"]
        self.ks = parameters["ks"]
        self.kf = parameters["kf"]
        self.rhos = parameters["rhos"]
        self.Kdr = parameters["Kdr"]
        self.solver_type = parameters["solver_type"]
        self.pressure_bcs = None

        self.betaf = 1.
        self.betap = 1.
        if "betaf" in parameters:
            self.betaf = parameters["betaf"]
        if "betap" in parameters:
            self.betap = parameters["betap"]

        # Time parameters
        self.dt = parameters["dt"]

        # Scaling
        self.vref = reference[1]
        self.pref = reference[2]

        # self.beta = self.dim * self.phi0 / Constant(2 * self.mu_f) + \
        #    (1 - self.phi0)**2 / Constant(self.dt * self.Kdr)

        self.beta = (1 - self.phi0)**2 / (self.dt * self.Kdr)

        # Construct blocks for M block matrix and its inverse (constructed as a generalized Cramer rule)
        # Id = Identity(self.dim)
        # self.m00 = Constant(self.rhos / self.dt ** 2) * (1 - self.phi0) * \
        #    Id + Constant(1 / self.dt) * self.phi0**2 * inv(self.kf) * Id
        # self.m01 = -self.phi0**2 * inv(self.kf) * Id
        # self.m10 = -self.phi0**2 * inv(self.kf) * Id
        # self.m11 = self.rhof * self.phi0 * Id + \
        #    Constant(self.dt) * self.phi0**2 * inv(self.kf) * Id

        # Inverse right matrix given by inv(AD - BC) for a block matrix [A B; C D]
        # M_factor = inv(self.m00 * self.m11 - self.m01 * self.m10)
        # self.invm00 = self.m11 * M_factor
        # self.invm01 = -self.m01 * M_factor
        # self.invm10 = -self.m10 * M_factor
        # self.invm11 = self.m00 * M_factor

        self.m00 = (self.rhos / self.dt**2) * (1 - self.phi0) + \
            (1 / self.dt) * self.phi0**2 * 1. / (self.kf)
        # self.m01 = -self.phi0**2 * 1./(self.kf)
        # self.m10 = -self.phi0**2 * 1./(self.kf)
        # self.m11 = self.rhof * self.phi0 + (self.dt) * self.phi0**2 * 1./(self.kf)

        # Inverse right matrix given by inv(AD - BC) for a block matrix [A B; C D]
        # M_factor = 1./(self.m00 * self.m11 - self.m01 * self.m10)
        # self.invm00 = self.m11 * M_factor
        # self.invm01 = -self.m01 * M_factor
        # self.invm10 = -self.m10 * M_factor
        # self.invm11 = self.m00 * M_factor

        # print("beta {}".format(self.beta))
        # print("minv 00 {}".format(1. / self.m00))

    def bilinearBlockform(self, tuple_lhs, tuple_rhs):
        """
        Perform the product of the stabilization block matrix B (see article) for the form
        (1/dt[phi0s.I, dt.phi.I].T inv(M)[phi0s.I, dt.phi0.I][u; v], [u; v])
        for M of size 2d x 2d and u,v of size d. It doesn't require the block components as they are precomputed in the constructor as mij and invmij.
        """

        # First modify arguments with vectors [phi0s, dt.phi0]
        tuple_lhs = ((1 - self.phi0) *
                     tuple_lhs[0], Constant(self.dt) * self.phi0 * tuple_lhs[1])
        tuple_rhs = ((1 - self.phi0) / Constant(self.dt) *
                     tuple_rhs[0], self.phi0 * tuple_rhs[1])  # Divided by dt
        invm00 = self.invm00
        invm01 = self.invm01
        invm10 = self.invm10
        invm11 = self.invm11

        # Apply inverse to tuple_lhs
        lhs0 = invm00 * tuple_lhs[0] + invm01 * tuple_lhs[1]
        lhs1 = invm10 * tuple_lhs[0] + invm11 * tuple_lhs[1]
        return dot(lhs0, tuple_rhs[0]) + dot(lhs1, tuple_rhs[1])

    def set_fluid_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.fluid_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def set_pressure_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.pressure_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def setup(self):
        """
        Assemble problem matrix, incorporate essential boundary conditions as well.
        """
        u, p = TrialFunctions(self.V)
        v, q = TestFunctions(self.V)

        # Add scalings
        u = Constant(self.vref) * u
        p = Constant(self.pref) * p

        phi0 = self.phi0
        phis = 1 - phi0  # Solid fraction
        mu_f = self.mu_f
        mu_s = Constant(self.parameters["mu_s"])
        lmbda = Constant(self.parameters["lmbda"])
        dim = self.dim
        dt = self.dt

        a_f_base = (Constant(self.rhof / self.dt) * self.phi0 * dot(u, v)
                    + 2 * Constant(self.mu_f) * self.phi0 *
                    inner(self.eps(u), self.eps(v))
                    - p * div(self.phi0 * v)
                    + self.phi0 ** 2 * dot(inv(self.kf) * u, v)) * dx

        a_p_base = (phis**2 / Constant(self.ks * self.dt) * p * q
                    + div(self.phi0 * u) * q) * dx

        # Same as a_p, but reinstantiated to keep different additional terms
        a_p_diff = (phis**2 / Constant(self.ks * self.dt) * p * q
                    + div(self.phi0 * u) * q) * dx

        if self.solver_type == "undrained":
            a_f = a_f_base
            a_p = a_p_base

        elif self.solver_type == "diagonal-stab":
            a_f = a_f_base + Constant(self.betaf) * phi0**2 * dot(inv(self.kf) * u, v) * dx

            beta_p = phis**2 / \
                Constant(self.betap * dt * (2. * mu_s / dim + lmbda))

            a_p = a_p_base + beta_p * p * q * dx

        elif self.solver_type == "fixed-stress-fp-s":

            # print("Note: This seems to work for realistic data!")

            beta_A = phis**2 / Constant(self.dt * (2. * mu_s / dim + lmbda))

            a_p = a_p_base + beta_A * p * q * dx

        elif (self.solver_type == "fixed-stress-sp-f"):

            # print("Warning: For small mu_f, the stabilization added due to beta_M is resulting in slow convergence.")

            beta_M = phi0 / Constant(2. * mu_f / dim)

            a_p = a_p_base + beta_M * p * q * dx

        elif (self.solver_type == "fixed-stress-p-fs"):

            print("Warning: For small mu_f, the stabilization added due to beta_M is resulting in slow convergence.")

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            a_p = a_p_base + (beta_A + beta_M) * p * q * dx

        elif (self.solver_type == "cahouet-chabard-p-fs"):

            beta_A = phis**2 / Constant(dt * (2. * mu_s / dim + lmbda))
            beta_M = phi0 / Constant(2. * mu_f / dim)

            det = (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) \
                * (Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) \
                - (phi0**2 * Constant(inv(kf)))**2
            beta_K = Constant(dt) / det * \
                ((Constant(rhof) * phi0 + phi0**2 * Constant(dt * inv(kf))) * (phis / Constant(dt))**2
                 + 2. * phi0**2 * Constant(inv(kf)) *
                 phi0 * (phis / Constant(dt))
                 + (Constant(rhos / dt**2) * phis + phi0**2 * Constant(inv(kf) / dt)) * phi0**2)

            a_p = a_p_base + (beta_A + beta_M) * p * q * dx
            a_p_diff = (phis**2 / Constant(self.ks * self.dt) * p * q
                        + dot(beta_K * grad(p), grad(q))) * dx

        A_fs = PETScMatrix()
        A_diff = PETScMatrix()
        A_base = PETScMatrix()
        assemble(a_f_base + a_p_base, tensor=A_base)
        assemble(a_f + a_p, tensor=A_fs)
        assemble(a_f + a_p_diff, tensor=A_diff)

        self.apply_fluid_bc(A_fs)
        self.apply_fluid_bc(A_diff)
        self.apply_fluid_bc(A_base)
        self.apply_pressure_bc(A_diff)

        self.sol = Function(self.V)

        # Set matrix and solver. No time dependent terms, so it can be assembled only once.
        self.A_fs = A_fs
        self.A_base = A_base
        self.A_diff = A_diff

        mat_A_fs = A_fs
        mat_A_diff = A_diff
        if self.parameters["splitting as preconditioner"]:
            mat_A_base_fs = A_base
            mat_A_base_diff = A_base
        else:
            mat_A_base_fs = A_fs
            mat_A_base_diff = A_fs

        if self.parameters["method"] == "iterative":
            self.solver_fs = PETScKrylovSolver()
            self.solver_fs.set_from_options()
            self.solver_diff = PETScKrylovSolver()
            self.solver_diff.set_from_options()
            # Set operators (A, P)
            self.solver_fs.set_operators(mat_A_base_fs, mat_A_fs)
            self.solver_diff.set_operators(mat_A_base_diff, mat_A_diff)

        elif self.parameters["method"] == "AAR":
            self.solver_fs = AAR(order=self.parameters["AAR order"], p=self.parameters["AAR p"],
                                 omega=self.parameters["AAR omega"], beta=self.parameters["AAR beta"],
                                 x0=None, matM=mat_A_fs.mat(), matA=mat_A_base_fs.mat(),
                                 atol=self.parameters["tolerance residual absolute"],
                                 rtol=self.parameters["tolerance residual relative"],
                                 maxiter=self.parameters["iterative maxiter"])
            self.solver_diff = AAR(order=self.parameters["AAR order"], p=self.parameters["AAR p"],
                                   omega=self.parameters["AAR omega"], beta=self.parameters["AAR beta"],
                                   x0=None, matM=mat_A_diff.mat(), matA=mat_A_base_diff.mat(),
                                   atol=self.parameters["tolerance residual absolute"],
                                   rtol=self.parameters["tolerance residual relative"],
                                   maxiter=self.parameters["iterative maxiter"])
        else:  # Direct by default
            self.solver_fs = LUSolver(mat_A_base_fs, 'mumps')
            self.solver_diff = LUSolver(mat_A_base_diff, 'mumps')

    def solve_fixed_stress_stab(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """
        self.apply_fluid_bc(F)
        its = self.solver_fs.solve(self.sol.vector(), F)
        if its and self.parameters["iterations solver output"]:
            print("\t\tSolved fluid-pressure in {} iterations".format(its))
        return self.sol.split()

    def solve_diffusion_stab(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """
        self.apply_fluid_bc(F)
        self.apply_pressure_bc(F)
        its = self.solver_diff.solve(self.sol.vector(), F)
        if its and self.parameters["iterations solver output"]:
            print("\t\tSolved fluid-pressure in {} iterations".format(its))
        return self.sol.split()

    def eps(self, vec):
        return sym(grad(vec))

    def apply_fluid_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.fluid_bcs:
            for b in self.fluid_bcs:
                b.apply(obj)

    def apply_pressure_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.pressure_bcs:
            for b in self.pressure_bcs:
                b.apply(obj)

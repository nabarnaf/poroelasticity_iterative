from dolfin import *
from lib.AAR import AAR


class Solid:

    def __init__(self, parameters, mesh, dsNeumann, reference=[1., 1., 1.]):
        """
        Create instance of solid model, which handles the solid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """

        # First assert that all required variables are in the parameters dictionary
        self.parameters = parameters
        required_fields = ["fe_degree_solid", "mu_s",
                           "lmbda", "rhos", "phi0", "ks", "kf", "dt", "solver_type"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters"

        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree = parameters["fe_degree_solid"]
        self.V = VectorFunctionSpace(mesh, 'CG', self.fe_degree)
        self.dsNeumann = dsNeumann
        self.solver_type = parameters["solver_type"]
        # assert self.solver_type in (
        # "undrained", "fixed-stress"), "Solid solver_type must be undrained or fixed-stress."

        # Model parameters
        self.mu_s = parameters["mu_s"]
        self.lmbda = parameters["lmbda"]
        self.rhos = parameters["rhos"]
        self.phi0 = parameters["phi0"]
        self.ks = parameters["ks"]
        self.kf = parameters["kf"]

        # Time parameters
        self.dt = parameters["dt"]

        self.uref = reference[0]

        # Stabilization
        self.betas = 1.
        if "betas" in parameters:
            self.betas = parameters["betas"]

    def set_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant.
        """
        self.bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def setup(self):
        """
        Assemble problem matrix, incorporate essential boundary conditions as well.
        """
        u = Constant(self.uref) * TrialFunction(self.V)
        v = TestFunction(self.V)

        phis = 1 - self.phi0  # Solid fraction

        a_base = (Constant(self.rhos / self.dt**2) * phis * dot(u, v)
                  + inner(self.hooke(self.eps(u)), self.eps(v))
                  + Constant(1 / self.dt) * self.phi0 ** 2 * dot(inv(self.kf) * u, v)) * dx

        # Undrained scheme requires div stabilization term.
        if self.solver_type == "undrained":
            N = Constant(self.ks) / phis**2
            a = a_base + N * div(phis * u) * div(phis * v) * dx
        elif self.solver_type in ("diagonal-stab", "diagonal-stab-3-way"):
            a = a_base + Constant(self.betas / self.dt) * \
                self.phi0 ** 2 * dot(inv(self.kf) * u, v) * dx
        else:
            a = a_base

        A_base = PETScMatrix()
        A = PETScMatrix()
        assemble(a, tensor=A)
        assemble(a_base, tensor=A_base)
        self.apply_bc(A)
        self.apply_bc(A_base)
        # A.mat().setBlockSize(3)
        # A_base.mat().setBlockSize(3)

        # Set matrix and solver. No time dependent terms, so it can be assembled only once.
        self.A = A
        self.A_base = A_base
        self.sol = Function(self.V)

        mat_A = A
        if self.parameters["splitting as preconditioner"]:
            mat_A_base = A_base
        else:
            mat_A_base = A

        if self.parameters["method"] == "iterative":
            self.solver = PETScKrylovSolver()
            self.solver.set_from_options()
            self.solver.set_operators(mat_A_base, mat_A)

        elif self.parameters["method"] == "AAR":
            self.solver = AAR(order=self.parameters["AAR order"], p=self.parameters["AAR p"],
                              omega=self.parameters["AAR omega"], beta=self.parameters["AAR beta"],
                              x0=None, matM=mat_A.mat(), matA=mat_A_base.mat(),
                              atol=self.parameters["tolerance residual absolute"],
                              rtol=self.parameters["tolerance residual relative"],
                              maxiter=self.parameters["iterative maxiter"])
        else:  # Direct by default
            self.solver = LUSolver(mat_A_base, 'mumps')

    def resetup(self, uref):
        self.uref = uref
        self.setup()

    def solve(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """
        self.apply_bc(F)
        its = self.solver.solve(self.sol.vector(), F)
        if its and self.parameters["iterations solver output"]:
            print("\t\tSolved elasticity in {} iterations".format(its))
        return self.sol

    def eps(self, vec):
        return sym(grad(vec))

    def hooke(self, ten):
        return 2 * self.mu_s * ten + self.lmbda * tr(ten) * Identity(self.dim)

    def apply_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.bcs:
            for b in self.bcs:
                b.apply(obj)

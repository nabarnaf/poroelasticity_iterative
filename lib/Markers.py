# -*- coding: utf-8 -*-
#!/usr/bin/env python3
"""
Created on Wed Nov 28 13:49:33 2018

@author: barnafi
"""


class Markers:
    """
    Class containing a list of integers which define markers
    """

    def __init__(self, markers, neumannSolidMarkers, neumannFluidMarkers, noSlipMarkers, none=None):

        self.markers = markers  # Markers dolfin object

        asList = lambda _x: _x if isinstance(_x, list) else [_x]  # use lists only
        self.neumannSolidMarkers = asList(neumannSolidMarkers)
        self.neumannFluidMarkers = asList(neumannFluidMarkers)

        # Set null flag
        if none:
            self.NONE = none
        else:
            self.NONE = int(markers.array().max() + 42) 


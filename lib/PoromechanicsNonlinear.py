from dolfin import *
import numpy as np
from petsc4py import PETSc
from mpi4py import MPI
from lib.Poromechanics import PoromechanicsAbstract
from lib.SolidNonlinearFluidPressure import SolidNonlinearFluidPressure
from lib.AndersonAcceleration import AndersonAcceleration
from lib.Mixing import MixingAitken, MixingFixed


class MonolithicNonlinear(PoromechanicsAbstract):

    def __init__(self, parameters, mesh, dsSolidNeumann, dsSolidRobin, dsFluidNeumann, dsFluidNoSlip):
        """
        Class in charge of coupling solid and fluid through an iterative splitting scheme.
        """
        super().__init__(parameters, mesh, dsSolidNeumann, dsFluidNeumann)

        # First assert that all required variables are in the parameters dictionary
        required_fields = ["solver_type", "mixing_type"]
        assert all([x in parameters.keys() for x in required_fields]
                   ), "Missing arguments in parameters for Monolithic Poromechanics, it requires: {}".format(required_fields)

        reference = (self.uref, self.vref, self.pref)

        # Solver parameters
        self.solver_type = parameters["solver_type"]
        self.mixing_type = parameters["mixing_type"]
        self.mixing_weight = 1.
        self.mixing_weight_diff = 1.
        if self.mixing_type in ["fixed-weights"]:
            self.mixing_weight = parameters["mixing_weight_fs"]
            self.mixing_weight_diff = parameters["mixing_weight_diff"]
        self.CC_solver_list = ["cahouet-chabard-fp-s-to-3-way",
                               "cahouet-chabard-p-fs", "cahouet-chabard-p-fs-to-3-way", "CC-fs-p"]
        allowed_mixings = ["fixed-weights",
                           "Aitken", "Aitken-constrained", "None"]
        CC_allowed_mixings = ["fixed-weights", "Aitken", "Aitken-constrained"]

        if not(self.mixing_type in allowed_mixings):
            print("Mixing does not exist")
            exit()
        if self.solver_type in self.CC_solver_list and not(self.mixing_type in allowed_mixings):
            print("Choose approrpiate mixing.")
            exit()

        self.CC_flag = True if self.solver_type in self.CC_solver_list else False

        self.solid_fluid_pressure = SolidNonlinearFluidPressure(
            parameters, mesh, dsSolidNeumann, dsSolidRobin, dsFluidNeumann, dsFluidNoSlip)
        print("Solving nonlinear problem with {} dofs".format(
            self.solid_fluid_pressure.V.dim()), flush=True)

        # Allocate vectors
        f_nm1 = Function(self.solid_fluid_pressure.V, name="u - v - p")
        self.us_nm1, self.uf_nm1, self.p_nm1 = f_nm1.split(True)
        self.us_nm2 = self.us_nm1.copy(True)
        self.residual = Function(self.solid_fluid_pressure.V)
        self.residual = self.residual.vector()
        self.sol_new = Function(self.solid_fluid_pressure.V)
        self.sol_k = Function(self.solid_fluid_pressure.V)
        self.sol_kk = Function(self.solid_fluid_pressure.V)
        self.increment_k = Function(self.solid_fluid_pressure.V)
        self.increment_kk = Function(self.solid_fluid_pressure.V)
        self.increment_new1 = Function(self.solid_fluid_pressure.V)
        self.increment_new2 = Function(self.solid_fluid_pressure.V)
        self.increment_old1 = Function(self.solid_fluid_pressure.V)
        self.increment_old2 = Function(self.solid_fluid_pressure.V)

        self.reassemble_jacobian_iteration = parameters["reassemble Jacobian every nth iteration"]

    def set_bcs(self, bcs_solid, bcs_fluid, bcs_pressure=[]):
        """
        Set boundary conditions to both physics. Assumed to be constant.
        """
        self.solid_fluid_pressure.set_solid_bcs(bcs_solid)
        self.solid_fluid_pressure.set_fluid_bcs(bcs_fluid)
        self.solid_fluid_pressure.set_pressure_bcs(bcs_pressure)

    def setup(self):
        """
        No need for setup for this class at this level
        """
        pass

    def solve_time_step(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source):
        """
        Solve a time step by means of the iterative scheme.
         It requires volumetric and surface loads as input,
        it handles variable update internally
        """

        sol_k = self.sol_k
        sol_kk = self.sol_kk
        sol_new = self.sol_new
        increment_k = self.increment_k
        increment_kk = self.increment_kk
        increment_new1 = self.increment_new1
        increment_new2 = self.increment_new2
        increment_old1 = self.increment_old1
        increment_old2 = self.increment_old2

        # Initialize vectors
        us_k, uf_k, p_k = sol_k.split()
        assign(us_k, self.us_nm1)
        assign(uf_k, self.uf_nm1)
        assign(p_k, self.p_nm1)
        for v in [increment_k, increment_kk, increment_new1, increment_new2, increment_old1, increment_old2]:
            v.vector().zero()

        comm = MPI.COMM_WORLD

        # Create global dofmaps
        dofs_s = self.solid_fluid_pressure.V.sub(0).dofmap().dofs()
        dofs_f = self.solid_fluid_pressure.V.sub(1).dofmap().dofs()
        dofs_p = self.solid_fluid_pressure.V.sub(2).dofmap().dofs()
        is_s = PETSc.IS().createGeneral(dofs_s)
        is_f = PETSc.IS().createGeneral(dofs_f)
        is_p = PETSc.IS().createGeneral(dofs_p)

        def petscnorm(vec):
            return vec.norm(PETSc.NormType.NORM_INFINITY)

        def solid_norm(vec):
            subvec = vec.getSubVector(is_s)
            out = subvec.norm(PETSc.NormType.NORM_INFINITY)
            vec.restoreSubVector(is_s, subvec)
            return out

        def fluid_norm(vec):
            subvec = vec.getSubVector(is_f)
            out = subvec.norm(PETSc.NormType.NORM_INFINITY)
            vec.restoreSubVector(is_f, subvec)
            return out

        def pressure_norm(vec):
            subvec = vec.getSubVector(is_p)
            out = subvec.norm(PETSc.NormType.NORM_INFINITY)
            vec.restoreSubVector(is_p, subvec)
            return out

        # Norms to normalize errors
        res0 = self.compute_residual(
            f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, sol_k, sol_kk)
        res0_s, res0_f, res0_p = solid_norm(
            res0), fluid_norm(res0), pressure_norm(res0)
        if res0_s < 3e-14:
            res0_s = 1
        if res0_f < 3e-14:
            res0_f = 1
        if res0_p < 3e-14:
            res0_p = 1

        mixing, anderson = self.setup_anderson_and_mixing()

        error_residual_abs, error_residual_rel, it = 1, 1, 0
        if self.keep_solutions:
            # Add a list in last position
            self.solutions.append([])

        self.ref_nrm = [1., 1., 1.]

        self.increment_converged = True
        assign(sol_k, sol_new)

        while error_residual_rel > self.tol_residual_rel \
                and error_residual_abs > self.tol_residual_abs \
                and it < self.maxiter:

            if it % self.reassemble_jacobian_iteration == 0:
                self.solid_fluid_pressure.init()

            # Step 1a: Solve incremental problem

            solve_args = [self.t, sol_k, self.us_nm1, self.us_nm2, self.uf_nm1,
                          self.p_nm1, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid]
            increment_new1.assign(self.solid_fluid_pressure.solve(
                *solve_args, variant_diff=False))

            # Step 1b: Apply diffusion stabilization
            if self.CC_flag:

                increment_new2.assign(
                    self.solid_fluid_pressure.solve(*solve_args, variant_diff=True))

            # Step 2: Obtain increment through mixing
            mixing.update_increment(sol_k.vector().vec(
            ), increment_new1.vector().vec(), increment_new2.vector().vec())

            # Update solution
            sol_new.vector().vec().axpy(1.0, mixing.increment)

            # Step 3, give increments to Anderson handler and obtain the current solution
            anderson.get_next_vector(sol_new.vector().vec())

            # Compute errors
            residual = self.compute_residual(
                f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, sol_k, sol_kk)
            res_s, res_f, res_p = solid_norm(residual), fluid_norm(
                residual), pressure_norm(residual)

            error_residual_abs = petscnorm(residual)
            error_residual_rel = sqrt(
                (res_s / res0_s)**2 + (res_f / res0_f)**2 + (res_p / res0_p)**2)

            assign(sol_k, sol_new)

            it += 1
            if self.iterations_output:
                #print("DEBUG", res_s, res_f, res_p)
                #print("DEBUG", res0_s, res0_f, res0_p)
                print("------- Iteration {}, \tres_abs={:.2E}, \tres_rel={:.2E}".format(
                    it, error_residual_abs, error_residual_rel), flush=True)

            if self.keep_solutions:
                # Add to current list the last solution as a dictionary
                self.solutions[-1].append({"us": us_k.copy(True),
                                           "uf": uf_k.copy(True), "p": p_k.copy(True)})

        # Finally update internal functions
        assign(self.us_nm2, self.us_nm1)
        assign(self.us_nm1, us_k)
        assign(self.uf_nm1, uf_k)
        assign(self.p_nm1, p_k)

        return it

    def export(self, time):
        self.xdmf.write(self.us_nm1, time)
        self.xdmf.write(self.uf_nm1, time)
        self.xdmf.write(self.p_nm1, time)

    def compute_solid_error(self, us_ref, us):
        return errornorm(us_ref, us, norm_type="L2", degree_rise=0)

    def compute_fluid_error(self, uf_ref, uf):
        return errornorm(uf_ref, uf, norm_type="L2", degree_rise=0)

    def compute_pressure_error(self, p_ref, p):
        return errornorm(p_ref, p, degree_rise=0)

    def compute_residual(self, fs_vol, fs_sur, ff_vol, ff_sur, sol_k, sol_kk):

        us, vf, p = split(sol_k)
        us_k, _, p_k = split(sol_kk)
        v, w, q = TestFunctions(self.solid_fluid_pressure.V)

        residual = self.solid_fluid_pressure.compute_residual(
            self.t, us, vf, p, v, w, q, self.us_nm1, self.us_nm2, us_k, self.uf_nm1, self.p_nm1, p_k, fs_vol, fs_sur, ff_vol, ff_sur, split_residual=False, forms=False)
        assemble(residual, tensor=self.residual)

        # self.solid_fluid_pressure.apply_solid_bc(self.residual)
        # self.solid_fluid_pressure.apply_fluid_bc(self.residual)

        return self.residual.vec()

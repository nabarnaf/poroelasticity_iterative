from dolfin import *


class Fluid:

    def __init__(self, parameters, mesh, dsNeumann, reference=[1., 1., 1.]):
        """
        Create instance of fluid model, which handles the fluid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """
        # First assert that all required variables are in the parameters dictionary
        required_fields = ["fe_degree_fluid_velocity",
                           "mu_f", "rhof", "phi0", "kf", "dt"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters"
        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree_u = parameters["fe_degree_fluid_velocity"]
        self.V = VectorFunctionSpace(mesh, 'CG', self.fe_degree_u)
        self.dsNeumann = dsNeumann
        # Model parameters
        self.mu_f = parameters["mu_f"]
        self.rhof = parameters["rhof"]
        self.phi0 = parameters["phi0"]
        self.kf = parameters["kf"]
        # Time parameters
        self.dt = parameters["dt"]

        self.vref = reference[1]

    def set_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant.
        """

        self.bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def setup(self):
        """
        Assemble problem matrix, incorporate essential boundary conditions as well.
        """
        u = Constant(self.vref) * TrialFunction(self.V)
        v = TestFunction(self.V)

        dt = Constant(self.dt)

        af = (Constant(self.rhof / dt) * self.phi0 * dot(u, v)
              + 2 * Constant(self.mu_f) * self.phi0 *
              inner(self.eps(u), self.eps(v))
              + self.phi0 ** 2 * dot(inv(self.kf) * u, v)) * dx

        A = PETScMatrix()
        assemble(af, tensor=A)
        self.apply_bc(A)

        # Set matrix and solver. No time dependent terms, so it can be assembled only once.
        self.A = A
        self.LU = LUSolver(self.A)

    def resetup(self,vref):
        self.vref = vref
        self.setup()

    def solve(self, F):
        """
        Solve current iteration given external loads and previous iterations
        """

        self.apply_bc(F)

        sol = Function(self.V)
        self.LU.solve(sol.vector(), F)
        return sol

    def eps(self, vec):
        return sym(grad(vec))

    def apply_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.bcs:
            for b in self.bcs:
                b.apply(obj)

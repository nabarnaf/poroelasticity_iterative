#
# DEPRECATED: Should be deleted if possible. [TODO]
#
from dolfin import *
from numpy import dot
from petsc4py import PETSc
from lib.Solid import Solid
from lib.FluidPressure import FluidPressure
from lib.AndersonAcceleration import AndersonAcceleration
from lib.Mixing import MixingAitken, MixingFixed
from lib.Poromechanics import PoromechanicsAbstract


class PoromechanicsTwoWay(PoromechanicsAbstract):

    def __init__(self, parameters, mesh, dsNeumann_solid, dsNeumann_fluid):
        """
        Class in charge of coupling solid and fluid through an iterative splitting scheme.
        """

        super().__init__(parameters, mesh)

        # First assert that all required variables are in the parameters dictionary
        required_fields = ["solver_type", "mixing_type"]
        assert all([x in parameters.keys() for x in required_fields]
                   ), "Missing arguments in parameters for Monolithic Poromechanics, it requires: {}".format(required_fields)

        reference = (self.uref, self.vref, self.pref)

        # Solver parameters
        self.solver_type = parameters["solver_type"]
        self.mixing_type = parameters["mixing_type"]
        self.mixing_weight = 1.
        self.mixing_weight_diff = 1.
        if self.mixing_type in ["fixed-weights"]:
            self.mixing_weight = parameters["mixing_weight_fs"]
            self.mixing_weight_diff = parameters["mixing_weight_diff"]
        self.CC_solver_list = ["cahouet-chabard-p-fs"]
        allowed_mixings = ["fixed-weights",
                           "Aitken", "Aitken-constrained", "None"]
        if not(self.mixing_type in allowed_mixings):
            print("Mixing does not exist")
            exit()
        if self.solver_type in self.CC_solver_list and not(self.mixing_type in allowed_mixings):
            print("Choose approrpiate mixing.")
            exit()

        three_way_solvers = ["cahouet-chabard-fp-s-to-3-way",
                             "cahouet-chabard-p-fs-to-3-way", "diagonal-stab-3-way", "fixed-stress-p-f-s"]
        if self.solver_type in three_way_solvers:
            raise Exception(
                "Can't use three way solvers with two-way solver class")

        self.CC_flag = True if self.solver_type in self.CC_solver_list else False

        # Initialize all physics
        self.solid = Solid(parameters, mesh, dsNeumann_solid, reference)
        self.fluid_pressure = FluidPressure(
            parameters, mesh, dsNeumann_fluid, reference)

        self.V_full = FunctionSpace(mesh,
                                    MixedElement(VectorElement('CG', mesh.ufl_cell(), parameters["fe_degree_solid"]),
                                                 VectorElement(
                                        'CG', mesh.ufl_cell(), parameters["fe_degree_fluid_velocity"]),
                                        FiniteElement('CG', mesh.ufl_cell(), parameters["fe_degree_pressure"])))
        print("Solving poromechanics problem with {} dofs".format(self.V_full.dim()))

        # Create placeholders for current solutions
        self.vf_p = Function(self.fluid_pressure.V)
        self.us = Function(self.solid.V)

        self.uf_nm1, self.p_nm1 = self.vf_p.split(True)
        self.us_nm1 = self.us.copy(True)
        self.us_nm2 = self.us_nm1.copy(True)

    def set_bcs(self, bcs_solid, bcs_fluid, bcs_pressure):
        """
        Set boundary conditions to both physics. Assumed to be constant.
        """
        self.solid.set_bcs(bcs_solid)
        self.fluid_pressure.set_fluid_bcs(bcs_fluid)
        self.fluid_pressure.set_pressure_bcs(bcs_pressure)

    def setup(self):
        """
        Assemble system matrices for both physics. Must be performed after setting boundary conditions, if present.
        """
        self.solid.setup()
        self.fluid_pressure.setup()

        # Allocate vectors
        self.sol_k = Function(self.V_full)
        us_k, uf_k, p_k = self.sol_k.split()

        self.sol_new = Function(self.V_full)
        self.increment = Function(self.V_full)
        self.increment_new1 = Function(self.V_full)
        self.increment_new2 = Function(self.V_full)
        self.increment_old1 = Function(self.V_full)
        self.increment_old2 = Function(self.V_full)
        self.Rs = PETScVector()
        self.Rf = PETScVector()
        self.Rp = PETScVector()
        self.Rfp = PETScVector()

    def solve_time_step(self, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source):
        """
        Solve a time step by means of the iterative scheme. It requires volumetric and surface loads as input,
        it handles variable update internally
        """

        from time import perf_counter

        t = perf_counter()
        sol_k = self.sol_k
        us_k, uf_k, p_k = sol_k.split()

        sol_new = self.sol_new
        # increment = self.increment
        increment_new1 = self.increment_new1
        increment_new2 = self.increment_new2
        increment_old1 = self.increment_old1
        increment_old2 = self.increment_old2

        Rs = self.Rs
        Rfp = self.Rfp

        # Initialize variables are last computed time step
        assign(us_k, self.us_nm1)
        assign(uf_k, self.uf_nm1)
        assign(p_k, self.p_nm1)

        # Norms to normalize errors
        norm_us = norm(self.us_nm1)
        norm_uf = norm(self.uf_nm1)
        norm_p = norm(self.p_nm1)
        if norm_us < 3e-16:
            norm_us = 1
        if norm_uf < 3e-16:
            norm_uf = 1
        if norm_p < 3e-16:
            norm_p = 1
        norm_total = norm_us + norm_uf + norm_p  # Just use l1 vector norm

        res0_s, res0_f, res0_p = self.compute_residual_norm(
            f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k, comp="split")

        if res0_s < 3e-14:
            res0_s = 1
        if res0_f < 3e-14:
            res0_f = 1
        if res0_p < 3e-14:
            res0_p = 1

        # Initialize handler for the mixing of the increments
        N_increments = 2 if self.CC_flag else 1
        if self.mixing_type == "Aitken":
            mixing = MixingAitken(
                self.parameters, N_increments, variant="unconstrained")
        elif self.mixing_type in "fixed-weights":
            mixing = MixingFixed(self.parameters, N_increments)

        elif self.mixing_type == "Aitken-constrained":
            mixing = MixingAitken(
                self.parameters, N_increments, variant="constrained")

        elif self.mixing_type == "None":
            params = {}
            for k in self.parameters:
                params[k] = self.parameters[k]
            params["mixing_weight_fs"] = 1
            params["mixing_weight_diff"] = 0
            mixing = MixingFixed(params, N_increments)

        # Anderson acceleration
        if self.parameters["anderson_order"] > 0:
            anderson = AndersonAcceleration(self.parameters)
        else:
            anderson = None

        # Define PETSc vectors used by mixing and acceleration
        xk = sol_k.vector().vec()
        xkk = sol_k.vector().vec().copy()
        fk = sol_k.vector().vec().copy()
        fkk = sol_k.vector().vec().copy()
        fk.zeroEntries()
        fkk.zeroEntries()

        # It is a linear problem with exact Jacobian;
        # nevertheless use an Newton type approach on purpose.
        error_residual, error_increment, it = 1, 1, 0
        if self.keep_solutions:
            # Add a list in last position
            self.solutions.append([])

        self.ref_nrm = [1., 1., 1.]

        self.increment_converged = True
        self.residual_converged = True

        while error_residual > self.tol_residual and it < self.maxiter:

            # Initialize current PETSc vector
            # sol_k.vector().vec().copy(result=xk)

            # Step 1: Solve incremental problem
            self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "fluid-pressure")

            df, dp = self.fluid_pressure.solve_fixed_stress_stab(Rfp)

            self.compute_residual(
                self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "solid", df_k=df, dp_k=dp)
            ds = self.solid.solve(Rs)

            assign(increment_new1.sub(0), ds)
            assign(increment_new1.sub(1), df)
            assign(increment_new1.sub(2), dp)

            # Step 1b: Apply diffusion stabilization
            if self.CC_flag:
                if self.verbose:
                    print("CC flag")
                self.compute_residual(
                    self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "fluid-pressure")

                df, dp = self.fluid_pressure.solve_diffusion_stab(Rfp)

                self.compute_residual(
                    self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k, us_k, f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, "solid", df_k=df, dp_k=dp)
                ds = self.solid.solve(Rs)
                assign(increment_new2.sub(0), ds)
                assign(increment_new2.sub(1), df)
                assign(increment_new2.sub(2), dp)

            # Step 2: Obtain increment through mixing
            mixing.solve_iteration(it, xk, increment_new1.vector().vec(),
                                   xkk, increment_old1.vector().vec(),
                                   increment_new2.vector().vec(), increment_old2.vector().vec())

            # copy through PETSc interface
            fk.copy(result=fkk)
            mixing.increment.copy(result=fk)  # Update fk
            # mixing.increment.copy(result=increment.vector().vec())

            # Step 3, give increments to Anderson handler and obtain the current solution
            if anderson:
                new = anderson.solve_iteration(it, xk, xkk, fk, fkk)
                xk.copy(result=xkk)
                new.copy(result=xk)
                xk.copy(result=sol_new.vector().vec())
            else:
                xk.copy(result=xkk)
                sol_new.vector().vec().axpy(1, fk)
                sol_new.vector().vec().copy(result=xk)

            # Compute errors
            res_s, res_f, res_p = self.compute_residual_norm(
                f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid, p_source, sol_k, comp="split")
            residual_norm = sqrt(res_s**2 + res_f**2 + res_p**2)

            # Use same tolerance for absolute and relative errors
            error_residual = min(residual_norm, res_s /
                                 res0_s + res_f / res0_f + res_p / res0_p)
            # error_residual = res_s / res0_s + res_f / res0_f + res_p / res0_p

            error_increment = fk.norm() / norm_total
            assign(sol_k, sol_new)
            # assign(us_k, sol_new.sub(0))
            # assign(uf_k, sol_new.sub(1))
            # assign(p_k, sol_new.sub(2))

            increment_old1.assign(increment_new1)
            increment_old2.assign(increment_new2)

            it += 1
            if self.iterations_output:
                print("------- Iteration {}, \terror_residual={:.3E},\t error_increment={:.3E}".format(
                    it, error_residual, error_increment))

            if self.keep_solutions:
                # Add to current list the last solution as a dictionary
                self.solutions[-1].append({"us": us_k.copy(True),
                                           "uf": uf_k.copy(True), "p": p_k.copy(True)})

        if error_residual > self.tol_residual:
            print("WARNING: method residual not converged")

        if error_increment > self.tol_increment:
            self.increment_converged = False

        if error_residual > self.tol_residual:
            self.residual_converged = False

        # Finally update internal functions
        assign(self.us_nm2, self.us_nm1)
        assign(self.us_nm1, us_k)
        assign(self.uf_nm1, uf_k)
        assign(self.p_nm1, p_k)

        return it

    def compute_residual(self, p_nm1_, uf_nm1_, us_nm1_, us_nm2_, p_km1_, uf_km1_, us_km1_, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="None", df_k=None, dp_k=None):

        if comp == None:
            print("No components have been chosen in the computation of the residual.")
            sys.exit()

        # Scale variables
        us_nm1 = Constant(self.uref) * us_nm1_
        us_nm2 = Constant(self.uref) * us_nm2_
        uf_nm1 = Constant(self.vref) * uf_nm1_
        p_nm1 = Constant(self.pref) * p_nm1_
        us_km1 = Constant(self.uref) * us_km1_
        uf_km1 = Constant(self.vref) * uf_km1_
        p_km1 = Constant(self.pref) * p_km1_

        """
        Compute fluid residual as in step 0
        """
        v = TestFunction(self.solid.V)
        w, q = TestFunctions(self.fluid_pressure.V)

        dt = Constant(self.dt)
        idt = 1. / dt
        phi0 = self.fluid_pressure.phi0
        phi0s = 1 - phi0
        ikf = inv(self.solid.kf)
        mu_f = self.fluid_pressure.mu_f
        rhof = self.fluid_pressure.rhof
        rhos = self.solid.rhos
        ks = self.fluid_pressure.ks

        # Compute solid residual
        rhs_s_n = dot(fs_vol, v) * dx + dot(fs_sur, v) * \
            self.solid.dsNeumann

        M_s = dot(Constant(rhos * idt**2) * phi0s *
                  (us_km1 - 2. * us_nm1 + us_nm2), v)
        A_s = inner(self.solid.hooke(
            self.solid.eps(us_km1)), self.solid.eps(v))
        DT_s = - div(phi0s * v) * p_km1
        K_s = - phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), v)
        lhs_s_n = (M_s + K_s + A_s + DT_s) * dx

        r_s = rhs_s_n - lhs_s_n

        if df_k:
            if dp_k:
                r_s -= (-div(phi0s * v) * dp_k - phi0 **
                        2 * dot(ikf * df_k, v)) * dx

        # Compute fluid residual
        rhs_f_n = dot(ff_vol, w) * dx + dot(ff_sur, w) * \
            self.fluid_pressure.dsNeumann

        M_f = dot(Constant(rhof * idt) * phi0 * (uf_km1 - uf_nm1), w)
        A_f = 2 * mu_f * phi0 * \
            inner(self.solid.eps(uf_km1),
                  self.solid.eps(w))
        DT_f = - div(phi0 * w) * p_km1
        K_f = phi0**2 * dot(ikf * (uf_km1 - idt * (us_km1 - us_nm1)), w)
        lhs_f_n = (M_f + A_f + DT_f + K_f) * dx

        r_f = rhs_f_n - lhs_f_n

        # Compute pressure residual
        rhs_p_n = Constant(1 / rhof) * p_source * q * dx

        D_sf = div(idt * phi0s * (us_km1 - us_nm1) + phi0 * uf_km1) * q
        M_p = phi0s**2 / Constant(ks * dt) * (p_km1 - p_nm1) * q
        lhs_p_n = (M_p + D_sf) * dx

        r_p = rhs_p_n - lhs_p_n

        if comp == "solid":
            assemble(r_s, tensor=self.Rs)
        elif comp == "fluid-pressure":
            assemble(r_f + r_p, tensor=self.Rfp)
        elif comp == "fluid":
            assemble(r_f, tensor=self.Rf)
        elif comp == "pressure":
            assemble(r_p, tensor=self.Rp)
        elif comp == "full":
            R = assemble(r_s + r_f + r_p)
            return R
        else:
            raise Exception(
                "Wrong component from PoromechanicsTwoWay.compute_residual")

    def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp="full", norm_us=1, norm_uf=1, norm_p=1):
        # def compute_residual_norm(self, fs_vol, fs_sur, ff_vol, ff_sur, p_source, solution_current, comp = "full", norm_us=1, norm_uf=1, norm_p=1):

        # # Scale variables
        # us_nm1 = Constant(self.uref) * us_nm1_
        # us_nm2 = Constant(self.uref) * us_nm2_
        # uf_nm1 = Constant(self.vref) * uf_nm1_
        # p_nm1 = Constant(self.pref) * p_nm1_
        # us_km1 = Constant(self.uref) * us_km1_
        # uf_km1 = Constant(self.vref) * uf_km1_
        # p_km1 = Constant(self.pref) * p_km1_
        """
        Compute fluid residual as in step 0
        """

        us_k, uf_k, p_k = solution_current.split()
        if comp == "full":
            R = self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                      us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="full")
            return norm(R)
        elif comp == "split":
            self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                  us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="solid")
            self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                  us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="fluid")
            self.compute_residual(self.p_nm1, self.uf_nm1, self.us_nm1, self.us_nm2, p_k, uf_k,
                                  us_k, fs_vol, fs_sur, ff_vol, ff_sur, p_source, comp="pressure")

            bc_s = [DirichletBC(b) for b in self.solid.bcs]
            bc_f = [DirichletBC(b) for b in self.fluid_pressure.fluid_bcs]
            for b in bc_s:
                b.apply(self.Rs)
            for b in bc_f:
                b.apply(self.Rf)
            return norm(self.Rs), norm(self.Rf), norm(self.Rp)

        # dt = Constant(self.dt)
        # idt = 1. / dt
        # phi0 = self.fluid_pressure.phi0
        # phi0s = 1 - phi0
        # ikf = inv(self.fluid_pressure.kf)
        # mu_f = self.fluid_pressure.mu_f
        # rhof = self.fluid_pressure.rhof
        # rhos = self.solid.rhos
        # ks = self.fluid_pressure.ks

        # V = self.V_full
        # us, uf, p = TrialFunctions(V)
        # vs, vf, q = TestFunctions(V)

        # if not self.A_monolithic_assembled:

        #     a_s = (Constant(rhos * idt**2) * phi0s * dot(us, vs)
        #            + inner(self.solid.hooke(self.solid.eps(us)),
        #                    self.solid.eps(vs))
        #            - p * div((1 - phi0) * vs)
        #            - phi0 ** 2 * dot(ikf * (uf - idt * us), vs)) * dx

        #     a_f = (Constant(rhof * idt) * phi0 * dot(uf, vf)
        #            + 2. * Constant(mu_f) * phi0 *
        #            inner(self.solid.eps(uf), self.solid.eps(vf))
        #            - p * div(phi0 * vf)
        #            + phi0 ** 2 * dot(ikf * (uf - idt * us), vf)) * dx

        #     a_p = (phi0s**2 / Constant(ks * dt) * p * q
        #            + div(phi0 * uf) * q
        #            + div(phi0s * idt * us) * q) * dx

        #     assemble(a_s + a_f + a_p, tensor=self.A_monolithic)
        #     self.A_monolithic_assembled = True
        #     for b in self.bcs_monolithic:
        #         b.apply(self.A_monolithic)

        # # Compute solid residual
        # rhs_s_n = dot(fs_vol, vs) * dx + dot(fs_sur, vs) * \
        #     self.solid.dsNeumann

        # M_s = dot(Constant(rhos * idt**2) * phi0s *
        #           (- 2. * self.us_nm1 + self.us_nm2), vs)
        # K_s = - phi0**2 * dot(ikf * idt * self.us_nm1, vs)
        # lhs_s_n = (M_s + K_s) * dx

        # r_s = rhs_s_n - lhs_s_n

        # # Compute fluid residual
        # rhs_f_n = dot(ff_vol, vf) * dx + dot(ff_sur, vf) * \
        #     self.fluid_pressure.dsNeumann

        # M_f = dot(Constant(rhof * idt) * phi0 * (-self.uf_nm1), vf)
        # K_f = phi0**2 * dot(ikf * idt * self.us_nm1, vf)
        # lhs_f_n = (M_f + K_f) * dx

        # r_f = rhs_f_n - lhs_f_n

        # # Compute pressure residual
        # rhs_p_n = Constant(1 / rhof) * p_source * q * dx

        # D_sf = div(idt * phi0s * (-self.us_nm1)) * q
        # M_p = phi0s**2 / Constant(ks * dt) * (-self.p_nm1) * q
        # lhs_p_n = (M_p + D_sf) * dx

        # r_p = rhs_p_n - lhs_p_n

        # assemble(r_s + r_f + r_p, tensor=self.b_monolithic)

        # for b in self.bcs_monolithic:
        #     b.apply(self.b_monolithic)

        # # Compute weighted norm
        # res = self.A_monolithic * solution_current.vector() - self.b_monolithic

        # if comp == "full":
        #     return norm(res)
        # elif comp == "split":
        #     from numpy.linalg import norm as npnorm
        #     res = res.vec()
        #     res_s = res.getValues(self.V_full.sub(0).dofmap().dofs())
        #     res_f = res.getValues(self.V_full.sub(1).dofmap().dofs())
        #     res_p = res.getValues(self.V_full.sub(2).dofmap().dofs())
        #     return npnorm(res_s), npnorm(res_f), npnorm(res_p)

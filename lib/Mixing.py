from dolfin import *
from petsc4py import PETSc
from copy import deepcopy
import numpy as np
import scipy.linalg as splinalg


class MixingAbstract:
    """
    Abstract mixing class. Mixing methods are expected to yield a different increment. 
    A flag allows to update the solution vector directly. 
    """

    def __init__(self, N_increments):
        assert N_increments in [1, 2], "Can only handle 1 or 2 increments"
        self.N_increments = N_increments

        self.increment = None  # Keep final increment internally
        self.k = 0

    def update_increment(self, solution_new, increment_new1, increment_new2=None):

        # Allocate local increment on first iteration
        if self.k == 0:
            self.increment = solution_new.copy()
            self.increment.zeroEntries()
            self.solution_old = solution_new.copy()
            self.solution_old.zeroEntries()
            self.increment_old1 = solution_new.copy()
            self.increment_old1.zeroEntries()
            self.increment_old2 = solution_new.copy()
            self.increment_old2.zeroEntries()

        if self.N_increments == 1:
            self.update_one_increment(solution_new, increment_new1)
        elif self.N_increments == 2:
            self.update_two_increments(solution_new, increment_new1, increment_new2)

        # Update internals
        solution_new.copy(self.solution_old)
        increment_new1.copy(self.increment_old1)
        increment_new2.copy(self.increment_old2)
        self.k += 1


class MixingFixed(MixingAbstract):

    def __init__(self, weight1, weight2, N_increments):
        super().__init__(N_increments)
        self.weight1 = weight1
        self.weight2 = weight2

    def update_one_increment(self, solution_new, increment_new):
        self.increment.zeroEntries()
        self.increment.axpy(self.weight1, increment_new)

    def update_two_increments(self, solution_new, increment_new1, increment_new2):
        self.increment.zeroEntries()
        self.increment.axpy(self.weight1, increment_new1)
        self.increment.axpy(self.weight2, increment_new2)


class MixingAitken(MixingAbstract):

    def __init__(self, N_increments, variant="unconstrained"):

        super().__init__(N_increments)
        self.variant = variant

        self.solutions = []

        self.alpha = 1.
        self.alpha1 = 0.5
        self.alpha2 = 0.5

    def update_one_increment(self, solution_new, increment_new):
        self.increment.zeroEntries()
        if self.k == 0:
            increment_new.copy(self.increment)
        else:
            sol = solution_new - self.solution_old
            inc = increment_new - self.increment_old

            nrm = inc.dot(inc)
            proj = sol.dot(inc)

            if nrm > 3e-16:
                self.alpha = -proj / nrm
            else:
                print("WARNING: Fixed mixing called with null increment.")
                self.alpha = 1

            self.increment.axpy(self.alpha, increment_new)

    def update_two_increments(self, solution_new, increment_new1, increment_new2):
        self.increment.zeroEntries()
        if self.k > 0:
            sol = solution_new - self.solution_old
            inc1 = increment_new1 - self.increment_old1
            inc2 = increment_new2 - self.increment_old2

            if self.variant == "constrained":
                """
                Solve for weights that sum up to 1 resulting in the smallest possible increment.
                """
                temp = inc1 - inc2
                temp2 = sol + inc2

                nrm = temp.dot(temp)
                proj = temp2.dot(temp)

                # If the problem seems badly scaled keep weights from previous iteration
                if nrm > 3e-16:
                    self.alpha1 = -proj / nrm
                    self.alpha2 = 1. - self.alpha1

            elif self.variant == "unconstrained":
                """
                No additional constraint for weights which includes a line search type relaxation aiming for the smallest possible increment.
                """
                Inc = np.transpose(np.array([inc1.array, inc2.array]))
                Q, R = np.linalg.qr(Inc)
                alphas = splinalg.solve(R, -Q.T @ sol)
                self.alpha1 = alphas[0]
                self.alpha2 = alphas[1]

            else:

                print("Chosen variant for Aitken does not exist.")

        self.increment.axpy(self.alpha1, increment_new1)
        self.increment.axpy(self.alpha2, increment_new2)

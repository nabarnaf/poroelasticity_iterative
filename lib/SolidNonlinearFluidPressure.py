from dolfin import *
from ufl import indices
from petsc4py import PETSc
import numpy


class SolidNonlinearFluidPressure:

    def inner_solve(self, A, sol, b):
        # assert (self.parameters["linear solver"] in (
        # "iterative", "direct")), "linear solver parameter bust be one of 'iterative/direct'"
        if self.parameters["linear solver"] == "direct":
            solve(A, sol, b, "mumps")
        else:  # == "iterative"
            solver = PETScKrylovSolver()
            ksp = PETSc.KSP().create()
            Amat = as_backend_type(A).mat()
            ksp.setOperators(Amat, Amat)
            ksp.setType("gmres")
            pc = ksp.getPC()
            ksp.setFromOptions()
            pc.setFromOptions()
            # solver.set_operators(A, A)  # Set operators (A, P)
            # solver.set_from_options()
            # solver.solve(sol, b)
            bvec = as_backend_type(b).vec()
            solvec = as_backend_type(sol).vec()
            ksp.solve(bvec, solvec)

    def __init__(self, parameters, mesh, dsSolidNeumann, dsSolidRobin, dsFluidNeumann, dsFluidNoSlip):
        """
        Create instance of fluid model, which handles the fluid iteration of the iterative splitting.
        Input:
        @parameters: Dictionary with all model parameters
        """

        # First assert that all required variables are in the parameters dictionary
        required_fields = [
            "fe_degree_solid",
            "fe_degree_fluid_velocity",
            "fe_degree_pressure",
            "Cg",
            "bf",
            "bs",
            "bn",
            "bfs",
            "bfn",
            "bsn",
            "k",
            "AS",
            "mu_f",
            "rhos",
            "rhof",
            "phi0",
            "ks",
            "kf",
            "dt",
            "solver_type",
            "reassemble Jacobian"]
        assert all([x in parameters.keys()
                    for x in required_fields]), "Missing arguments in parameters: {}".format([x for x in required_fields if x not in parameters.keys()])

        self.parameters = parameters
        # Solver
        self.solver_type = parameters["solver_type"]

        # Problem setting
        self.mesh = mesh
        self.dim = mesh.topology().dim()
        self.fe_degree_u = parameters["fe_degree_solid"]
        self.fe_degree_v = parameters["fe_degree_fluid_velocity"]
        self.fe_degree_p = parameters["fe_degree_pressure"]
        el_s = VectorElement('CG', mesh.ufl_cell(), self.fe_degree_u)
        el_f = VectorElement('CG', mesh.ufl_cell(), self.fe_degree_v)
        el_p = FiniteElement('CG', mesh.ufl_cell(), self.fe_degree_p)
        self.V = FunctionSpace(mesh, MixedElement(el_s, el_f, el_p))
        self.Vs = FunctionSpace(mesh, el_s)
        self.Vf = FunctionSpace(mesh, el_f)
        self.Vp = FunctionSpace(mesh, el_p)
        self.Vsf = FunctionSpace(mesh, MixedElement(el_s, el_f))
        self.Vfp = FunctionSpace(mesh, MixedElement(el_f, el_p))
        self.A_s = PETScMatrix()
        self.A_f = PETScMatrix()
        self.A_sf = PETScMatrix()
        self.A_p = PETScMatrix()
        self.A_p_diff = PETScMatrix()
        self.A_fp = PETScMatrix()
        self.A_fp_diff = PETScMatrix()
        self.fluid_lhs_assembled = False
        self.pressure_lhs_assembled = False
        self.pressure_lhs_diff_assembled = False
        self.p_lhs_assembled = False
        self.p_diff_lhs_assembled = False
        self.fp_lhs_assembled = False
        self.fp_diff_lhs_assembled = False
        self.s_lhs_assembled = False

        self.increment = Function(self.V)
        self.solution = Function(self.V)

        self.dsSolidNeumann = dsSolidNeumann
        self.dsSolidRobin = dsSolidRobin
        self.dsFluidNeumann = dsFluidNeumann
        self.dsFluidNoSlip = dsFluidNoSlip

        # Model parameters
        self.mu_f = parameters["mu_f"]
        self.rhos = parameters["rhos"]
        self.rhof = parameters["rhof"]
        self.phi0 = parameters["phi0"]
        # Usyk,. mc Culloch 2002
        self.Cg = parameters["Cg"]
        self.bf = parameters["bf"]
        self.bs = parameters["bs"]
        self.bn = parameters["bn"]
        self.bfs = parameters["bfs"]
        self.bfn = parameters["bfn"]
        self.bsn = parameters["bsn"]
        self.k = parameters["k"]
        self.ks = parameters["ks"]
        self.kf = parameters["kf"]
        self.AS = parameters["AS"]
        # Stabilization
        self.stab_fs = parameters["stab fs"]
        # Solver parameters
        self.reassemble_jacobian = parameters["reassemble Jacobian"]
        # Burtschell, Chapelle, Moireau (2017). Nistche for vf=vs.
        self.gamma = parameters["gamma"]

        # Read fibers
        rr = HDF5File(MPI.comm_world, parameters["fibers"], "r")
        f = Function(VectorFunctionSpace(self.mesh, 'CG', 1))
        rr.read(f, "f")
        self.f0 = f.copy(True)
        rr.read(f, "s")
        self.s0 = f.copy(True)
        rr.read(f, "n")
        self.n0 = f.copy(True)

        # Time parameters
        self.dt = parameters["dt"]

        self.firstcall = True

    def init(self):
        if not self.reassemble_jacobian:
            self.s_lhs_assembled = False

    def set_solid_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant.
        """
        self.solid_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def set_fluid_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.fluid_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def set_pressure_bcs(self, bcs):
        """
        Set boundary conditions as a list. Assumed to be constant in time.
        """

        self.pressure_bcs = bcs if (isinstance(bcs, list) or bcs is None) else [
            bcs]  # Possibly None, otherwise it must be a list

    def get_bcs(self, diff=False):

        bcs = []
        if self.solid_bcs:
            bcs = bcs + self.solid_bcs
        if self.fluid_bcs:
            bcs = bcs + self.fluid_bcs
        if diff and self.pressure_bcs:
            bcs = bcs + self.pressure_bcs
        return bcs

    def solve(self, t, solution_k, us_nm1, us_nm2, vf_nm1, p_nm1, fs_vol, fs_sur, ff_vol, ff_sur, variant_diff=False):
        """
        Build nonlinear forms to be solved
        """
        # obtain solution functions and initialize them
        us_k, vf_k, p_k = solution_k.split()
        self.us_n = us_nm1
        if self.solver_type == "monolithic":
            solution = Function(self.V)
            us, vf, p = split(solution)
            v, w, q = TestFunctions(self.V)

            solution.assign(solution_k)
        elif self.solver_type in ("fixed-stress-fp-s", "undrained"):
            us = Function(self.Vs)
            v = TestFunction(self.Vs)
            vfp = Function(self.Vfp)
            vf, p = split(vfp)
            w, q = TestFunctions(self.Vfp)

            assign(us, us_k)
            assign(vfp.sub(0), vf_k)
            assign(vfp.sub(1), p_k)
        elif self.solver_type == "cahouet-chabard-fp-s-to-3-way":
            us = Function(self.Vs)
            v = TestFunction(self.Vs)
            vf = Function(self.Vf)
            w = TestFunction(self.Vf)
            p = Function(self.Vp)
            q = TestFunction(self.Vp)
            assign(us, us_k)
            assign(vf, vf_k)
            assign(p, p_k)

        # Then define residuals for lhs jacobian. Constant functions are given through solution_k.
        if (self.solver_type == "monolithic"):
            v, w, q = TestFunctions(self.V)
            F = self.compute_residual(t,
                                      us, vf, p, v, w, q, us_nm1, us_nm2, us_k, vf_nm1, p_nm1, p_k, fs_vol, fs_sur, ff_vol, ff_sur)
            dF = derivative(F, solution, TrialFunction(self.V))
        else:
            res_s, res_f, res_p, res_p_diff = self.compute_residual(
                t, us, vf, p, v, w, q, us_nm1, us_nm2, us_k, vf_nm1, p_nm1, p_k, fs_vol, fs_sur, ff_vol, ff_sur, split_residual=True, forms=True)

        if self.solver_type == "monolithic":
            # Set matrix and solve.
            A = assemble(dF)
            self.apply_solid_bc(A)
            self.apply_fluid_bc(A)

            b = assemble(-F)
            self.apply_solid_bc(b)
            self.apply_fluid_bc(b)
            # LU = LUSolver(A, method="mumps")
            # LU.solve(self.increment.vector(), b)
            self.inner_solve(A, self.increment.vector(), b)

        elif self.solver_type == "fixed-stress-fp-s":
            # Solve the fluid-pressure
            res_fp = res_f + res_p
            if not self.fp_lhs_assembled:
                a = derivative(res_fp, vfp, TrialFunction(self.Vfp))
                assemble(a, tensor=self.A_f)
                self.apply_fluid_bc(self.A_f)
                # self.LU_f = LUSolver(self.A_f, method="mumps")
                self.fp_lhs_assembled = True
            b = assemble(-res_fp)
            self.apply_fluid_bc(b)
            inc_fp = Function(self.Vfp)
            # self.LU_f.solve(inc_fp.vector(), b)
            self.inner_solve(self.A_f, inc_fp.vector(), b)
            assign(self.increment.sub(1), inc_fp.sub(0))
            assign(self.increment.sub(2), inc_fp.sub(1))

            # Update fluid velocity and pressure
            vfp.vector().vec().axpy(1, inc_fp.vector().vec())

            # Solve solid
            inc_s = Function(self.Vs)
            if self.reassemble_jacobian:
                A = derivative(res_s, us, TrialFunction(self.Vs))
                A = assemble(A)
                b = assemble(-res_s)
                for obj in (A, b):
                    self.apply_solid_bc(obj)
                # solve(A, inc_s.vector(), b, "mumps")
                self.inner_solve(A, inc_s.vector(), b)
            else:
                if not self.s_lhs_assembled:
                    a = derivative(res_s, us, TrialFunction(self.Vs))
                    assemble(a, tensor=self.A_s)
                    self.apply_solid_bc(self.A_s)
                    # self.LU_s = LUSolver(self.A_s, method="mumps")
                    self.s_lhs_assembled = True
                b = assemble(-res_s)
                self.apply_solid_bc(b)
                # self.LU_s.solve(inc_s.vector(), b)
                self.inner_solve(self.A_s, inc_s.vector(), b)
            assign(self.increment.sub(0), inc_s)

            # Update displacement
            us.vector().vec().axpy(1, inc_s.vector().vec())

        elif self.solver_type == "undrained":

            # Solve solid
            A = derivative(res_s, us, TrialFunction(self.Vs))
            A = assemble(A)
            b = assemble(-res_s)
            for obj in (A, b):
                self.apply_solid_bc(obj)
            inc_s = Function(self.Vs)
            # solve(A, inc_s.vector(), b, "mumps")
            self.inner_solve(A, inc_s.vector(), b)
            assign(self.increment.sub(0), inc_s)

            # Update displacement
            us.vector().vec().axpy(1, inc_s.vector().vec())

            # Solve the fluid-pressure
            res_fp = res_f + res_p
            if not self.fluid_lhs_assembled:
                a = derivative(res_fp, vfp, TrialFunction(self.Vfp))
                assemble(a, tensor=self.A_f)
                self.apply_fluid_bc(self.A_f)
                # self.LU_f = LUSolver(self.A_f, method="mumps")
                self.fluid_lhs_assembled = True
            b = assemble(-res_fp)
            self.apply_fluid_bc(b)
            inc_fp = Function(self.Vfp)
            # self.LU_f.solve(inc_fp.vector(), b)
            self.inner_solve(self.A_f, inc_fp.vector(), b)
            assign(self.increment.sub(1), inc_fp.sub(0))
            assign(self.increment.sub(2), inc_fp.sub(1))

            # Update fluid velocity and pressure
            vfp.vector().vec().axpy(1, inc_fp.vector().vec())

        elif self.solver_type == "cahouet-chabard-fp-s-to-3-way-reversed":

            # Solve solid
            A = derivative(res_s, us, TrialFunction(self.Vs))
            A = assemble(A)
            b = assemble(-res_s)
            for obj in (A, b):
                self.apply_solid_bc(obj)
            inc_s = Function(self.Vs)
            # solve(A, inc_s.vector(), b, "mumps")
            self.inner_solve(A, inc_s.vector(), b)
            assign(self.increment.sub(0), inc_s)
            us.vector().vec().axpy(1, inc_s.vector().vec())

            # Solve the fluid
            if not self.fluid_lhs_assembled:
                a = derivative(res_f, vf, TrialFunction(self.Vf))
                assemble(a, tensor=self.A_f)
                self.apply_fluid_bc(self.A_f)
                # self.LU_f = LUSolver(self.A_f, method="mumps")
                self.fluid_lhs_assembled = True
            b = assemble(-res_f)
            self.apply_fluid_bc(b)
            inc_f = Function(self.Vf)
            # self.LU_f.solve(inc_f.vector(), b)
            self.inner_solve(self.A_f, inc_f.vector(), b)
            assign(self.increment.sub(1), inc_f)
            vf.vector().vec().axpy(1, inc_f.vector().vec())

            # Solve pressure
            if not self.pressure_lhs_assembled:
                if variant_diff:
                    a = derivative(res_p_diff, p, TrialFunction(self.Vp))
                    assemble(a, tensor=self.A_p_diff)
                    self.apply_pressure_bc(self.A_p_diff)
                    # self.LU_p_diff = LUSolver(self.A_p_diff, method="mumps")
                    self.pressure_lhs_assembled = True
                else:
                    a = derivative(res_p, p, TrialFunction(self.Vp))
                    assemble(a, tensor=self.A_p)
                    # self.LU_p = LUSolver(self.A_p, method="mumps")

            b = assemble(-res_p)
            if variant_diff:
                # LUp = self.LU_p_diff
                Ap = self.A_p_diff
                self.apply_pressure_bc(b)
            else:
                # LUp = self.LU_p
                Ap = self.A_p
            inc_p = Function(self.Vp)
            # LUp.solve(inc_p.vector(), b)
            self.inner_solve(Ap, inc_p.vector(), b)
            assign(self.increment.sub(2), inc_p)

            p.vector().vec().axpy(1, inc_p.vector().vec())

        elif self.solver_type == "cahouet-chabard-fp-s-to-3-way":

            # Solve pressure
            if not self.pressure_lhs_assembled or not self.pressure_lhs_diff_assembled:
                if variant_diff:
                    a = derivative(res_p_diff, p, TrialFunction(self.Vp))
                    assemble(a, tensor=self.A_p_diff)
                    self.apply_pressure_bc(self.A_p_diff)
                    # self.LU_p_diff = LUSolver(self.A_p_diff, method="mumps")
                    self.pressure_lhs_diff_assembled = True
                else:
                    a = derivative(res_p, p, TrialFunction(self.Vp))
                    assemble(a, tensor=self.A_p)
                    # self.LU_p = LUSolver(self.A_p, method="mumps")
                    self.pressure_lhs_assembled = True

            b = assemble(-res_p)
            if variant_diff:
                Ap = self.A_p_diff
                # LUp = self.LU_p_diff
                self.apply_pressure_bc(b)
            else:
                Ap = self.A_p
                # LUp = self.LU_p
            inc_p = Function(self.Vp)
            # LUp.solve(inc_p.vector(), b)
            self.inner_solve(Ap, inc_p.vector(), b)
            assign(self.increment.sub(2), inc_p)

            p.vector().vec().axpy(1, inc_p.vector().vec())

            # Solve the fluid
            if not self.fluid_lhs_assembled:
                a = derivative(res_f, vf, TrialFunction(self.Vf))
                assemble(a, tensor=self.A_f)
                self.apply_fluid_bc(self.A_f)
                # self.LU_f = LUSolver(self.A_f, method="mumps")
                self.fluid_lhs_assembled = True

            b = assemble(-res_f)
            self.apply_fluid_bc(b)
            inc_f = Function(self.Vf)
            # self.LU_f.solve(inc_f.vector(), b)
            self.inner_solve(self.A_f, inc_f.vector(), b)
            assign(self.increment.sub(1), inc_f)

            vf.vector().vec().axpy(1, inc_f.vector().vec())

            # Solve solid
            inc_s = Function(self.Vs)
            if self.reassemble_jacobian:
                A = derivative(res_s, us, TrialFunction(self.Vs))
                A = assemble(A)
                b = assemble(-res_s)
                for obj in (A, b):
                    self.apply_solid_bc(obj)
                # solve(A, inc_s.vector(), b, "mumps")
                self.inner_solve(A, inc_s.vector(), b)
            else:
                if not self.s_lhs_assembled:
                    a = derivative(res_s, us, TrialFunction(self.Vs))
                    assemble(a, tensor=self.A_s)
                    self.apply_solid_bc(self.A_s)
                    # self.LU_s = LUSolver(self.A_s, method="mumps")
                    self.s_lhs_assembled = True
                b = assemble(-res_s)
                self.apply_solid_bc(b)
                # self.LU_s.solve(inc_s.vector(), b)
                self.inner_solve(self.A_s, inc_s.vector(), b)
            assign(self.increment.sub(0), inc_s)

            # Update displacement
            us.vector().vec().axpy(1, inc_s.vector().vec())

        elif self.solver_type == "CC-fs-p":
            # Solve the fluid-pressure / fixed-stress approach
            res_sf = res_s + res_f
            if variant_diff and not self.p_diff_lhs_assembled:
                a = derivative(res_p_diff, p, TrialFunction(self.Vp))
                assemble(a, tensor=self.A_p_diff)
                self.apply_pressure_bc(self.A_p_diff)
                # self.LU_p_diff = LUSolver(self.A_p_diff, method="mumps")
                self.p_diff_lhs_assembled = True
            if (not variant_diff) and (not self.p_lhs_assembled):
                a = derivative(res_p, p, TrialFunction(self.Vp))
                assemble(a, tensor=self.A_p)
                self.apply_fluid_bc(self.A_p)
                # self.LU_p = LUSolver(self.A_p, method="mumps")
                self.p_lhs_assembled = True

            b = assemble(-res_p)
            if variant_diff:
                # LUp = self.LU_p_diff
                Ap = self.A_p_diff
                self.apply_pressure_bc(b)
            else:
                Ap = self.A_p
                # LUp = self.LU_p
            inc_p = Function(self.Vp)
            # LUp.solve(inc_p.vector(), b)
            self.inner_solve(Ap, inc_p.vector(), b)
            assign(self.increment.sub(2), inc_p)

            # Update fluid velocity and pressure
            p.vector().vec().axpy(1, inc_p.vector().vec())

            inc_sf = Function(self.Vsf)
            if self.reassemble_jacobian:
                A = derivative(res_sf, usvf, TrialFunction(self.Vsf))
                A = assemble(A)
                b = assemble(-res_sf)
                for obj in (A, b):
                    self.apply_solid_bc(obj)
                    self.apply_fluid_bc(obj)
                # solve(A, inc_sf.vector(), b, "mumps")
                self.inner_solve(A, inc_sf.vector(), b)
            else:
                if not self.s_lhs_assembled:
                    a = derivative(res_sf, usvf, TrialFunction(self.Vsf))
                    assemble(a, tensor=self.A_sf)
                    self.apply_solid_bc(self.A_sf)
                    self.apply_fluid_bc(self.A_sf)
                    # self.LU_sf = LUSolver(self.A_sf, method="mumps")
                    self.s_lhs_assembled = True
                b = assemble(-res_sf)
                self.apply_solid_bc(b)
                self.apply_fluid_bc(b)
                # self.LU_sf.solve(inc_sf.vector(), b)
                self.inner_solve(self.A_sf, inc_sf.vector(), b)

            assign(self.increment.sub(0), inc_sf.sub(0))
            assign(self.increment.sub(1), inc_sf.sub(1))

            # Update displacement
            usvf.vector().vec().axpy(1, inc_sf.vector().vec())

        return self.increment

    def hooke(self, Ften):
        return diff(self.energy(Ften), Ften)

    def eps(self, vec):
        return sym(grad(vec))

    def energy(self, F):

        f0 = self.f0
        s0 = self.s0
        n0 = self.n0

        E = 0.5 * (F.T * F - Identity(self.dim))
        Eff, Efs, Efn = inner(E * f0, f0), inner(E * f0, s0), inner(E * f0, n0)
        Esf, Ess, Esn = inner(E * s0, f0), inner(E * s0, s0), inner(E * s0, n0)
        Enf, Ens, Enn = inner(E * n0, f0), inner(E * n0, s0), inner(E * n0, n0)

        Q = Constant(self.bf) * Eff**2 \
            + Constant(self.bs) * Ess**2 \
            + Constant(self.bn) * Enn**2 \
            + Constant(self.bfs) * 2.0 * Efs**2 \
            + Constant(self.bfn) * 2.0 * Efn**2 \
            + Constant(self.bsn) * 2.0 * Esn**2
        WP = 0.5 * Constant(self.Cg) * (exp(Q) - 1)
        WV = Constant(self.k) / 2 * (det(F) - 1) * ln(det(F))

        return WP + WV

    def apply_solid_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.solid_bcs:
            for b in self.solid_bcs:
                b.apply(obj)

    def apply_fluid_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.fluid_bcs:
            for b in self.fluid_bcs:
                b.apply(obj)

    def apply_pressure_bc(self, obj):
        """
        Apply boundary conditions to an object, be it a matrix or a vector.
        """
        if self.pressure_bcs:
            for b in self.pressure_bcs:
                b.apply(obj)

    def getActive(self, t, tenF):
        C = tenF.T * tenF
        I4f = dot(self.f0, C * self.f0)
        # Heartbeat in 0.8 second
        Ta = self.AS * Constant(max(sin(2 * DOLFIN_PI * t / 0.8), 0))
        Pa = Ta * outer(tenF * self.f0, self.f0) / sqrt(I4f)
        return Pa

    def getPiolaTotal(self, t, us):
        Ften = grad(us) + Identity(self.dim)
        Ften = variable(Ften)
        P = diff(self.energy(Ften), Ften)
        Pa = self.getActive(t, Ften)
        if self.parameters["linear mechanics"]:
            Id = Identity(self.dim)
            eps = 0.5 * (grad(us) + grad(us).T)
            hooke = 2 * self.parameters["mu_s"] * eps + \
                self.parameters["lmbda"] * tr(eps) * Id
            return hooke
        else:
            return P + Pa

    def linearizedEnergy(self, eps):

        f0 = self.f0
        s0 = self.s0
        n0 = self.n0

        epsff, epsfs, epsfn = inner(eps * f0, f0), inner(eps * f0, s0), inner(eps * f0, n0)
        epssf, epsss, epssn = inner(eps * s0, f0), inner(eps * s0, s0), inner(eps * s0, n0)
        epsnf, epsns, epsnn = inner(eps * n0, f0), inner(eps * n0, s0), inner(eps * n0, n0)

        trace = 0.
        for i in range(self.dim):
            trace += eps[i, i]

        Q = Constant(self.bf) * epsff**2 \
            + Constant(self.bs) * epsss**2 \
            + Constant(self.bn) * epsnn**2 \
            + Constant(self.bfs) * 2.0 * epsfs**2 \
            + Constant(self.bfn) * 2.0 * epsfn**2 \
            + Constant(self.bsn) * 2.0 * epssn**2
        WP = 0.5 * Constant(self.Cg) * (exp(Q) - 1)
        WV = Constant(self.k) / 2 * trace**2

        return WP + WV

    def stressTangent(self, t, us):
        # Linear
        gradus = grad(us)
        eps = 0.5 * (grad(us) + grad(us).T)
        eps = variable(eps)
        linstress = diff(self.linearizedEnergy(eps), eps)
        dlinstress = diff(linstress, eps)

        # Nonlinear
        F_ = grad(us) + Identity(self.dim)
        F = variable(F_)
        nonlinstress = diff(self.energy(F), F)
        dnonlinstress = diff(nonlinstress, F)

        activestress = self.getActive(t, F)
        dactivestress = diff(activestress, F)

        dstress = dlinstress  # dnonlinstress + dactivestress

        # convert dstress to second-rank tensor
        # Create list to contain all components of the stress tensor
        Vdg = FunctionSpace(self.mesh, 'DG', 0)
        functions_list = self.dim**2 * self.dim**2*[None]

        for i in range(self.dim):
            for j in range(self.dim):
                for k in range(self.dim):
                    for l in range(self.dim):
                        # Store as PETScVector right away with .vector().vec()
                        functions_list[(i*self.dim + j) * self.dim**2 + k*self.dim +
                                       l] = project(dstress[i, j, k, l], Vdg).vector().vec()

        # convert identity tensors into vectors
        Id = Identity(self.dim)
        Id_vec = numpy.zeros(self.dim**2)
        for i in range(self.dim):
            for j in range(self.dim):
                Id_vec[i*self.dim + j] = Id[i, j]

        # Compute inverse tensor and store local I:C^{-1}:I
        spatial_result = Function(Vdg)
        maxres = 0
        for dof in range(Vdg.dim()):
            # Local matrix, placeholder for inverse as well
            C = numpy.zeros((self.dim**2, self.dim**2))
            for i in range(self.dim**2):
                for j in range(self.dim**2):
                    C[i, j] = functions_list[i*self.dim**2 + j].getValue(dof)
            C = numpy.linalg.inv(C)
            res = 0
            for i in range(self.dim**2):
                for j in range(self.dim**2):
                    # Now replace with inverse and local Kdr
                    #functions_list[i*self.dim**2 + j].setValue(dof, C[i,j])
                    res += Id_vec[i] * C[i, j] * Id_vec[j]
            # Update spatially varying result
            spatial_result.vector().vec().setValue(dof, res)
            maxres = max(maxres, res)

        return spatial_result

    def getRobinLoad(self, us, vs):
        # Pfaller et al.
        k_perp = Constant(2e5)  # [Pa/m]
        c_perp = Constant(5e3)  # [Pa*s/m]
        n = FacetNormal(self.mesh)
        return -outer(n, n) * (k_perp * us + c_perp * vs) - \
            (Identity(self.dim) - outer(n, n)) * k_perp / 10 * us

    def compute_residual(self, t, us, vf, p, v, w, q, us_nm1, us_nm2, us_k, vf_nm1, p_nm1, p_k, fs_vol, fs_sur, ff_vol, ff_sur, split_residual=False, forms=True):

        phi0 = self.phi0
        phis = 1 - phi0  # Solid fraction
        dt = self.dt
        idt = 1 / dt
        rhos = self.rhos
        rhof = self.rhof
        ks = self.ks
        kf = self.kf
        ikf = inv(kf)
        mu_f = self.mu_f
        dim = us.ufl_shape[0]

        vs = Constant(idt) * (us - us_nm1)
        vs_nm1 = Constant(idt) * (us_nm1 - us_nm2)

        dx = Measure('dx', domain=self.mesh)

        # Determine KDrInv apriori
        # if self.firstcall == True:
        #    self.Kdrinv = self.stressTangent(t, us_nm1)
        #    self.firstcall = False

        # First build the residual to build rhs from previous iteration
        Ptot = self.getPiolaTotal(t, us)
        ts_robin = self.getRobinLoad(us, vs)

        a_s_full = (Constant(rhos * idt) * phis * dot(vs - vs_nm1, v) + inner(Ptot, grad(v)) - p * div(phis * v) +
                    phi0 ** 2 * dot(ikf * (vs - vf), v) - dot(fs_vol, v)) * dx \
            - dot(ts_robin, v) * self.dsSolidRobin   \
            - dot(fs_sur, v) * self.dsSolidNeumann

        sigma_vis = 2 * Constant(mu_f) * self.eps(vf)
        h = CellDiameter(self.mesh)
        n = FacetNormal(self.mesh)
        a_f_full = (Constant(rhof * idt) * phi0 * dot(vf - vf_nm1, w)
                    + phi0 *
                    inner(sigma_vis, grad(w))
                    - p * div(phi0 * w)
                    + phi0 ** 2 * dot(ikf * (vf - vs), w)) * dx \
            - dot(ff_vol, w) * dx \
            - dot(ff_sur, w) * self.dsFluidNeumann \
            + (Constant(self.gamma) / h * dot(vf - vs, w) -
               dot(phi0 * (sigma_vis - p * Identity(self.dim)) * n, w)) * self.dsFluidNoSlip

        a_p_full = (phis**2 / Constant(ks * dt) * (p - p_nm1) * q
                    + div(phi0 * vf) * q
                    + div(phis * vs) * q) * dx

        # Written twice to avoid clas reference
        a_p_diff = (phis**2 / Constant(ks * dt) * (p - p_nm1) * q
                    + div(phi0 * vf) * q
                    + div(phis * vs) * q) * dx

        # Stabilization terms
        if self.solver_type == "undrained" and forms:

            # Stabilization
            N = Constant(ks) / phis**2
            a_s_full += N * div(phis * (us - us_k)) * div(phis * v) * dx

        elif self.solver_type == "fixed-stress-fp-s" and forms:

            # res_s += -phi0**2 * Constant(idt) * dot(ikf * (us - us_k), v) * dx
            a_p_full += Constant(self.stab_fs) * (p - p_k) * q * dx
            # The a priori choice:
            #a_p_full += phis**2 * Constant(idt) * self.Kdrinv * (p - p_k) * q * dx

        elif self.solver_type == "cahouet-chabard-fp-s-to-3-way" and forms:

            # res_s += -phi0**2 * Constant(idt) * dot(ikf * (us - us_k), v) * dx
            # Id = Identity(self.dim)
            # beta_f = phi0**2 * ikf * \
            #     inv(Id + rhos * phis / (phi0**2 * dt) * kf * Id)
            # res_f += dot(beta_f * (vf - vf_k), w) * dx

            beta_CC1 = phi0 / Constant(2. * mu_f / dim)
            beta_CC2 = 1e0 * inv(Constant(rhof / dt) / phi0 + inv(kf))
            a_p_diff += Constant(beta_CC2) * \
                dot(grad(p - p_k), grad(q)) * dx
            a_p_full += Constant(self.stab_fs+beta_CC1) * (p - p_k) * q * dx

        elif self.solver_type == "CC-fs-p" and forms:

            a_p_full += Constant(self.stab_fs) * (p - p_k) * q * dx
            a_p_diff += Constant(self.stab_diff) * \
                dot(grad(p - p_k), grad(q)) * dx

        if split_residual:
            return a_s_full, a_f_full, a_p_full, a_p_diff
        else:
            return a_s_full + a_f_full + a_p_full

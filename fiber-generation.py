#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 15:56:21 2019

@author: barnafi
"""

from dolfin import *
from lib.MeshCreation import prolateGeometry
from lib.Fibers import generateFibers

filename = "prolate_h4_v2_ASCII"
output_dir = "fibers"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(filename)
generateFibers(mesh, markers, ENDOCARD, EPICARD, BASE, output_dir, filename)

# TO READ FILE AFTERWARDS:
rr = HDF5File(MPI.comm_world, "{}/{}.h5".format(output_dir, filename), "r")
f0 = Function(VectorFunctionSpace(mesh, 'CG', 1))
rr.read(f0, "f")
rr.read(f0, "s")
rr.read(f0, "n")

"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
It then compares many schemes based on a monolithic formulation to then save iteration counts
"""

from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic
from statistics import mean

# Create geometry and set Neumann boundaries
Nelements = 40
length = 64
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, length)


neumann_solid_markers = [TOP]  # All others get weakly 0'd.
neumann_fluid_markers = []

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

# Then create solver class

# Convert E, nu to Lame parameters
E = 3e4
nu = 0.2
mu_s = E / (2 * (1 + nu))
lmbda = E * nu / ((1 + nu) * (1 - 2 * nu))

parameters = {"mu_f": 1e-3,
              "rhof": 1e3,
              "rhos": 500,
              "phi0": 1e-3,
              "mu_s": mu_s,
              "lmbda": lmbda,
              "ks": 1e6,
              "kf": 1e-7,
              "dt": 1e-2,
              "t0": 0.0,
              "tf": 0.5,
              "Kdr": 2 * mu_s + lmbda,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual": 1e-6,
              "tolerance increment": 1e-100,
              "maxiter": 200,
              "anderson_order": 0,
              "anderson_delay": 0,
              "export_solutions": False,
              #"output_name": "monolithic",
              "output_name": "footing",
              "solver_type": "monolithic",  # monolithic, undrained, fixed-stress-fp-u
              # "solver_type": "undrained", #consistent with previous results?
              # "solver_type": "fixed-stress-fp-s", # works well!
              # "solver_type": "diagonal-stab", # works well!
              #"solver_type": "diagonal-stab-3-way", # works well!
              # "solver_type": "fixed-stress-p-fs", # Quite large stabilization resulting in slow convergence
              #"solver_type": "fixed-stress-p-f-s", # There is no reason why suddenyl shoudl work nicely!
              # "solver_type": "fixed-stress-sp-f", # Quite large stabilization resulting in slow convergence
              # "solver_type": "cahouet-chabard-p-fs", # without AA, no improvement visible compared to fs-p-fs; works sufficiently with AA and fixed convex mixing; Only right BC (complementary to both solid and fluid seems to work best; but only left or combinations are not significantly worse.
              #"solver_type": "cahouet-chabard-p-fs-to-3-way", # seems as accurate as cahouet-chabard-p-fs but decoupling of f and s for free.
              # "solver_type": "cahouet-chabard-fp-s-to-3-way", # Seems to work if both sovlers are weighted correctly - mixing needed; BCs should be applied complementary to fluid BCs, i.e., left and right
              "mixing_type": "None",
              # "mixing_type": "Aitken",
              #"mixing_type": "fixed-weights",
              #"mixing_weight_fs": 1.,
              #"mixing_weight_diff": 0.1,
              #"mixing_type": "Aitken-constrained",
              "verbose": True,
              "keep_solutions": False,
              "iterations output": False,
              "method": "iterative"}

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_fluid = lambda t: Constant((0., 0.))


def f_sur_solid(t):
    return Expression(("0", "abs(x[0]-L)<L/2?-t*1e5:0"), t=t, L=length / 2, degree=4)


aa_list = [0, 1, 5]
solid_degrees = [1, 2]
solvers_list = ["undrained", "fixed-stress-fp-s", "fixed-stress-p-fs",
                "cahouet-chabard-p-fs", "cahouet-chabard-fp-s-to-3-way", "cahouet-chabard-p-fs-to-3-way"]
solvers_list = ["undrained", "fixed-stress-fp-s", "cahouet-chabard-fp-s-to-3-way"]


def getParams():
    new = {}
    for k in parameters:
        new[k] = parameters[k]
    return new


def getBCs(poromechanics, solver_type):
    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        0), Constant((0, 0)), markers, BOTTOM)]

    def boundary_foot(x, on_boundary):
        return on_boundary and near(x[1], length) and abs(x[0] - length / 2) < length / 4

    def boundary_foot_not(x, on_boundary):
        return on_boundary and not(near(x[1], length) and abs(x[0] - length / 2) < length / 4)

    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        1), Constant((0, 0)), boundary_foot)]
    # At complement of intersection of solid and fluid

    if solver_type in ("cahouet-chabard-p-fs-to-3-way", "cahouet-chabard-p-fs"):
        bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            2), Constant(0), "on_boundary")]
    elif solver_type == "cahouet-chabard-fp-s-to-3-way":
        bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
            2), Constant(0), boundary_foot_not)]
    else: 
        bcs_pressure = [None]

    return bcs_solid, bcs_fluid, bcs_pressure


fil = open("output/analysis-footing.csv", "w+")
print("FE_solid, AA, solver, avg iter", file=fil)


MAX_ITER_CAP = 10

for solid_degree in solid_degrees:
    for aa in aa_list:
        for solver_type in solvers_list:

            # Then create solver class
            parameters = getParams()
            parameters["anderson_order"] = aa
            parameters["solver_type"] = solver_type
            parameters["fe_degree_solid"] = solid_degree

            # Monolithic solver and splits based on a monolithic structure
            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics, solver_type)

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Copy solve method from poromechanics to create stopping after 10 its with MAXITER.
            t = poromechanics.t0
            its = []

            maxiter_counter = 0
            while t < poromechanics.tf:

                t += poromechanics.dt
                poromechanics.t = t
                iter_count = poromechanics.solve_time_step(f_vol_solid(
                    t), f_sur_solid(t), f_vol_fluid(t), f_sur_fluid(t), Constant(0))
                its.append(iter_count)
                if iter_count == parameters["maxiter"]:
                    maxiter_counter += 1
                else:
                    maxiter_counter = 0
                if maxiter_counter == MAX_ITER_CAP:
                    print(
                        "Maximum number of capped iterations reached, breaking current setting.")
                    break
                print(
                    "-- Solved time t={:.4f} in {} iterations".format(t, iter_count))
            avg_iter = mean(its)

            print("FE_solid {}; solver {}; AA {}; avg iter {}\n".format(
                solid_degree, solver_type, aa, avg_iter))
            print("{}, {}, {}, {}".format(
                solid_degree, aa, solver_type, avg_iter), file=fil)

fil.close()

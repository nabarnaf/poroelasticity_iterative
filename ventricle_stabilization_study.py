"""
Script in charge of studying the sensitivity of the nonlinear ventricle simulation with respect to the stabilization parameters.
"""
from dolfin import *
from lib.MeshCreation import prolateGeometry, generateBoundaryMeasure
from lib.PoromechanicsNonlinear import MonolithicNonlinear
from copy import deepcopy
import numpy as np
import pylab as plt

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6

filename = "prolate_h4_v2_ASCII"
# filename = "prolate_4mm"

# Simulation params
beta_min, beta_max = 8e-2, 1.2e-1
betas_num = 5
B_min, B_max = 1e0, 1e0
Bs_num = 1

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(filename)

solid_robin_markers = [ENDOCARD, EPICARD, BASE]
solid_neumann_markers = [NONE]
fluid_neumann_markers = [BASE]
fluid_noslip_markers = [ENDOCARD, EPICARD]
# fluid_noslip_markers = [NONE]

dsSN = generateBoundaryMeasure(mesh, markers, solid_neumann_markers)
dsSR = generateBoundaryMeasure(mesh, markers, solid_robin_markers)
dsFN = generateBoundaryMeasure(mesh, markers, fluid_neumann_markers)
dsFns = generateBoundaryMeasure(mesh, markers, fluid_noslip_markers)

# Create base parameters
parameters = {"mu_f": 0.03,
              "rhof": 1e3,
              "rhos": 1e3,
              "phi0": 0.05,
              "ks": 1e8,  # 1e8
              "kf": 1e-7,  # 1e-7
              "Cg": .88e3,  # [Pa]
              "bf": 8,  # [-]
              "bs": 6,  # [-]
              "bn": 3,  # [-]
              "bfs": 12,  # [-]
              "bfn": 3,  # [-]
              "bsn": 3,  # [-]
              "fibers": "fibers/{}.h5".format(filename),
              "AS": 3e4,  # 3e4
              "k": 5e4,
              "dt": 1e-3,
              "t0": 0.0,
              "tf": 1e-2,
              "mu_s": 4066,
              "lmbda": 711,
              "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "gamma": 20,
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "linear mechanics": False,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual": 1e-6,
              "tolerance increment": 1e-20,  # Can't converge on increments
              "maxiter": 100,
              "anderson_order": 0,
              "anderson_delay": 0,
              "export_solutions": False,
              "output_name": "nonlinear",
              # "solver_type": "monolithic",
              # "solver_type": "undrained",
              # "solver_type": "fixed-stress-fp-s",
              "solver_type": "cahouet-chabard-fp-s-to-3-way",
              "stab fs": 1.0,
              "stab diff": 1.0,
              # "mixing_type": "Aitken",
              # "mixing_type": "None",
              "mixing_type": "fixed-weights",
              "mixing_weight_fs": 1.0,  # Used for non CC cases
              "mixing_weight_diff": 0.1,
              # "mixing_type": "Aitken-constrained",
              "keep_solutions": False,
              "iterations output": True,
              "reassemble Jacobian": True,
              "reassemble Jacobian every nth iteration": 1}

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = f_sur_fluid = lambda t: Constant(
    (0., 0., 0.))


def getBCs(poromechanics):
  bcs_solid = []
  bcs_fluid = []

  sfp = poromechanics.solid_fluid_pressure
  if sfp.solver_type == "monolithic":
    Vs = sfp.V.sub(0)
    Vf = sfp.V.sub(1)
  elif sfp.solver_type in ("fixed-stress-fp-s", "undrained"):
    Vs = sfp.Vs
    Vf = sfp.Vfp.sub(0)
  elif sfp.solver_type == "cahouet-chabard-fp-s-to-3-way":
    Vs = sfp.Vs
    Vf = sfp.Vf

  # bcs_solid = [DirichletBC(Vs, Constant((0, 0, 0)), markers, BASE)]
  # bcs_fluid = [DirichletBC(Vf, Constant((0, 0, 0)), markers, ENDOCARD),
    # DirichletBC(Vf, Constant((0, 0, 0)), markers, EPICARD)]

  if poromechanics.solver_type == "cahouet-chabard-fp-s-to-3-way":
    Vp = poromechanics.solid_fluid_pressure.Vp
    bcs_pressure = [DirichletBC(Vp, Constant(0), markers, ENDOCARD),
                    DirichletBC(Vp, Constant(0), markers, EPICARD),
                    DirichletBC(Vp, Constant(0), markers, BASE)]
  else:
    bcs_pressure = []
  return bcs_solid, bcs_fluid, bcs_pressure


betas = np.linspace(beta_min, beta_max, betas_num)
Bs = np.linspace(B_min, B_max, Bs_num)

avg_map = np.zeros((betas_num, Bs_num))

for i, beta in enumerate(betas):
  for j, B in enumerate(Bs):

    print("Solving beta={:.2e}; B={:.2e}\n".format(beta, B))

    # Then create solver class
    parameters["stab fs"] = beta
    parameters["stab diff"] = B

    # Monolithic solver and splits based on a monolithic structure
    poromechanics = MonolithicNonlinear(
        parameters, mesh, dsSN, dsSR, dsFN, dsFns)

    bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

    poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
    poromechanics.setup()

    poromechanics.solve(f_vol_solid, f_sur_solid,
                        f_vol_fluid, f_sur_fluid)
    avg_iter = poromechanics.avg_iter
    avg_map[i, j] = avg_iter

np.save("output/avg_stabilization_nonlinear_cc_p2", avg_map)

y_labels = [beta_min, beta_max]
x_labels = [B_min, B_max]

fig, ax = plt.subplots(1, 1)
img = ax.imshow(avg_map, origin='lower', extent=[-0.2, 0.2, -1, 1])

ax.set_xticks([-1, 1])
ax.set_xticklabels(x_labels)

ax.set_yticks([-1, 1])
ax.set_yticklabels(y_labels)

plt.xlabel("B")
plt.ylabel("beta")

plt.colorbar(img)

plt.savefig("output/avg_stabilization_nonlinear_cc_p2_plot")

print("\n---=== Done ===---")

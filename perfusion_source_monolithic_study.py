"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
It then compares many schemes based on a monolithic formulation to then save iteration counts
"""

from dolfin import *
from lib.MeshCreation import generate_square
from lib.Poromechanics import Monolithic

# Create geometry and set Neumann boundaries
Nelements = 10
side_length = 1e-2
mesh, markers, LEFT, RIGHT, TOP, BOTTOM, NONE = generate_square(
    Nelements, side_length)


neumann_solid_markers = []  # All get weakly 0'd.
neumann_fluid_markers = []

ds = Measure('ds', domain=mesh, subdomain_data=markers,
             metadata={'optimize': True})
dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))


# Shaw, Izu, Chen-Izu (2013). Mechanical Analysis of Single Myocyte Contraction in a 3-D Elastic Matrix.
E = 3e4
lmbda = 5e4

# Formula from https://en.wikipedia.org/wiki/Lam%C3%A9_parameters
R = sqrt(E**2 + 9 * lmbda**2 + 2 * E * lmbda)
mu_s = 0.25 * (E - 3 * lmbda + R)


# Then create solver class
parameters = {"mu_f": 0.03,
              "rhof": 1e3,
              "rhos": 1e3,
              "phi0": 0.05,
              "mu_s": mu_s,
              "lmbda": lmbda,
              "ks": 1e6,
              # Aprox. from Michler et al. (2012). A computationally efficient framework for the simulation of cardiac perfusion using a multi-compartment Darcy porous-media flow model.
              "kf": 1e-10,
              "dt": 0.1,
              "t0": 0.0,
              "tf": 1.,
              "Kdr": mu_s + lmbda,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 1,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "tolerance residual": 1e-8,
              "tolerance increment": 1e-100,
              "maxiter": 200,
              "anderson_order": 0,
              "anderson_delay": 0,
              "export_solutions": False,
              #"output_name": "monolithic",
              "output_name": "perfusion-source",
              # "solver_type": "monolithic", #monolithic, undrained, fixed-stress-fp-u
              # "solver_type": "undrained", #consistent with previous results?
              # "solver_type": "fixed-stress-fp-s", # works well!
              # "solver_type": "diagonal-stab",  # works well!
              #"solver_type": "diagonal-stab-3-way", # works well!
              # "solver_type": "fixed-stress-p-fs", # Quite large stabilization resulting in slow convergence
              #"solver_type": "fixed-stress-p-f-s", # There is no reason why suddenyl shoudl work nicely!
              #"solver_type": "fixed-stress-sp-f", # Quite large stabilization resulting in slow convergence
              # "solver_type": "cahouet-chabard-p-fs", # without AA, no improvement visible compared to fs-p-fs; works sufficiently with AA and fixed convex mixing; Only right BC (complementary to both solid and fluid seems to work best; but only left or combinations are not significantly worse.
              # seems as accurate as cahouet-chabard-p-fs but decoupling of f and s for free.
              # "solver_type": "cahouet-chabard-p-fs-to-3-way",
              # "solver_type": "cahouet-chabard-fp-s-to-3-way", # Seems to work if both sovlers are weighted correctly - mixing needed; BCs should be applied complementary to fluid BCs, i.e., left and right
              # "mixing_type": "None",
              # "mixing_type": "Aitken",
              "mixing_type": "fixed-weights",
              "mixing_weight_fs": 1.,
              "mixing_weight_diff": 0.1,
              #"mixing_type": "Aitken-constrained",
              "verbose": True,
              "keep_solutions": False,
              "iterations output": False,
              "method": "iterative"}



# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = f_sur_fluid = lambda t: Constant(
    (0., 0.))


def p_source(t): return Constant(500)


aa_list = [0, 1, 5]
solid_degrees = [1, 2]
solvers_list = ["undrained", "fixed-stress-fp-s", "fixed-stress-p-fs",
                "cahouet-chabard-p-fs", "cahouet-chabard-fp-s-to-3-way", "cahouet-chabard-p-fs-to-3-way"]
solvers_list = ["undrained", "fixed-stress-fp-s", "cahouet-chabard-fp-s-to-3-way"]


def getParams():
    new = {}
    for k in parameters:
        new[k] = parameters[k]
    return new


def getBCs(poromechanics):
    bcs_solid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        0), Constant((0, 0)), markers, LEFT)]

    bcs_fluid = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
        1), Constant((0, 0)), markers, LEFT)]

    bcs_pressure = [DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, TOP),
                    DirichletBC(poromechanics.solid_fluid_pressure.V.sub(
                        2), Constant(0), markers, RIGHT),
                    DirichletBC(poromechanics.solid_fluid_pressure.V.sub(2), Constant(0), markers, BOTTOM)]

    return bcs_solid, bcs_fluid, bcs_pressure


fil = open("output/analysis-perfusion-with-swelling-params-k1e4.csv", "w+")
print("FE_solid, AA, solver, avg iter", file=fil)

for solid_degree in solid_degrees:
    for aa in aa_list:
        for solver_type in solvers_list:

            # Then create solver class
            parameters = getParams()
            parameters["anderson_order"] = aa
            parameters["solver_type"] = solver_type
            parameters["fe_degree_solid"] = solid_degree

            # Monolithic solver and splits based on a monolithic structure
            poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

            bcs_solid, bcs_fluid, bcs_pressure = getBCs(poromechanics)

            poromechanics.set_bcs(bcs_solid, bcs_fluid, bcs_pressure)
            poromechanics.setup()

            # Solve
            poromechanics.solve(f_vol_solid, f_sur_solid,
                                f_vol_fluid, f_sur_fluid, p_source)
            avg_iter = poromechanics.avg_iter

            print("FE_solid {}; solver {}; AA {}; avg iter {}\n".format(
                solid_degree, solver_type, aa, avg_iter))
            print("{}, {}, {}, {}".format(
                solid_degree, aa, solver_type, avg_iter), file=fil)

fil.close()

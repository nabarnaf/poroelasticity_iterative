# README #

This repository implements block-partitioned iterative solvers for poroelasticity. We provide two models, one is a linear poroelastic model (Burtschell et al. 2019) and the second one is the same model with a nonlinear stress tensor. 

It works with FEniCS , for which we recommend using the installation with Anaconda, and it will be assumed to exist within your system. Test scripts are to be run from the main folder. 

All tests used for the results from (Both, Barnafi, Radu, Zunino, Quarteroni. Arxiv (2020)) are in test-iterative-splitting-schemes-for-a-soft-material-poromechanics-model.

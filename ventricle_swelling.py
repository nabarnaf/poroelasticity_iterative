#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 15:56:21 2019

@author: barnafi
"""

from dolfin import *
from lib.MeshCreation import prolateGeometry, generateBoundaryMeasure
from lib.PoromechanicsNonlinear import MonolithicNonlinear

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
# parameters["form_compiler"]["quadrature_degree"] = 6

# filename = "prolate_4mm"
filename = "prolate_h4_v2_ASCII"

mesh, markers, ENDOCARD, EPICARD, BASE, NONE = prolateGeometry(filename)

solid_robin_markers = [ENDOCARD, EPICARD, BASE]
solid_neumann_markers = [NONE]
fluid_neumann_markers = [BASE]
fluid_noslip_markers = [ENDOCARD, EPICARD]

dsSN = generateBoundaryMeasure(mesh, markers, solid_neumann_markers)
dsSR = generateBoundaryMeasure(mesh, markers, solid_robin_markers)
dsFN = generateBoundaryMeasure(mesh, markers, fluid_neumann_markers)
dsFns = generateBoundaryMeasure(mesh, markers, fluid_noslip_markers)


# Then create solver class
parameters = {"mu_f": 0.035,
              "rhof": 2e3,
              "rhos": 2e3,
              "phi0": 0.1,
              "ks": 1e8,  # 1e8
              "kf": 1e-7,  # 1e-7
              "Cg": .88e3,  # [Pa]
              "bf": 8,  # [-]
              "bs": 6,  # [-]
              "bn": 3,  # [-]
              "bfs": 12,  # [-]
              "bfn": 3,  # [-]
              "bsn": 3,  # [-]
              "fibers": "fibers/{}.h5".format(filename),
              "AS": 0,  # 3e4
              "k": 5e4,
              "dt": 1e-1,
              "t0": 0.0,
              "tf": 2.0,
              "mu_s": 4066,
              "lmbda": 711,
              "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
              "gamma": 20,
              "uref": 1.0,  # 3e-5,
              "vref": 1.0,  # 6e-3,
              "pref": 1.0,  # 4e1,
              "fe_degree_solid": 2,
              "fe_degree_fluid_velocity": 2,
              "fe_degree_pressure": 1,
              "linear mechanics": True,
              "tolerance residual": 1e-8,
              "tolerance increment": 1e-10,
              "maxiter": 20,
              "anderson_order": 0,
              "anderson_delay": 0,
              "export_solutions": True,
              "output_name": "ventricle_swelling",
              # "solver_type": "monolithic",
              # "solver_type": "undrained", 
              # "solver_type": "fixed-stress-fp-s",
              "solver_type": "cahouet-chabard-fp-s-to-3-way",
              "stab fs": 1.0,
              "stab diff": 1.0,
              # "mixing_type": "Aitken",
              "mixing_type": "None",
              # "mixing_type": "fixed-weights",
              "mixing_weight_fs": 1.0,  # Used for non CC cases
              "mixing_weight_diff": 0.1,
              # "mixing_type": "Aitken-constrained",
              "keep_solutions": False,
              "iterations output": True}

# parameters["stab fs"] =  (1 - parameters["phi0"])**2 / (parameters["dt"] * parameters["Kdr"])
# parameters["stab fs"] *= 10

# Monolithic solver and splits based on a monolithic structure

poromechanics = MonolithicNonlinear(parameters, mesh, dsSN, dsSR, dsFN, dsFns)

# Create load terms
f_vol_solid = f_vol_fluid = f_sur_solid = lambda t: Constant(
    (0., 0., 0.))


def f_sur_fluid(t):
  return Constant(-1e3 * parameters["phi0"] * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)


def getBCs(poromechanics):
  bcs_solid = []
  bcs_fluid = []

  sfp = poromechanics.solid_fluid_pressure
  if sfp.solver_type == "monolithic":
    Vs = sfp.V.sub(0)
    Vf = sfp.V.sub(1)
  elif sfp.solver_type in ("fixed-stress-fp-s", "undrained"):
    Vs = sfp.Vs
    Vf = sfp.Vfp.sub(0)
  elif sfp.solver_type == "cahouet-chabard-fp-s-to-3-way":
    Vs = sfp.Vs
    Vf = sfp.Vf

  # bcs_solid = [DirichletBC(Vs, Constant((0, 0, 0)), markers, BASE)]
  # bcs_fluid = [DirichletBC(Vf, Constant((0, 0, 0)), markers, ENDOCARD),
               # DirichletBC(Vf, Constant((0, 0, 0)), markers, EPICARD)]

  if poromechanics.solver_type == "cahouet-chabard-fp-s-to-3-way":
    Vp = poromechanics.solid_fluid_pressure.Vp
    bcs_pressure = [DirichletBC(Vp, Constant(0), markers, ENDOCARD),
                    DirichletBC(Vp, Constant(0), markers, EPICARD),
                    DirichletBC(Vp, Constant(0), markers, BASE)]
  else:
    bcs_pressure = []
  return bcs_solid, bcs_fluid, bcs_pressure


poromechanics.set_bcs(*(getBCs(poromechanics)))
poromechanics.setup()

# Solve
poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
avg_iter = poromechanics.avg_iter
print("Avg iter {}\n".format(avg_iter))

"""
Script in charge of performing a Swelling test case on a square:
- Solid: No sliding boundaries at bottom and left
- Fluid: No slip at top and bottom, incremental flow at left, free flow at right
"""
from dolfin import *
from lib.MeshCreation import generate_cube
from lib.Poromechanics import Monolithic, TwoWay
import pylab as plt
NONE = 99
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["optimize"] = True
parameters["mesh_partitioner"] = "ParMETIS"
PETScOptions.set("ksp_atol", 1e-10)
PETScOptions.set("ksp_rtol", 1e-8)
PETScOptions.set("ksp_maxiter", 1000)
PETScOptions.set("ksp_type", "gmres")
PETScOptions.set("pc_type", "ilu")
PETScOptions.set("pc_factor_levels", 3)
# parameters['reorder_dofs_serial'] = False  # Required to improve euclid performance... makes monolithic fail
# parameters["form_compiler"]["quadrature_degree"] = 6

plt.rc('font', family='serif')
plt.rcParams.update(
    {'font.size': 18, 'lines.linewidth': 4})

solid_degree = 1
method = "iterative"


def get_geometry(Nelements):
    side_length = 1e-2
    return generate_cube(Nelements, side_length)


def solve_two_way(Nelements):
    mesh, markers, XP, XM, YP, YM, ZP, ZM = get_geometry(Nelements)

    neumann_solid_markers = [XP, YP, ZP]
    neumann_fluid_markers = [XM, YM]

    ds = Measure('ds', domain=mesh, subdomain_data=markers,
                 metadata={'optimize': True})
    dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
    dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

    parameters = {"mu_f": 0.035,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "phi0": 0.1,
                  "mu_s": 4066,
                  "lmbda": 711,
                  "ks": 1e3,
                  "kf": 1e-7,
                  "dt": 0.1,
                  "t0": 0.0,
                  "tf": 0.5,
                  "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                  "uref": 1.0,  # 3e-5,
                  "vref": 1.0,  # 6e-3,
                  "pref": 1.0,  # 4e1,
                  "fe_degree_solid": solid_degree,
                  "fe_degree_fluid_velocity": 2,
                  "fe_degree_pressure": 1,
                  "tolerance residual absolute": 1e-10,
                  "tolerance residual relative": 1e-6,
                  "splitting as preconditioner": False,
                  "maxiter": 100,
                  "anderson_order": 0,
                  "anderson_delay": 0,
                  "export_solutions": False,
                  # "output_name": "monolithic",
                  "output_name": "swelling",
                  "solver_type": "diagonal-stab",
                  "betas": -0.5,
                  "betaf": 0.0,
                  "betap": 1.,
                  "mixing_type": "None",
                  "verbose": False,
                  "keep_solutions": False,
                  "iterations output": False,
                  "iterations solver output": False,
                  "method": method}

    poromechanics = TwoWay(parameters, mesh, dsNs, dsNf)
    print("Monolithic dofs =", poromechanics.V_full.dim())

    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    V = poromechanics.solid.V
    bcs_s = [DirichletBC(V.sub(0), Constant(0), markers, XM),
             DirichletBC(V.sub(1), Constant(0), markers, YM),
             DirichletBC(V.sub(2), Constant(0), markers, ZM)]

    V = poromechanics.fluid_pressure.V
    bcs_f = [DirichletBC(V.sub(0), Constant((0, 0, 0)), markers, ZM),
             DirichletBC(V.sub(0), Constant((0, 0, 0)), markers, ZP)]
    # Intersetion is ZM, so bc for pressure is set on the complement.
    bcs_p = [DirichletBC(V.sub(1), Constant(0), markers, XM),
             DirichletBC(V.sub(1), Constant(0), markers, XP),
             DirichletBC(V.sub(1), Constant(0), markers, YM),
             DirichletBC(V.sub(1), Constant(0), markers, YP),
             DirichletBC(V.sub(1), Constant(0), markers, ZP)]
    poromechanics.set_bcs(bcs_s, bcs_f, bcs_p)

    # Create load terms
    f_vol_solid = f_vol_fluid = lambda t: Constant((0., 0., 0.))

    def f_sur_fluid(t):  # Hard coded boundary
        return Constant(-1e3 * 0.1 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    def f_sur_solid(t):
        return Constant(-1e3 * 0.9 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    # Solve
    from time import perf_counter
    t = perf_counter()
    poromechanics.setup()
    t_setup = perf_counter() - t
    t = perf_counter()
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    t_solve = perf_counter() - t
    # avg_iter = poromechanics.avg_iter
    print("Solved problem with {} dofs".format(poromechanics.V_full.dim()))

    return t_setup, t_solve


def solve_monolithic(Nelements):
    # Create geometry and set Neumann boundaries
    mesh, markers, XP, XM, YP, YM, ZP, ZM = get_geometry(Nelements)

    neumann_solid_markers = [XP, YP, ZP]
    neumann_fluid_markers = [XM, YM]

    ds = Measure('ds', domain=mesh, subdomain_data=markers,
                 metadata={'optimize': True})
    dsNs = sum([ds(i) for i in neumann_solid_markers], ds(NONE))
    dsNf = sum([ds(i) for i in neumann_fluid_markers], ds(NONE))

    # Then create solver class
    # C = 8
    # x, y = SpatialCoordinate(mesh)
    # phi0 = Constant(0.1) + Constant(0.5) * \
    # (sin(Constant(C * pi) * x / side_length))**2
    parameters = {"mu_f": 0.035,
                  "rhof": 1e3,
                  "rhos": 1e3,
                  "phi0": 0.1,
                  "mu_s": 4066,
                  "lmbda": 711,
                  "ks": 1e3,
                  "kf": 1e-7,
                  "dt": 0.1,
                  "t0": 0.0,
                  "tf": 0.5,
                  "Kdr": 4066 + 711,  # Using isotropic formula. Kdr = 2 * mu_s / d + lmbda_s
                  "uref": 1.0,  # 3e-5,
                  "vref": 1.0,  # 6e-3,
                  "pref": 1.0,  # 4e1,
                  "fe_degree_solid": solid_degree,
                  "fe_degree_fluid_velocity": 2,
                  "fe_degree_pressure": 1,
                  "tolerance residual absolute": 1e-10,
                  "tolerance residual relative": 1e-6,
                  "splitting as preconditioner": False,
                  "maxiter": 1,
                  "anderson_order": 0,
                  "anderson_delay": 0,
                  "export_solutions": False,
                  # "output_name": "monolithic",
                  "output_name": "swelling",
                  "solver_type": "monolithic",
                  "betas": -0.5,
                  "betaf": 0.0,
                  "betap": 1.,
                  "mixing_type": "None",
                  "verbose": False,
                  "keep_solutions": False,
                  "iterations output": False,
                  "iterations solver output": False,
                  "method": method}

    # Monolithic solver and splits based on a monolithic structure

    poromechanics = Monolithic(parameters, mesh, dsNs, dsNf)

    # After initialization, Solid and Fluid are instantiated and thus we can access the corresponding spaces for BCs
    V = poromechanics.solid_fluid_pressure.V
    print("Monolithic dofs =", V.dim())
    bcs_s = [DirichletBC(V.sub(0).sub(0), Constant(0), markers, XM),
             DirichletBC(V.sub(0).sub(1), Constant(0), markers, YM),
             DirichletBC(V.sub(0).sub(2), Constant(0), markers, ZM)]

    bcs_f = [DirichletBC(V.sub(1), Constant((0, 0, 0)), markers, ZM),
             DirichletBC(V.sub(1), Constant((0, 0, 0)), markers, ZP)]
    # Intersetion is ZM, so bc for pressure is set on the complement.
    bcs_p = [DirichletBC(V.sub(2), Constant(0), markers, XM),
             DirichletBC(V.sub(2), Constant(0), markers, XP),
             DirichletBC(V.sub(2), Constant(0), markers, YM),
             DirichletBC(V.sub(2), Constant(0), markers, YP),
             DirichletBC(V.sub(2), Constant(0), markers, ZP)]
    poromechanics.set_bcs(bcs_s, bcs_f, bcs_p)

    # Create load terms
    f_vol_solid = f_vol_fluid = lambda t: Constant((0., 0., 0.))

    def f_sur_fluid(t):  # Hard coded boundary
        return Constant(-1e3 * 0.1 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    def f_sur_solid(t):
        return Constant(-1e3 * 0.9 * (1 - exp(-(t**2) / 0.25))) * FacetNormal(mesh)

    # Solve
    from time import perf_counter
    t = perf_counter()
    poromechanics.setup()
    t_setup = perf_counter() - t
    t = perf_counter()
    poromechanics.solve(f_vol_solid, f_sur_solid, f_vol_fluid, f_sur_fluid)
    t_solve = perf_counter() - t
    print("Solved problem with {} dofs".format(poromechanics.solid_fluid_pressure.V.dim()))
    # avg_iter = poromechanics.avg_iter
    return t_setup, t_solve


Ns = []
twoway = []
monol = []
prop_setup = []
prop_solve = []
prop_total = []

fil = open("output/wall-time-monolithic-vs-2way-3d.csv", "w+")
print("Number of elements; Two way setup; Two way solution; Two way total; Monolithic setup; Monolithic solution; Monolithic total", file=fil)

for Nels in range(5, 41, 5):
    print("Solving for {} elements per side".format(Nels))

    Ns.append(Nels)
    twoway.append(solve_two_way(Nels))
    monol.append(solve_monolithic(Nels))

    twoway_setup, twoway_solution = twoway[-1]
    twoway_total = sum(twoway[-1])
    mono_setup, mono_solution = monol[-1]
    mono_total = sum(monol[-1])

    prop_setup.append(twoway_setup / mono_setup)
    prop_solve.append(twoway_solution / mono_solution)
    prop_total.append(twoway_total / mono_total)
    print("{};{};{};{};{};{};{}".format(Nels, twoway_setup, twoway_solution,
                                        twoway_total, mono_setup, mono_solution, mono_total), file=fil)
    print("\n")

fil.close()
# plt.plot(Ns, prop_setup, label="setup")
# plt.plot(Ns, prop_solve, label="solve")
plt.plot(Ns, prop_total, label="total")
plt.plot(Ns, len(Ns) * [1], c='red', linestyle='-')
plt.xlabel('Elements per side')
plt.ylabel('Wall time [s]')
# plt.legend()
plt.savefig("output/wall-time-comparison-3d", bbox_inches="tight")
